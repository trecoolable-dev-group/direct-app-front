import { TestBed } from '@angular/core/testing';

import { AvatarsControlService } from './avatars-control.service';

describe('AvatarsControlService', () => {
  let service: AvatarsControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AvatarsControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
