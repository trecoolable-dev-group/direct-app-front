import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsItemSettingsBoxBaseComponent } from './accounts-item-settings-box-base.component';

describe('AccountsItemSettingsBoxBaseComponent', () => {
  let component: AccountsItemSettingsBoxBaseComponent;
  let fixture: ComponentFixture<AccountsItemSettingsBoxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsItemSettingsBoxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsItemSettingsBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
