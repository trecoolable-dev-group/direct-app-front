import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LottieDisplayBaseComponent } from './lottie-display-base.component';

describe('LottieDisplayBaseComponent', () => {
  let component: LottieDisplayBaseComponent;
  let fixture: ComponentFixture<LottieDisplayBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LottieDisplayBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LottieDisplayBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
