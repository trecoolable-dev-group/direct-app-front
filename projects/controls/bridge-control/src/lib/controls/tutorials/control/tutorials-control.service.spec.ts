import { TestBed } from '@angular/core/testing';

import { TutorialsControlService } from './tutorials-control.service';

describe('TutorialsControlService', () => {
  let service: TutorialsControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TutorialsControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
