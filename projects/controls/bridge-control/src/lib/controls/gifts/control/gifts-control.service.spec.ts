import { TestBed } from '@angular/core/testing';

import { GiftsControlService } from './gifts-control.service';

describe('GiftsControlService', () => {
  let service: GiftsControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GiftsControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
