import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bridge-elements-overlays-box-base',
  templateUrl: './overlays-box-base.component.html',
  styleUrls: ['./overlays-box-base.component.css']
})
export class OverlaysBoxBaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
