import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'bridge-elements-accounts-item-settings-box-effects-section-base',
    templateUrl: './accounts-item-settings-box-effects-section-base.component.html',
    styleUrls: ['./accounts-item-settings-box-effects-section-base.component.css']
})
export class AccountsItemSettingsBoxEffectsSectionBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}






    /// GREETING ///
    //============================

    //user greeting opened
    public greeting_opened = -1;


    /*
     * @Params:  none
     * @Does:    toggles greeting opened
     */
    public async toggleGreetingOpened() {
        let model = this;
        //check if greeting is opened
        if (model.greeting_opened != 1) {
            //set opened
            model.greeting_opened = 1;
        } else {
            //set to closed
            model.greeting_opened = 0;
            //await to remove
            await wait(200);
            //remove greeting
            model.greeting_opened = -1;
        }
    }
    //end toggleGreetingOpened()



    /*
     * @Params:  effect_id
     * @Does:    sets greeting effect
     */
    public handleGreetingSelect(voice_name) {
        let model = this;
        //set default voice
        // model.User.Voice.voice_name = voice_name;
        //save greeting
        // model.User.Voice.saveSettings();
    }
    //end handleGreetingSelect()

    //============================
    /// END GREETING ///







}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d };
}


function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}