import { TestBed } from '@angular/core/testing';

import { GiftsBracketsItemControlService } from './gifts-brackets-item-control.service';

describe('GiftsBracketsItemControlService', () => {
  let service: GiftsBracketsItemControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GiftsBracketsItemControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
