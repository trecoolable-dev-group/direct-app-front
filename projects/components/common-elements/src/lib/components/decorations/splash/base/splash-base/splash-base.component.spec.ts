import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplashBaseComponent } from './splash-base.component';

describe('SplashBaseComponent', () => {
  let component: SplashBaseComponent;
  let fixture: ComponentFixture<SplashBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplashBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplashBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
