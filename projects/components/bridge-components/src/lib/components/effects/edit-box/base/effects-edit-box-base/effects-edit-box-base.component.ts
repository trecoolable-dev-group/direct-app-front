import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'bridge-elements-effects-edit-box-base',
    templateUrl: './effects-edit-box-base.component.html',
    styleUrls: ['./effects-edit-box-base.component.css']
})
export class EffectsEditBoxBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}







    /// DISPLAY ///
    //================================

    //opened flag
    @Input('Opened') Opened = -1;

    //close event
    @Output('onClose') onClose: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  none
     * @Does:    sets opened to false
     */
    public close(force = false) {
        let model = this;
        //check if has unsaved changed
        // if (model.EffectItem.hasChanges() && !force) {
        //     //toggle save opened
        //     model.toggleSaveOpened();
        //     return;
        // }
        //set opened to close
        model.Opened = 0;
        //emit close event
        model.onClose.emit();
    }
    //end close()

    //================================
    /// END DISPLAY ///
















    /// SECTIONS ///
    //=============================

    //current section
    public section = 'info';

    //sections
    public sections = [
        'info',
        'settings',
        'triggers'
    ];


    /*
     * @Params:  section
     * @Does:    changes section
     */
    public async changeSection(section) {
        let model = this;
        //set section
        model.section = section;
    }
    //end changeSection()


    //=============================
    /// END SECTIONS ///
















    /// EFFECT ///
    //================================

    //effect item
    @Input('EffectItem') EffectItem = null;


    //================================
    /// END EFFECT ///













    /// SAVING ///
    //=============================

    //overlay save_opened
    public save_opened = -1;


    /*
     * @Params:  none
     * @Does:    toggles overlay edit_opened
     */
    public toggleSaveOpened() {
        let model = this;
        //check if is save_opened
        if (model.save_opened != 1) {
            //set save_opened
            model.save_opened = 1;
        } else {
            //set closed
            model.save_opened = 0;
            //await to remove
            setTimeout(() => {
                model.save_opened = -1;
            }, 200);
        }
    }
    //end toggleSaveOpened()


    //=============================
    /// END SAVING ///



}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};

//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}