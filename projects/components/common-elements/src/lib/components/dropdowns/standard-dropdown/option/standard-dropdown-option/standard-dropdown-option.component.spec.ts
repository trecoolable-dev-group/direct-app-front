import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardDropdownOptionComponent } from './standard-dropdown-option.component';

describe('StandardDropdownOptionComponent', () => {
  let component: StandardDropdownOptionComponent;
  let fixture: ComponentFixture<StandardDropdownOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardDropdownOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardDropdownOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
