import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'bridge-elements-events-list-gift-item-base',
    templateUrl: './events-list-gift-item-base.component.html',
    styleUrls: ['./events-list-gift-item-base.component.css']
})
export class EventsListGiftItemBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}




    /// EVENT ///
    //============================

    //event
    @Input('EventItem') EventItem = null;

    //============================
    /// END EVENT ///





}