import { TestBed } from '@angular/core/testing';

import { AccountsItemActivityControlService } from './accounts-item-activity-control.service';

describe('AccountsItemActivityControlService', () => {
  let service: AccountsItemActivityControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountsItemActivityControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
