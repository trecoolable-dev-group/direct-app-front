import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsListBaseComponent } from './events-list-base.component';

describe('EventsListBaseComponent', () => {
  let component: EventsListBaseComponent;
  let fixture: ComponentFixture<EventsListBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsListBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsListBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
