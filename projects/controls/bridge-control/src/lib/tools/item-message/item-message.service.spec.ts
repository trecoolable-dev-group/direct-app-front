import { TestBed } from '@angular/core/testing';

import { ItemMessageService } from './item-message.service';

describe('ItemMessageService', () => {
  let service: ItemMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
