import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindingBoxYounowInfoBaseComponent } from './binding-box-younow-info-base.component';

describe('BindingBoxYounowInfoBaseComponent', () => {
  let component: BindingBoxYounowInfoBaseComponent;
  let fixture: ComponentFixture<BindingBoxYounowInfoBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindingBoxYounowInfoBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindingBoxYounowInfoBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
