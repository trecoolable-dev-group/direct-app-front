import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsEditBoxTriggersSectionItemBaseComponent } from './effects-edit-box-triggers-section-item-base.component';

describe('EffectsEditBoxTriggersSectionItemBaseComponent', () => {
  let component: EffectsEditBoxTriggersSectionItemBaseComponent;
  let fixture: ComponentFixture<EffectsEditBoxTriggersSectionItemBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsEditBoxTriggersSectionItemBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsEditBoxTriggersSectionItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
