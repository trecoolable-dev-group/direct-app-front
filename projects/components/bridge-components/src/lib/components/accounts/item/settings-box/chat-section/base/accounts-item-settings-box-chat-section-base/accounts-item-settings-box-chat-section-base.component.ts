import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bridge-elements-accounts-item-settings-box-chat-section-base',
  templateUrl: './accounts-item-settings-box-chat-section-base.component.html',
  styleUrls: ['./accounts-item-settings-box-chat-section-base.component.css']
})
export class AccountsItemSettingsBoxChatSectionBaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
