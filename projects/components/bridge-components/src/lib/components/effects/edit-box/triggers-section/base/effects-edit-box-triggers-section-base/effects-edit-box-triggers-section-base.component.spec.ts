import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsEditBoxTriggersSectionBaseComponent } from './effects-edit-box-triggers-section-base.component';

describe('EffectsEditBoxTriggersSectionBaseComponent', () => {
  let component: EffectsEditBoxTriggersSectionBaseComponent;
  let fixture: ComponentFixture<EffectsEditBoxTriggersSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsEditBoxTriggersSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsEditBoxTriggersSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
