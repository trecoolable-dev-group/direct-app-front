import { Component, OnInit } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-gifts-settings-brackets-section-base',
    templateUrl: './gifts-settings-brackets-section-base.component.html',
    styleUrls: ['./gifts-settings-brackets-section-base.component.css']
})
export class GiftsSettingsBracketsSectionBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set younow brackets
        this.setYounowBrackets();
    }



    //controls
    public Gifts = this.BridgeControl.Gifts;
    public Brackets = this.BridgeControl.Gifts.Brackets;







    /// YOUNOW ///
    //===============================

    /*
     * @Params:  none
     * @Does:    fetches brackets for younow
     */
    public async setYounowBrackets() {
        let model = this;
        //fetch younow brackets
        let fetchOp = await model.Brackets.fetchPlatformBrackets('younow');
    }
    //end setYounowBrackets()

    //===============================
    /// END YOUNOW ///



}