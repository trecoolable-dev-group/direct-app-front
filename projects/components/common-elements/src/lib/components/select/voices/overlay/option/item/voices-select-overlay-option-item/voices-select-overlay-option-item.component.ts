import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
    selector: 'common-elements-voices-select-overlay-option-item',
    templateUrl: './voices-select-overlay-option-item.component.html',
    styleUrls: ['./voices-select-overlay-option-item.component.css']
})
export class VoicesSelectOverlayOptionItemComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}





    /// INFO ///
    //==============================

    //voice name
    @Input('Name') Name = '';

    //active flag
    @Input('Active') Active = false;

    //select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  event
     * @Does:    sets splash
     *           triggers on select
     */
    public selectVoice(event) {
        let model = this;
        //splash
        model.splash(event);
        //trigger select event
        model.onSelect.emit(model.Name);
    }
    //end selectVoice()


    //==============================
    /// END INFO ///


















    /// SPLASH ///
    //=======================================

    //splash element
    @ViewChild('SplashElement') SplashElement: any;


    /*
     * @Params:  event
     * @Does:    triggers splash
     */
    public splash(event) {
        let model = this;
        //trigger splash
        if (model.SplashElement && model.SplashElement.splash) model.SplashElement.splash(event);
    }
    //end handleClick()


    //=======================================
    /// END SPLASH ///









}