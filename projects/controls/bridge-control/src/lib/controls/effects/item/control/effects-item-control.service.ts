import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';
import { EffectsItemSettingsControlService } from '../settings/control/effects-item-settings-control.service';
import { ItemMessageService } from '@controls/bridge-control/src/lib/tools/item-message/item-message.service';
import { EffectsItemTriggersControlService } from '../triggers/control/effects-item-triggers-control.service';

export class EffectsItemControlService {

    constructor(private EffectsDb: ItemsDbService, public effect_id: string) {}




    /// DB ///
    //===========================

    //collections
    private LOCAL_EFFECTS_COL = 'local_effects_col';
    private EFFECTS_DATA_COL = 'effects_data_col';
    private EFFECTS_SETTINGS_COL = 'effects_settings_col';
    private EFFECTS_TRIGGERS_COL = 'effects_triggers_col';

    //===========================
    /// END DB ///







    /// MESSAGE ///
    //=================================

    //message
    public message = new ItemMessageService();

    //messages
    public input_messages = {};

    //=================================
    /// END MESSAGE ///









    /// INFO ///
    //===========================

    //info
    public info = {
        effect_id: this.effect_id,
        name: '',
        type: '',
        media: ''
    }

    //info inputs
    public info_inputs = { ...this.info };

    //has info flag
    public has_info = false;


    /*
     * @Params:  none
     * @Does:    checks if has info
     *           fetches info from effects data
     *           fetches settings from effects settings
     * @Return:  effect
     */
    public async fetchInfo() {
        let model = this;
        //check if has info
        if (model.has_info) return reply(true, 'has info', { info: model.info, settings: model.Settings.settings });
        //fetch info
        let fetchOp: any = await model.EffectsDb.findItemsByKey('effect_id', this.effect_id, model.EFFECTS_DATA_COL);
        //check if got info
        if (!fetchOp.data.items.length) return reply(false, 'no matching effect', { info: null });
        //has effect, set info
        let info = fetchOp.data.items[0];
        //fetch settings
        let settingsOp: any = await model.Settings.fetchSettings();
        //check has settings
        if (!settingsOp.success) return settingsOp;
        //set settings
        let settings = settingsOp.data.settings;
        //set inputs
        model.info = info;
        //set inputs
        model.info_inputs = { ...info };
        //set has info
        model.has_info = true;
        //return with info
        return reply(true, 'got info', { info: model.info, settings: settings });
    }
    //end fetchInfo()



    /*
     * @Params:  none
     * @Does:    saves effect info
     */
    public async saveInfo() {
        let model = this;
        //set blank error
        let error = null;
        //fetch changes
        let keys = model.fetchChanges();
        //set info to inputs
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //set validate
            let validOp = null;
            //check is name
            if (key == 'name') validOp = await model.validateName(model.info_inputs['name']);
            //check if is file
            if (key == 'media') validOp = await model.validateMediaData(model.info_inputs['media']);
            //check if is valid
            if (validOp == null || validOp.success) {
                //set changes
                model.info[key] = model.info_inputs[key];
            } else {
                //set error message
                model.message.setMessage(validOp.message, key, validOp.success, 5000);
                //set input message
                model.input_messages[key] = { ...model.message };
                //set error
                error = validOp;
            }
        }
        //check if has error
        if (error) return reply(false, 'error saving info');
        //set info item
        let item = {
            effect_id: model.effect_id,
            ...model.info
        };
        //set has info
        model.has_info = true;
        //save to info
        model.EffectsDb.upsertItem(item, model.EFFECTS_DATA_COL);
        //set message
        model.message.setMessage('saved info', '', true, 5000);
        //return saved
        return reply(true, 'saved info');
    }
    //end saveInfo()



    /*
     * @Params:  key, value
     * @Does:    validates input
     *           sets input key
     */
    public async handleChange(key, value) {
        let model = this;
        //set input key
        model.info_inputs[key] = value;
    }
    //end handleChange()



    /*
     * @Params:  none
     * @Does:    checks if has changes
     * @Return:  changes
     */
    public fetchChanges() {
        let model = this;
        //set changes array
        let changes = [];
        //change keys
        let keys = [
            'name',
            'type',
            'media'
        ];
        //check if is not saved
        if (!model.has_info) return keys;
        //iterate through keys
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //check if is different
            if (JSON.stringify(model.info[key]) != JSON.stringify(model.info_inputs[key])) changes.push(key);
        }
        //return changes
        return changes;
    }
    //end fetchChanges()



    /*
     * @Params:  none
     * @Does:    resets inputs to original
     */
    public resetChanges() {
        let model = this;
        //set info to previous
        model.info_inputs = { ...model.info };
        //save info
        model.saveInfo();
    }
    //end resetChanges()

    //===========================
    /// END INFO ///










    /// FILE INPUT ///
    //=============================


    /*
     * @Params:  file
     * @Does:    handles media file input
     *           sets input media file
     */
    public async handleFileInput(file) {
        let model = this;
        //check file valid
        let validOp = await model.validateMedia(file);
        //check if is valid
        if (!validOp.success) {
            //set message
            let message = new ItemMessageService();
            //set message
            message.setMessage(validOp.message, 'media', validOp.success, 5000);
            //set to media message
            model.input_messages['media'] = message;
            return;
        }
        //get extension
        let extension = file.name.split('.').pop();
        //set type
        let type = '';
        //check if is audio
        if (extension == 'mpeg' || extension == 'mp3' || extension == 'ogg' || extension == 'wav') type = 'audio';
        //check if is video
        if (extension == 'mp4' || extension == 'webm') type = 'video';
        //check if is gif
        if (extension == 'gif') type = 'gif';
        //check if is lottie
        if (extension == 'json') type = 'lottie';
        //set file data reader
        let reader = new FileReader();
        //set on loaded
        reader.onloadend = () => {
            //set effect type
            model.info_inputs.type = type;
            //set media input
            (model.info_inputs.media as any) = reader.result;
        };
        //read file
        reader.readAsDataURL(file);
    }
    //end handleFileInput()



    /*
     * @Params:  name
     * @Does:    validates name input
     * @Return:  {name, valid}
     */
    public async validateName(name) {
        let model = this;
        //check if is short
        if (!name || name.replace(/\s/g, '').length < 3) return reply(false, 'name must be greater than 3 characters', { valid: false });
        //check if is long
        if (name.length > 32) return reply(false, 'name must be shorter than 32 characters', { valid: false });
        //check if has special chars
        if (/[~`@!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?\()]/g.test(name)) return reply(false, 'name cannot contain special characters', { valid: false });
        //check if starts with space
        if (name[0] == ' ') return reply(false, 'name cannot start with a space', { valid: false });
        //check if ends with a space
        if (name[name.length - 1] == ' ') return reply(false, 'name cannot end with a space', { valid: false });
        //return valid
        return reply(true, 'name is valid', { name: name.toLowerCase(), valid: true });
    }
    //end validateName()



    /*
     * @Params:  file
     * @Does:    validates file size and type
     * @Return:  valid
     */
    public async validateMedia(file) {
        let model = this;
        //check if has media
        if (!file) return reply(false, 'no effect media file', { valid: false });
        //check if size is larger than
        if (file.size > 10000000) {
            //return too large
            return reply(false, 'filesize must be under 10MB', { valid: false });
        }
        //get extension
        let extension = file.name.split('.').pop();
        //valid extensions
        let validExtensions = [
            'mpeg',
            'mp3',
            'wav',
            'ogg',
            'mp4',
            'webm',
            'gif',
            'json',
        ];
        //check if is valid
        if (validExtensions.indexOf(extension) == -1) return reply(false, 'invalid file type', { valid: false });
        //return valid
        return reply(true, 'valid file');
    }
    //end validateMedia()


    /*
     * @Params:  data
     * @Does:    validates data size
     * @Return:  valid
     */
    public async validateMediaData(data) {
        let model = this;
        //check if has media
        if (!data.length) return reply(false, 'no effect media file', { valid: false });
        //check if size is larger than
        if (data.length > 15500000) {
            //return too large
            return reply(false, 'filesize must be under 15MB', { valid: false });
        }
        //return valid
        return reply(true, 'valid file');
    }
    //end validateMediaData()

    //=============================
    /// END FILE INPUT ///












    /// SETTINGS ///
    //===============================

    public Settings = new EffectsItemSettingsControlService(this.EffectsDb, this.effect_id);

    //===============================
    /// END SETTINGS ///










    /// TRIGGERS ///
    //===============================

    public Triggers = new EffectsItemTriggersControlService(this.EffectsDb, this.effect_id);

    //===============================
    /// END TRIGGERS ///




}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}