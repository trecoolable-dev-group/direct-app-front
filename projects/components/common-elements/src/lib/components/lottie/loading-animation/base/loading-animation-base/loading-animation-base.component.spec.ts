import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingAnimationBaseComponent } from './loading-animation-base.component';

describe('LoadingAnimationBaseComponent', () => {
  let component: LoadingAnimationBaseComponent;
  let fixture: ComponentFixture<LoadingAnimationBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingAnimationBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingAnimationBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
