import { TestBed } from '@angular/core/testing';

import { EffectsLocalControlService } from './effects-local-control.service';

describe('EffectsLocalControlService', () => {
  let service: EffectsLocalControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EffectsLocalControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
