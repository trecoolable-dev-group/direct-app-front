import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountriesSelectBaseComponent } from './countries-select-base.component';

describe('CountriesSelectBaseComponent', () => {
  let component: CountriesSelectBaseComponent;
  let fixture: ComponentFixture<CountriesSelectBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountriesSelectBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountriesSelectBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
