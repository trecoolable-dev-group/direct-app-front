import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardCheckboxBaseComponent } from './standard-checkbox-base.component';

describe('StandardCheckboxBaseComponent', () => {
  let component: StandardCheckboxBaseComponent;
  let fixture: ComponentFixture<StandardCheckboxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardCheckboxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardCheckboxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
