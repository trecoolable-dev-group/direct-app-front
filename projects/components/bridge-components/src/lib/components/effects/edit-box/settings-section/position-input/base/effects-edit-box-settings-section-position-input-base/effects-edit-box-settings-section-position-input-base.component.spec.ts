import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsEditBoxSettingsSectionPositionInputBaseComponent } from './effects-edit-box-settings-section-position-input-base.component';

describe('EffectsEditBoxSettingsSectionPositionInputBaseComponent', () => {
  let component: EffectsEditBoxSettingsSectionPositionInputBaseComponent;
  let fixture: ComponentFixture<EffectsEditBoxSettingsSectionPositionInputBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsEditBoxSettingsSectionPositionInputBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsEditBoxSettingsSectionPositionInputBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
