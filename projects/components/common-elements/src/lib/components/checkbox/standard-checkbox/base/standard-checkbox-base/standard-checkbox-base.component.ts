import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'common-elements-standard-checkbox-base',
    templateUrl: './standard-checkbox-base.component.html',
    styleUrls: ['./standard-checkbox-base.component.css']
})
export class StandardCheckboxBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}






    /// STYLE ///
    //===============================

    @Input('HolderClass') HolderClass = 'std-square';

    @Input('BoxClass') BoxClass = 'med-square';

    @Input('BorderColor') BorderColor = '#eaf1f7';

    @Input('ActiveColor') ActiveColor = '#0ebc11';

    @Input('InactiveColor') InactiveColor = '#333';

    @Input('IconName') IconName = 'check-icon';

    @Input('IconColor') IconColor = '#eaf1f7';


    //background color
    public background_color = this.InactiveColor;


    //===============================
    /// END STYLE ///









    /// EVENTS ///
    //=====================================

    //active input
    @Input('Active') Active = false;

    @Output('onClick') onClick: EventEmitter < any > = new EventEmitter < any > ();


    /*
     * @Params:  none
     * @Does:    handles click and triggers click
     */
    public handleClick(event) {
        let model = this;
        //trigger on click
        model.onClick.emit();
    }
    //end handleClick()

    //=====================================
    /// END EVENTS ///








}