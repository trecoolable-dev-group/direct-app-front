import { Component, OnInit } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-binding-box-younow-info-base',
    templateUrl: './binding-box-younow-info-base.component.html',
    styleUrls: ['./binding-box-younow-info-base.component.css']
})
export class BindingBoxYounowInfoBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {}



    //controls
    public Younow = this.BridgeControl.Events.Younow;





}