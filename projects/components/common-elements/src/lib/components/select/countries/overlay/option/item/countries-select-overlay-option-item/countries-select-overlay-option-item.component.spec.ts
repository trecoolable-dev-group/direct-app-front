import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountriesSelectOverlayOptionItemComponent } from './countries-select-overlay-option-item.component';

describe('CountriesSelectOverlayOptionItemComponent', () => {
  let component: CountriesSelectOverlayOptionItemComponent;
  let fixture: ComponentFixture<CountriesSelectOverlayOptionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountriesSelectOverlayOptionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountriesSelectOverlayOptionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
