import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsSettingsFiltersSectionBaseComponent } from './events-settings-filters-section-base.component';

describe('EventsSettingsFiltersSectionBaseComponent', () => {
  let component: EventsSettingsFiltersSectionBaseComponent;
  let fixture: ComponentFixture<EventsSettingsFiltersSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsSettingsFiltersSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsSettingsFiltersSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
