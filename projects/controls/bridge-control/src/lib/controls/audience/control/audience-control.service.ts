import { ItemsDbService } from '../../../tools/items-db/items-db.service';

export class AudienceControlService {

    constructor() {
        //set save members
        this.setMembers();
    }



    /// DB ///
    //===========================

    //db name
    private db_name = 'audience_db';

    //collections
    private AUDIENCE_SAVES_COL = 'audience_saves_col';

    //effects db stores
    private db_stores = {
        [this.AUDIENCE_SAVES_COL]: '&_id'
    }

    //audience db
    private AudienceDB = new ItemsDbService(this.db_name, this.db_stores);


    //===========================
    /// END DB ///












    /// AUDIENCE ///
    //============================

    //members
    public members = [];


    /*
     * @Params:  none
     * @Does:    fetches saved members and sets if not outdated
     */
    public async setMembers() {
        let model = this;
        //fetch members
        let fetchOp: any = await model.AudienceDB.findItemsByKey('_id', 'audience_save', model.AUDIENCE_SAVES_COL);
        //check if has items
        if (fetchOp.data.items.length) {
            //set save
            let save = fetchOp.data.items[0];
            //check if is updated
            if (save.updated > (Date.now() - 2 * 60 * 60000)) {
                //has updated save, set save
                model.members = save.members;
            }
        }
    }
    //end setMembers()



    /*
     * @Params:  none
     * @Does:    saves members
     */
    public saveMembers() {
        let model = this;
        //set save object
        let save = {
            _id: 'audience_save',
            members: model.members,
            updated: Date.now()
        };
        //save to saves
        model.AudienceDB.upsertItem(save, model.AUDIENCE_SAVES_COL);
    }
    //end saveMembers()



    /*
     * @Params:  member
     * @Does:    checks if has member in audience
     *           if has member, returns
     *           if no member, adds member
     *           saves members
     */
    public addMember(member) {
        let model = this;
        //check if has member
        if (model.members.indexOf(member) == -1) return false;
        //add member
        model.members.push(member);
        //save memebrs
        model.saveMembers();
        //return added
        return true;
    }
    //end addMember()


    //============================
    /// END AUDIENCE ///





}