import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-effects-edit-box-triggers-section-base',
    templateUrl: './effects-edit-box-triggers-section-base.component.html',
    styleUrls: ['./effects-edit-box-triggers-section-base.component.css']
})
export class EffectsEditBoxTriggersSectionBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set initial triggers
        this.setTriggers();
    }



    //controls
    public Effects = this.BridgeControl.Effects;;




    /// EFFECT ///
    //============================

    //effect item
    @Input('EffectItem') EffectItem = null;

    //============================
    /// END EFFECT ///













    /// TRIGGERS ///
    //=============================

    /*
     * @Params:  none
     * @Does:    sets initial triggers
     */
    public async setTriggers() {
        let model = this;
        //fetch triggers
        let fetchOp = await model.EffectItem.Triggers.fetchTriggers(true);
        console.log(fetchOp);
    }
    //end setTriggers()



    /*
     * @Params:  none
     * @Does:    adds trigger with date trigger phrase
     */
    public addTrigger() {
        let model = this;
        //add trigger
        model.EffectItem.Triggers.addTrigger('');
    }
    //end addTrigger()

    //=============================
    /// END TRIGGERS ///



}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};

//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}