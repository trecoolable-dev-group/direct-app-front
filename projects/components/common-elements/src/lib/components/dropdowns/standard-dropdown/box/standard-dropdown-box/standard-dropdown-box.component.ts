import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'common-elements-standard-dropdown-box',
    templateUrl: './standard-dropdown-box.component.html',
    styleUrls: ['./standard-dropdown-box.component.css']
})
export class StandardDropdownBoxComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}








    /// DISPLAY ///
    //================================

    //opened flag
    public opened = true;

    //close event
    @Output('onClose') onClose: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  none
     * @DOes:    sets opened to false
     */
    public close() {
        let model = this;
        //set opened to false
        model.opened = false;
        //await to emit close
        setTimeout(() => {
            //emit close event
            model.onClose.emit();
        }, 200);
    }
    //end close()

    //================================
    /// END DISPLAY ///













    /// INPUTS ///
    //===================================

    //title
    @Input('Title') Title = 'Select';

    //options
    @Input('Options') Options = [];

    //current value
    @Input('Value') Value = '';

    //on select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ();


    /*
     * @Params:  item
     * @Does:    triggers on select
     */
    public handleSelect(item) {
        let model = this;
        //check if has value
        if (item.value == undefined) return;
        //set value
        model.Value = item.value;
        //trigger on select
        model.onSelect.emit(item);
    }
    //end handleSelect()


    //===================================
    /// END INPUTS ///










}