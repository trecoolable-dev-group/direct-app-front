import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideToggleBaseComponent } from './slide-toggle-base.component';

describe('SlideToggleBaseComponent', () => {
  let component: SlideToggleBaseComponent;
  let fixture: ComponentFixture<SlideToggleBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideToggleBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideToggleBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
