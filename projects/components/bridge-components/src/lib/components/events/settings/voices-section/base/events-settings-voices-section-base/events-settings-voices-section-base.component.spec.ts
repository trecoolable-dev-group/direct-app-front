import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsSettingsVoicesSectionBaseComponent } from './events-settings-voices-section-base.component';

describe('EventsSettingsVoicesSectionBaseComponent', () => {
  let component: EventsSettingsVoicesSectionBaseComponent;
  let fixture: ComponentFixture<EventsSettingsVoicesSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsSettingsVoicesSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsSettingsVoicesSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
