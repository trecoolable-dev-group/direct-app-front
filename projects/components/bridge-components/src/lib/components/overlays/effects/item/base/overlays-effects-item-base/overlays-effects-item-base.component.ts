import { Component, OnInit, Input } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-overlays-effects-item-base',
    templateUrl: './overlays-effects-item-base.component.html',
    styleUrls: ['./overlays-effects-item-base.component.css']
})
export class OverlaysEffectsItemBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set effect
        this.setEffect();
    }



    //controls
    public Effects = this.BridgeControl.Effects;
    public Playing = this.BridgeControl.Effects.Playing;





    /// EFFECT ///
    //================================

    //effect item
    public EffectItem = null;

    //effect id
    @Input('EffectId') EffectId = '';


    /*
     * @Params:  none
     * @Does:    sets effect item
     *           fetches effect info
     *           sets overlay params
     */
    public async setEffect() {
        let model = this;
        //check if has effect
        if (!model.EffectId) return;
        //set effect
        let effect = model.Effects.EffectItem(model.EffectId);
        //fetch effect info
        let fetchOp = await effect.fetchInfo();
        //check if has effect
        if (fetchOp.success) {
            //set effect item
            model.EffectItem = effect;
        }
    }
    //end setEffect()



    /*
     * @Params:  none
     * @Does:    removes item from playing
     */
    public handleEnd() {
        let model = this;
        //remove item from list
        model.Playing.removePlaying(model.EffectId);
    }
    //end handleEnd()


    //================================
    /// END EFFECT ///









}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};

//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}