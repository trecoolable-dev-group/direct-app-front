import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsEditBoxInfoSectionBaseComponent } from './effects-edit-box-info-section-base.component';

describe('EffectsEditBoxInfoSectionBaseComponent', () => {
  let component: EffectsEditBoxInfoSectionBaseComponent;
  let fixture: ComponentFixture<EffectsEditBoxInfoSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsEditBoxInfoSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsEditBoxInfoSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
