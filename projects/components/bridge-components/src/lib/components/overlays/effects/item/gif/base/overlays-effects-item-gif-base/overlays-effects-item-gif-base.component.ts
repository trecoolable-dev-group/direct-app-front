import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'bridge-elements-overlays-effects-item-gif-base',
    templateUrl: './overlays-effects-item-gif-base.component.html',
    styleUrls: ['./overlays-effects-item-gif-base.component.css']
})
export class OverlaysEffectsItemGifBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {

    }


    ngAfterViewInit(): void {
        //set image
        this.setImage();
    }





    /// EFFECT ///
    //===========================

    //image element
    @ViewChild('ImageElement') ImageElement: ElementRef;

    //media data
    @Input('Media') Media = '';

    //start
    @Input('Start') Start = 0;

    //end
    @Input('End') End = 5;

    //on end event
    @Output('onEnd') onEnd = new EventEmitter < any > ();


    /*
     * @Params:  none
     * @Does:    sets gif
     */
    public setImage() {
        let model = this;
        //set element
        let element = model.ImageElement.nativeElement;
        //set source
        element.src = model.Media;
        //set timeout to stop playing
        setTimeout(() => {
            //trigger on end
            model.onEnd.emit();
        }, (model.End - model.Start) * 1000);
    }
    //end setImage()

    //===========================
    /// END EFFECT ///


}