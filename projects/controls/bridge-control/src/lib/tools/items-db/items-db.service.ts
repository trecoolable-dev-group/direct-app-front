import Dexie from 'dexie';


export class ItemsDbService {

    constructor(public db_name: string, public db_stores: object, public version: number = 1) {
        //init db
        this.initDb();
    }





    /// DB ///
    //==============================

    //db
    public DB: any = new Dexie('bridge_' + this.db_name);

    /*
     * @Params:  none
     * @Does:    inits db stores
     */
    public async initDb() {
        let model = this;
        //iterate through store keys
        Object.keys(model.db_stores).forEach(key => {
            //add added
            model.db_stores[key] = model.db_stores[key] + ',_x_date_added';
        });
        //set stores
        model.DB.version(model.version).stores(model.db_stores);
        //open database
        model.DB.open().catch(function(err) {
            console.error('LocalDB Error: ' + err);
        });
    }
    //end initDb()


    //==============================
    /// END DB ///












    /// INSERTING ///
    //===================================

    /*
     * @Params:  item, collection
     * @Does:    adds item to db
     */
    public async insertItem(item, collection) {
        let model = this;
        try {
            //add item
            let addOp = await model.DB[collection].add(item);
            //return added
            return reply(true, 'added item', { item: item });
        } catch (error) {
            console.error(error);
            //return error
            return reply(false, 'error inserting item');
        }
    }
    //end insertItem()



    /*
     * @Params:  item, collection
     * @Does:    adds item to db
     */
    public async upsertItem(item, collection) {
        let model = this;
        try {
            //set date added
            item._x_date_added = Date.now();
            //add item
            let addOp = await model.DB[collection].put(item);
            //return added
            return reply(true, 'added item', { item: item });
        } catch (error) {
            console.error(error);
            //return error
            return reply(false, 'error inserting item');
        }
    }
    //end upsertItem()


    //===================================
    /// END INSERTING ///













    /// UPDATE ///
    //===================================

    /*
     * @Params:  key, item, collection
     * @Does:    updates item matching key
     */
    public async updateItem(key, item, collection) {
        let model = this;
        try {
            //update item
            let updateOp = await model.DB[collection].update(key, item);
            //check if updated
            if (!updateOp) return reply(false, 'no items updated');
            //return added
            return reply(true, 'updated item');
        } catch (error) {
            console.error(error);
            //return error
            return reply(false, 'error updating item');
        }
    }
    //end updateItem()


    //===================================
    /// END UPDATE ///














    /// REMOVE ///
    //===================================

    /*
     * @Params:  key, collection
     * @Does:    removes item matching key
     */
    public async deleteItem(key, collection) {
        let model = this;
        try {
            //delete by key
            let deleteOp = await model.DB[collection].delete(key);
            //return deleted
            return reply(true, 'deleted item');
        } catch (error) {
            console.error(error);
            //return error
            return reply(false, 'error deleting item');
        }
    }
    //end deleteItem()


    //===================================
    /// END REMOVE ///
















    /// FETCHING ///
    //===================================

    /*
     * @Params:  key, value, collection, size
     * @Does:    fetches item matching key
     */
    public async findItemsByKey(key, value, collection, size = 5) {
        let model = this;
        try {
            //fetch item
            let fetchOp = await model.DB[collection].where(key).equals(value).limit(size).toArray();
            //return with items
            return reply(true, 'got items', { items: fetchOp });
        } catch (error) {
            console.error(error);
            //return error
            return reply(false, 'error fetching item');
        }
    }
    //end findItemsByKey()



    /*
     * @Params:  collection, size, page
     * @Does:    fetches items limited to size and skipped to page
     */
    public async fetchItemsPaged(collection, size = 5, page = 0) {
        let model = this;
        try {
            //fetch item
            let fetchOp = await model.DB[collection].orderBy('_x_date_added').reverse().offset(page).limit(size).toArray();
            //return with items
            return reply(true, 'got items', { items: fetchOp });
        } catch (error) {
            console.error(error);
            //return error
            return reply(false, 'error fetching item');
        }
    }
    //end fetchItemsPaged()


    //===================================
    /// END FETCHING ///









}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d };
}