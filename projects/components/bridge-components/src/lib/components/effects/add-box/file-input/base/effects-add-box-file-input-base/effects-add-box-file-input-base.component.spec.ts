import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsAddBoxFileInputBaseComponent } from './effects-add-box-file-input-base.component';

describe('EffectsAddBoxFileInputBaseComponent', () => {
  let component: EffectsAddBoxFileInputBaseComponent;
  let fixture: ComponentFixture<EffectsAddBoxFileInputBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsAddBoxFileInputBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsAddBoxFileInputBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
