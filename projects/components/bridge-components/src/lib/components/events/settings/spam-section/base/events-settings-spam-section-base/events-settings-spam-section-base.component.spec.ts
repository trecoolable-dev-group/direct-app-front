import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsSettingsSpamSectionBaseComponent } from './events-settings-spam-section-base.component';

describe('EventsSettingsSpamSectionBaseComponent', () => {
  let component: EventsSettingsSpamSectionBaseComponent;
  let fixture: ComponentFixture<EventsSettingsSpamSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsSettingsSpamSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsSettingsSpamSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
