import { TestBed } from '@angular/core/testing';

import { ItemsDbService } from './items-db.service';

describe('ItemsDbService', () => {
  let service: ItemsDbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemsDbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
