import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardGenderChoicesBaseComponent } from './standard-gender-choices-base.component';

describe('StandardGenderChoicesBaseComponent', () => {
  let component: StandardGenderChoicesBaseComponent;
  let fixture: ComponentFixture<StandardGenderChoicesBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardGenderChoicesBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardGenderChoicesBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
