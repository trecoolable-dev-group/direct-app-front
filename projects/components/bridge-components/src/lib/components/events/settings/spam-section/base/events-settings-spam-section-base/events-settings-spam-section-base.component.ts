import { Component, OnInit } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-events-settings-spam-section-base',
    templateUrl: './events-settings-spam-section-base.component.html',
    styleUrls: ['./events-settings-spam-section-base.component.css']
})
export class EventsSettingsSpamSectionBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {}



    //controls
    public Voices = this.BridgeControl.Voices;


}