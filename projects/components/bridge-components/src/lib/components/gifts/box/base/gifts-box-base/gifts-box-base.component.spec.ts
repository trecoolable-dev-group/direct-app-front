import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftsBoxBaseComponent } from './gifts-box-base.component';

describe('GiftsBoxBaseComponent', () => {
  let component: GiftsBoxBaseComponent;
  let fixture: ComponentFixture<GiftsBoxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftsBoxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftsBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
