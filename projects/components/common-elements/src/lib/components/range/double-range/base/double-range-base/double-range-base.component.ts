import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'common-elements-double-range-base',
    templateUrl: './double-range-base.component.html',
    styleUrls: ['./double-range-base.component.css']
})
export class DoubleRangeBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}





    /// STYLES ///
    //===========================

    //bar height
    @Input('BarHeight') BarHeight = 25;

    //bar class
    @Input('BarClass') BarClass = 'light-round';

    //bar class
    @Input('DotSize') DotSize = '';

    //bar color
    @Input('BarColor') BarColor = 'green';

    //min color
    @Input('MinColor') MinColor = 'red';

    //max color
    @Input('MaxColor') MaxColor = 'yellow';

    //label class
    @Input('LabelClass') LabelClass = '';

    //values class
    @Input('ValuesClass') ValuesClass = '';


    //===========================
    /// END STYLES ///










    /// VALUES ///
    //===============================

    //label
    @Input('Label') Label = '';

    //current top value
    @Input('TopValue') TopValue = 50;

    //current bottom value
    @Input('BottomValue') BottomValue = 25;

    //step value
    @Input('Step') Step = 1;

    //difference
    @Input('Diff') Diff = 5;

    //max value
    @Input('MaxValue') MaxValue = 100;

    //max warn
    @Input('MaxWarn') MaxWarn = -1;

    //min value
    @Input('MinValue') MinValue = 0;

    //min warn
    @Input('MinWarn') MinWarn = -1;

    //===============================
    /// END VALUES ///









    /// CHANGE ///
    //=================================

    //top event
    @Output('onTop') onTop: EventEmitter < any > = new EventEmitter < any > ();

    //bottom event
    @Output('onBottom') onBottom: EventEmitter < any > = new EventEmitter < any > ();

    //top ping flag
    public top_ping = false;

    //bottom ping flag
    public bottom_ping = false;


    /*
     *
     * @Does:    checks if is 
     */
    public async handleTopChange() {
        let model = this;
        //check if is under bottom
        if (model.TopValue <= model.BottomValue) model.TopValue = model.BottomValue + model.Diff;
        //check if is over max warn
        if (model.TopValue > model.MaxWarn && model.MaxWarn != -1) model.TopValue = model.MaxWarn;
        //trigger on top
        model.onTop.emit(model.TopValue);
    }
    //end handleTopChange()



    /*
     *
     * @Does:    checks if is 
     */
    public handleBottomChange() {
        let model = this;
        //check if is under bottom
        if (model.BottomValue > model.TopValue) model.BottomValue = model.TopValue - model.Diff;
        //check if is under min warn
        if (model.BottomValue < model.MinWarn && model.MinWarn != -1) model.BottomValue = model.MinWarn;
        //trigger on bottom
        model.onBottom.emit(model.BottomValue);
    }
    //end handleBottomChange()


    //=================================
    /// END CHANGE ///







}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}