import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
    selector: 'bridge-elements-effects-edit-box-settings-section-duration-input-base',
    templateUrl: './effects-edit-box-settings-section-duration-input-base.component.html',
    styleUrls: ['./effects-edit-box-settings-section-duration-input-base.component.css']
})
export class EffectsEditBoxSettingsSectionDurationInputBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}






    /// EFFECT ///
    //============================

    //effect item
    @Input('EffectItem') EffectItem = null;

    //============================
    /// END EFFECT ///







}