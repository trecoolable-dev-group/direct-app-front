import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'bridge-elements-overlays-effects-item-audio-base',
    templateUrl: './overlays-effects-item-audio-base.component.html',
    styleUrls: ['./overlays-effects-item-audio-base.component.css']
})
export class OverlaysEffectsItemAudioBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {

    }


    ngAfterViewInit(): void {
        //set audio
        this.setAudio();
    }





    /// EFFECT ///
    //===========================

    //audio element
    @ViewChild('AudioElement') AudioElement: ElementRef;

    //media data
    @Input('Media') Media = '';

    //start
    @Input('Start') Start = 0;

    //end
    @Input('End') End = 5;

    //volume
    @Input('Volume') Volume = 0.7;

    //on end event
    @Output('onEnd') onEnd = new EventEmitter < any > ();


    /*
     * @Params:  none
     * @Does:    plays audio
     */
    public setAudio() {
        let model = this;
        //set ended flag
        let ended = false;
        //set element
        let element = model.AudioElement.nativeElement;
        //set source
        element.src = model.Media;
        //set current time
        element.currentTime = model.Start;
        //set volume
        element.volume = (model.Volume < 1) ? model.Volume : 0.7;
        //play media
        element.play();
        //set on ended handelr
        element.onended = () => {
            //check if is ended
            if (ended) return;
            //set ended
            ended = true;
            //stop playing
            element.pause();
            //trigger on end
            model.onEnd.emit();
        }
        //set timeout to stop playing
        setTimeout(() => {
            //check if is ended
            if (ended) return;
            //set ended
            ended = true;
            //stop playing
            element.pause();
            //trigger on end
            model.onEnd.emit();
        }, (model.End - model.Start) * 1000);
    }
    //end setAudio()

    //===========================
    /// END EFFECT ///


}