import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'common-elements-slide-toggle-base',
    templateUrl: './slide-toggle-base.component.html',
    styleUrls: ['./slide-toggle-base.component.css']
})
export class SlideToggleBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}






    /// STYLE ///
    //========================

    //slide height
    @Input('SlideHeight') SlideHeight = 27;

    //dot color
    @Input('DotColor') DotColor = '#f9fdfd';

    //active color
    @Input('ActiveColor') ActiveColor = '#53ca3c';

    //inactive color
    @Input('InactiveColor') InactiveColor = '#3b4454';


    //========================
    /// END STYLE ///











    /// ACTIVE ///
    //====================

    //active
    @Input('Active') Active = true;

    //active output
    @Output('onToggle') onToggle: EventEmitter < any > = new EventEmitter < any > ();


    /*
     * @Params:  none
     * @Does:    toggles active
     *           triggers active toggle
     */
    public toggleActive() {
        let model = this;
        //toggle active
        model.Active = !model.Active;
        //trigger on toggle
        model.onToggle.emit(model.Active);
    }
    //end toggleActive()


    //====================
    /// END ACTIVE ///







}