import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsEditBoxSettingsSectionDurationInputBaseComponent } from './effects-edit-box-settings-section-duration-input-base.component';

describe('EffectsEditBoxSettingsSectionDurationInputBaseComponent', () => {
  let component: EffectsEditBoxSettingsSectionDurationInputBaseComponent;
  let fixture: ComponentFixture<EffectsEditBoxSettingsSectionDurationInputBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsEditBoxSettingsSectionDurationInputBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsEditBoxSettingsSectionDurationInputBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
