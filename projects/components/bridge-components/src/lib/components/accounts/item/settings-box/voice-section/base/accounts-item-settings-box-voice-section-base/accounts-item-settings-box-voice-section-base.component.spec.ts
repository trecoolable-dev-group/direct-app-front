import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsItemSettingsBoxVoiceSectionBaseComponent } from './accounts-item-settings-box-voice-section-base.component';

describe('AccountsItemSettingsBoxVoiceSectionBaseComponent', () => {
  let component: AccountsItemSettingsBoxVoiceSectionBaseComponent;
  let fixture: ComponentFixture<AccountsItemSettingsBoxVoiceSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsItemSettingsBoxVoiceSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsItemSettingsBoxVoiceSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
