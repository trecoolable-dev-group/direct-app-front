import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
    selector: 'bridge-elements-tutorials-mini-box-base',
    templateUrl: './tutorials-mini-box-base.component.html',
    styleUrls: ['./tutorials-mini-box-base.component.css']
})
export class TutorialsMiniBoxBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}




    /// DISPLAY ///
    //================================

    //opened flag
    @Input('Opened') Opened = -1;

    //close event
    @Output('onClose') onClose: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  none
     * @Does:    sets opened to false
     */
    public close() {
        let model = this;
        //set opened to close
        model.Opened = 0;
        //emit close event
        model.onClose.emit();
    }
    //end close()

    //================================
    /// END DISPLAY ///










    /// POSITION ///
    //=================================

    //left box
    @Input('LeftBox') LeftBox = false;

    //right box
    @Input('RightBox') RightBox = false;


    //=================================
    /// END POSITION ///











    /// TUTORIAL ///
    //===================================

    //title
    @Input('Title') Title = '';

    //hint
    @Input('Hint') Hint = '';

    //image
    @Input('Image') Image = '';


    //===================================
    /// END TUTORIAL ///


}