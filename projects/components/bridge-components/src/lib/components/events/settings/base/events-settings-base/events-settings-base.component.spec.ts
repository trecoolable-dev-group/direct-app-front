import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsSettingsBaseComponent } from './events-settings-base.component';

describe('EventsSettingsBaseComponent', () => {
  let component: EventsSettingsBaseComponent;
  let fixture: ComponentFixture<EventsSettingsBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsSettingsBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsSettingsBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
