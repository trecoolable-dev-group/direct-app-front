import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindingBoxYounowInputBaseComponent } from './binding-box-younow-input-base.component';

describe('BindingBoxYounowInputBaseComponent', () => {
  let component: BindingBoxYounowInputBaseComponent;
  let fixture: ComponentFixture<BindingBoxYounowInputBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindingBoxYounowInputBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindingBoxYounowInputBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
