import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'common-elements-standard-text-input-base',
    templateUrl: './standard-text-input-base.component.html',
    styleUrls: ['./standard-text-input-base.component.css']
})
export class StandardTextInputBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}











    /// STYLE ///
    //=================================

    //input style
    @Input('InputClass') InputClass = 'large-height flex-row light-round light-stroke fill-light-shade';

    //input type
    @Input('InputType') InputType = 'text';

    //input text style
    @Input('TextStyle') TextStyle = '';

    //input text style
    @Input('TextClass') TextClass = '';

    //message
    @Input('Message') Message = '';

    //message box style
    @Input('MessageBoxClass') MessageBoxClass = 'med-height';

    //message style
    @Input('MessageClass') MessageClass = '';

    //input status
    @Input('Status') Status = '';


    //=================================
    /// END STYLE ///









    /// EVENTS ///
    //=====================================

    //value input
    @Input('Value') Value = '';

    //placeholder input
    @Input('Placeholder') Placeholder = '';

    //on enter event
    @Output('onEnter') onEnter: EventEmitter < any > = new EventEmitter < any > ();

    //on change event
    @Output('onChange') onChange: EventEmitter < any > = new EventEmitter < any > ();


    /*
     * @Params:  none
     * @Does:    handles enter
     *           emits on enter
     */
    public handleEnter(event) {
        let model = this;
        //trigger on enter
        model.onEnter.emit(model.Value);
    }
    //end handleEnter()



    /*
     * @Params:  none
     * @Does:    handles keydown change
     *           emits on change
     */
    public handleChange(event) {
        let model = this;
        //trigger on on change
        model.onChange.emit(model.Value);
    }
    //end handleChange()


    //=====================================
    /// END EVENTS ///










    /// SPLASH ///
    //============================

    //splash element
    @ViewChild('SplashElement') SplashElement: any;

    //splash color
    @Input('SplashColor') SplashColor = 'rgba(180,183,185,0.3)';

    //splash size
    @Input('SplashSize') SplashSize = 'large';


    /*
     * @Params:  none
     * @Does:    triggers splash
     *           starts matchmaking skip
     */
    public handleClick(event) {
        let model = this;
        //trigger splash
        if (model.SplashElement && model.SplashElement.splash) model.SplashElement.splash(event);


    }
    //end handleClick()


    //=======================================
    /// END SPLASH ///









}