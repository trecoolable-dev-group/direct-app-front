import { Component, OnInit, Input } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-gifts-settings-brackets-section-item-base',
    templateUrl: './gifts-settings-brackets-section-item-base.component.html',
    styleUrls: ['./gifts-settings-brackets-section-item-base.component.css']
})
export class GiftsSettingsBracketsSectionItemBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set bracket
        this.setBracket();
    }



    //controls
    public Gifts = this.BridgeControl.Gifts;
    public Brackets = this.BridgeControl.Gifts.Brackets;









    /// BRACKET ///
    //============================

    //bracket id
    @Input('BracketId') BracketId = '';

    //platform
    @Input('Platform') Platform = '';

    //bracket item
    public BracketItem = null;


    /*
     * @Params:  none
     * @Does:    fetches bracket info
     *           sets bracket
     */
    public async setBracket() {
        let model = this;
        //check if has id
        if (!model.BracketId) return;
        //set bracket
        let bracket = model.Brackets.BracketItem(model.BracketId, model.Platform);
        //fetch info
        let fetchOp = await bracket.fetchInfo();
        //check if has info
        if (fetchOp.success) {
            //set bracket
            model.BracketItem = bracket;
        }
    }
    //end setBracket()



    /*
     * @Params:  none
     * @Does:    adds effect to effects
     */
    public handleEffectSelect(effect_id) {
        let model = this;
        //add effect
        model.BracketItem.addEffect(effect_id);
        //toggle opened
        model.toggleEffectsOpened();
    }
    //end handleEffectSelect()


    //============================
    /// END BRACKET ///














    /// EFFECTS DISPLAY ///
    //=================================

    //overlay effects_opened
    public effects_opened = -1;


    /*
     * @Params:  none
     * @Does:    toggles overlay effects_opened
     */
    public toggleEffectsOpened() {
        let model = this;
        //check if is effects_opened
        if (model.effects_opened != 1) {
            //set effects_opened
            model.effects_opened = 1;
        } else {
            //set closed
            model.effects_opened = 0;
            //await to remove
            setTimeout(() => {
                model.effects_opened = -1;
            }, 200);
        }
    }
    //end toggleEffectsOpened()

    //=================================
    /// END EFFECTS DISPLAY ///





}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};

//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}