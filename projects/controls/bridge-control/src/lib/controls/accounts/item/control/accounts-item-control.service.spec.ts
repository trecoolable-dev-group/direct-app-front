import { TestBed } from '@angular/core/testing';

import { AccountsItemControlService } from './accounts-item-control.service';

describe('AccountsItemControlService', () => {
  let service: AccountsItemControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountsItemControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
