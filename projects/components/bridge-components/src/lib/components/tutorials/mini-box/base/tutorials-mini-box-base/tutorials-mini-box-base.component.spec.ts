import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorialsMiniBoxBaseComponent } from './tutorials-mini-box-base.component';

describe('TutorialsMiniBoxBaseComponent', () => {
  let component: TutorialsMiniBoxBaseComponent;
  let fixture: ComponentFixture<TutorialsMiniBoxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorialsMiniBoxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorialsMiniBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
