import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsAddBoxBaseComponent } from './effects-add-box-base.component';

describe('EffectsAddBoxBaseComponent', () => {
  let component: EffectsAddBoxBaseComponent;
  let fixture: ComponentFixture<EffectsAddBoxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsAddBoxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsAddBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
