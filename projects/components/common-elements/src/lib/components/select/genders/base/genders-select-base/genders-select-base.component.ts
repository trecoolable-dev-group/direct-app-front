import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'common-elements-standard-genders-select-base',
    templateUrl: './genders-select-base.component.html',
    styleUrls: ['./genders-select-base.component.css']
})
export class GendersSelectBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}






    /// INPUTS ///
    //============================

    //label content
    @Input('Label') Label = '';

    //label class
    @Input('LabelClass') LabelClass = '';

    //value class
    @Input('ValueClass') ValueClass = 'bold-text margin-side-med margin-vert';

    //value content
    @Input('Value') Value = '';

    //drop class
    @Input('DropClass') DropClass = 'small-square icon-holder right-carat-icon fill-true-white margin-side-med margin-vert';

    //splash color
    @Input('SplashColor') SplashColor = 'rgba(225,220,235,0.05)';

    //============================
    /// END INPUTS ///














    /// DISPLAY ///
    //=================================

    //overlay opened
    public opened = -1;


    /*
     * @Params:  none
     * @Does:    toggles overlay opened
     */
    public toggleOverlayOpened() {
        let model = this;
        //check if is opened
        if (model.opened != 1) {
            //set opened
            model.opened = 1;
        } else {
            //set closed
            model.opened = 0;
            //await to remove
            setTimeout(() => {
                model.opened = -1;
            }, 200);
        }
    }
    //end toggleOverlayOpened()

    //=================================
    /// END DISPLAY ///


















    /// SELECT ///
    //============================

    //select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  code
     * @Does:    sets value
     *           triggers on select for country code
     *           closes interface
     */
    public handleSelect(value) {
        let model = this;
        //set value
        model.Value = value;
        //emit country code
        model.onSelect.emit(value);
        //close interface
        if (model.opened == 1) model.toggleOverlayOpened();
    }
    //end handleSelect()


    //============================
    /// END SELECT ///












}