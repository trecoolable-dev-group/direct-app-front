import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsSelectBoxItemBaseComponent } from './effects-select-box-item-base.component';

describe('EffectsSelectBoxItemBaseComponent', () => {
  let component: EffectsSelectBoxItemBaseComponent;
  let fixture: ComponentFixture<EffectsSelectBoxItemBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsSelectBoxItemBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsSelectBoxItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
