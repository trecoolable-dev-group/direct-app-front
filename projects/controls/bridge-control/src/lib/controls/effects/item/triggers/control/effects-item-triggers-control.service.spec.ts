import { TestBed } from '@angular/core/testing';

import { EffectsItemTriggersControlService } from './effects-item-triggers-control.service';

describe('EffectsItemTriggersControlService', () => {
  let service: EffectsItemTriggersControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EffectsItemTriggersControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
