import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';


export class EffectsLocalControlService {

    constructor(private EffectsDb: ItemsDbService) {}




    /// DB ///
    //===========================

    //collections
    private LOCAL_EFFECTS_COL = 'local_effects_col';
    private EFFECTS_DATA_COL = 'effects_data_col';
    private EFFECTS_SETTINGS_COL = 'effects_settings_col';
    private EFFECTS_TRIGGERS_COL = 'effects_triggers_col';

    //===========================
    /// END DB ///








    /// LOCAL ///
    //===================================

    //local effects
    public effects = [];



    /*
     * @Params:  more
     * @Does:    fetches local effects
     */
    public async fetchEffects(more = true) {
        let model = this;
        //check if is more and has effects
        if (model.effects.length && !more) return reply(true, 'has effects', { effects: model.effects.slice(0, 25) });
        //make fetch
        let fetchOp: any = await model.EffectsDb.fetchItemsPaged(model.LOCAL_EFFECTS_COL, 25, model.effects.length);
        //check if success
        if (!fetchOp.success) return reply(false, 'could not fetch effects');
        //add to effects
        model.effects = [...model.effects, ...fetchOp.data.items.map(item => { return item.effect_id })];
        //return with effects
        return reply(true, 'got effects', { effects: fetchOp.data.items.map(item => { return item.effect_id }) });
    }
    //end fetchEffects()



    /*
     * @Params:  effect_id, name, type
     * @Does:    adds effect to local
     */
    public async addEffect(effect_id, name, type) {
        let model = this;
        //create effect item
        let effect = {
            effect_id: effect_id,
            name: name,
            type: type,
        }
        //add effect to effects
        let addOp: any = await model.EffectsDb.upsertItem(effect, model.LOCAL_EFFECTS_COL);
        //check added
        if (!addOp.success) return reply(false, 'could not add effect');
        //added effect
        return reply(true, 'added effect', { effect_id: effect_id });
    }
    //end addEffect()



    /*
     * @Params:  effect_id, name, type
     * @Does:    updates effect
     */
    public async updateEffect(effect_id, name, type) {
        let model = this;
        //create effect item
        let effect = {
            effect_id: effect_id,
            name: name,
            type: type,
        }
        //update effect
        let updateOp: any = await model.EffectsDb.upsertItem(effect, model.LOCAL_EFFECTS_COL);
        //check added
        if (!updateOp.success) return reply(false, 'could not update effect');
        //updated effect
        return reply(true, 'updated effect', { effect_id: effect_id });
    }
    //end updateEffect()



    /*
     * @Params:  effect_id
     * @Does:    removes effect
     */
    public async removeEffect(effect_id) {
        let model = this;
        //remove effect
        let removeOp = await model.EffectsDb.deleteItem(effect_id, model.LOCAL_EFFECTS_COL);
        //check added
        if (!removeOp.success) return reply(false, 'could not remove effect');
        //added effect
        return reply(true, 'removed effect');
    }
    //end removeEffect()


    //===================================
    /// END LOCAL ///














    /// SEARCH ///
    //=====================================

    //search results
    public search_results = [];

    //search input
    public search_input = '';


    /*
     * @Params:  name, offset
     * @Does:    search by name
     * @Return:  effects
     */
    public async searchEffects(name = '', offset = 0) {
        let model = this;
        //set blank fetch
        let fetchOp: any = null;
        //check if has length
        if (!name.length) {
            //fetch all
            fetchOp = await model.EffectsDb.fetchItemsPaged(model.LOCAL_EFFECTS_COL, 25, offset);
        } else {
            //has length, search by name
            fetchOp = await model.EffectsDb.DB[model.LOCAL_EFFECTS_COL].where('name').startsWithAnyOfIgnoreCase(name).offset(offset).limit(25).toArray();
            //set formatted result
            fetchOp = reply(true, 'got items', { items: fetchOp });
        }
        //set results
        let results = fetchOp.data.items.map(item => { return item.effect_id });
        //set search results
        model.search_results = [...model.search_results, ...results];
        //return with items
        return reply(true, 'got effects', { effects: results });
    }
    //end searchEffects()

    //=====================================
    /// END SEARCH ///















}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}