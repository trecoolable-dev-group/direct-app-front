import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';
import { ItemsStoreService } from '@controls/bridge-control/src/lib/tools/items-store/items-store.service';
import { EffectsItemControlService } from '../item/control/effects-item-control.service';
import { EffectsSearchControlService } from '../search/control/effects-search-control.service';
import { EffectsPlayingControlService } from '../playing/control/effects-playing-control.service';
import { EffectsLocalControlService } from '../local/control/effects-local-control.service';
import { EffectsTriggersControlService } from '../triggers/control/effects-triggers-control.service';

export class EffectsControlService {

    constructor() {
        let model = this;
        setTimeout(() => {
            //set standard
            model.setStandard();
        }, 2500);
    }




    /// DB ///
    //===========================

    //db name
    private db_name = 'effects_db';

    //collections
    private LOCAL_EFFECTS_COL = 'local_effects_col';
    private EFFECTS_DATA_COL = 'effects_data_col';
    private EFFECTS_SETTINGS_COL = 'effects_settings_col';
    private EFFECTS_TRIGGERS_COL = 'effects_triggers_col';

    //effects db stores
    private db_stores = {
        [this.LOCAL_EFFECTS_COL]: '&effect_id,name,type',
        [this.EFFECTS_DATA_COL]: '&effect_id',
        [this.EFFECTS_SETTINGS_COL]: '&effect_id',
        [this.EFFECTS_TRIGGERS_COL]: '&trigger_id,effect_id,phrase'
    }

    //effects db
    private EffectsDB = new ItemsDbService(this.db_name, this.db_stores, 2);


    //===========================
    /// END DB ///











    /// EFFECTS ///
    //==============================

    //effects
    public EffectItems = new ItemsStoreService('effect_id', 100);


    /*
     * @Params:  effect_id
     * @Does:    fetches effect item
     *           if no item, adds effect to effects
     * @Return:  audience
     */
    public EffectItem(effect_id): EffectsItemControlService {
        let model = this;
        //fetch effect
        let effect = model.EffectItems.fetchItem(effect_id);
        //check if has effect
        if (effect == null) {
            //create new effect
            effect = new EffectsItemControlService(model.EffectsDB, effect_id);
            //add to effects
            model.EffectItems.addItem(effect_id, effect);
        }
        //return with effect
        return effect;
    }
    //end EffectItem()

    //==============================
    /// END EFFECTS ///







    /// LOCAL ///
    //=================================

    public Local = new EffectsLocalControlService(this.EffectsDB);

    //=================================
    /// END LOCAL ///







    /// PLAYING ///
    //==============================

    public Playing = new EffectsPlayingControlService();

    //==============================
    /// END PLAYING ///








    /// SEARCH ///
    //==============================

    public Search = new EffectsSearchControlService(this.EffectsDB);

    //==============================
    /// END SEARCH ///









    /// TRIGGERS ///
    //==============================

    public Triggers = new EffectsTriggersControlService(this.EffectsDB);

    //==============================
    /// END TRIGGERS ///










    /// STANDARD ///
    //===================================

    /*
     * @Params:  none
     * @Does:    adds standard items
     */
    public async setStandard() {
        let model = this;
        //set standard
        let standards = [
            { name: 'ding', trigger: 'oh ding', media: 'https://www.myinstants.com/media/sounds/ding-sound-effect_2.mp3' },
            { name: 'oof', trigger: 'oh oof', media: 'https://www.myinstants.com/media/sounds/uuhhh_evAtIOq.mp3' },
            { name: 'tre yay', trigger: 'tre-yay1234', media: 'https://www.myinstants.com/media/sounds/kids-saying-yay-sound-effect_2.mp3' },
            { name: 'confetti', trigger: 'tre-party', media: 'https://acegif.com/wp-content/uploads/2020/05/confetti.gif' },
            { name: 'share', trigger: '#share', media: 'https://media1.giphy.com/media/2wW2uRmq6PpqZ6mFQr/giphy.gif' },
            { name: 'anotha one', trigger: '', media: 'https://www.myinstants.com/media/sounds/another-one_dPvHt2Z.mp3' },
            { name: 'yessir', trigger: 'yessir', media: 'https://www.myinstants.com/media/sounds/yessirr.mp3' },
            { name: 'minion yess', trigger: 'yesss', media: 'https://media1.tenor.com/images/5d7f4de753efeb7001c480d338c3e2a2/tenor.gif' },
            { name: 'spongebob whyyy', trigger: 'whythooo', media: 'https://media4.giphy.com/media/hv4JHNAMH73tnPMORk/source.gif' },
            { name: 'oh true', trigger: 'oh true', media: 'https://www.myinstants.com/media/sounds/2-chainz-says-true_1.mp3' },
            { name: 'stitch hula', trigger: 'hawaii-bray', media: 'https://media1.giphy.com/media/mPGo386UYlmFy/giphy.gif' },
            { name: 'michael ouch', trigger: 'tre-ouch', media: 'https://media.tenor.com/images/5c735243bd7bf04b6bbe5c5b55b28b72/tenor.gif' },
            { name: 'dolphin', trigger: '', media: 'https://www.myinstants.com/media/sounds/spongebob-dolphin-censor.mp3' },
            { name: 'turtles', trigger: 'mart_001', media: 'https://www.myinstants.com/media/sounds/i-like-turtles.mp3' },
            { name: 'oh man', trigger: '', media: 'https://www.myinstants.com/media/sounds/oh-man-not-again.mp3' },
            { name: 'bruh', trigger: 'tre-bruh987', media: 'https://www.myinstants.com/media/sounds/movie_1_C2K5NH0.mp3' },
            { name: 'big rona', trigger: '', media: 'https://www.myinstants.com/media/sounds/corona.mp3' },
            { name: '7woe7', trigger: '7woe7-James', media: 'https://www.myinstants.com/media/sounds/09037.mp3' },
        ];
        //iterate to add
        for (let i = standards.length - 1; i >= 0; i--) {
            //set item
            let item = standards[i];
            //add standard
            await model.addStandard(item.name, item.name, item.trigger, item.media);
        }
        return true;
    }
    //end setStandard()



    /*
     * @Params:  iden, name, trigger, media
     * @Does:    adds standard items
     */
    public async addStandard(iden, name, trigger, media) {
        let model = this;
        try {
            //get extension
            let extension = media.split('.').pop();
            //set type
            let type = 'audio';
            //check if is video
            if (extension.includes('mp4')) type = 'video';
            //check if is gif
            if (extension.includes('gif')) type = 'gif';
            //create item
            let effect = model.EffectItem(iden);
            //fetch info
            let fetchOp = await effect.fetchInfo();
            //check if has effect
            if (fetchOp.success == false && fetchOp.data.info == null) {
                //does not have effect, set name
                effect.info_inputs.name = name;
                //set media
                effect.info_inputs.media = media;
                //set type
                effect.info_inputs.type = type;
                //save info
                effect.saveInfo();
                //set volume
                effect.Settings.settings_inputs.volume = 0.6;
                //check if is gif or video
                if (type == 'gif' || type == 'video') {
                    //set settings
                    effect.Settings.settings_inputs.offset_x = 0;
                    effect.Settings.settings_inputs.offset_y = 50;
                    //set scale
                    effect.Settings.settings_inputs.scale_x = 45;
                    effect.Settings.settings_inputs.scale_y = 50;
                }
                //save settings
                effect.Settings.saveSettings();
                //set trigger
                if (trigger.length) effect.Triggers.addTrigger(trigger);
            }
            //check if is in local
            let localOp: any = await model.EffectsDB.findItemsByKey('effect_id', iden, model.LOCAL_EFFECTS_COL, 1);
            //check if has item
            if (!localOp.data.items.length) {
                //create effect item
                let item = {
                    effect_id: iden,
                    name: name,
                    type: type,
                }
                //add effect to effects
                model.EffectsDB.upsertItem(item, model.LOCAL_EFFECTS_COL);
                //add to local
                model.Local.effects.unshift(iden);
            }
            return;
        } catch (error) {
            return;
        }
    }
    //end addStandard()

    //===================================
    /// END STANDARD ///





}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}