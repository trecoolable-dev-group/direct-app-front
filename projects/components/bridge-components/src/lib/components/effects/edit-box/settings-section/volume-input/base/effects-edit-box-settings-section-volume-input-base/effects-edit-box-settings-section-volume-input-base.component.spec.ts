import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsEditBoxSettingsSectionVolumeInputBaseComponent } from './effects-edit-box-settings-section-volume-input-base.component';

describe('EffectsEditBoxSettingsSectionVolumeInputBaseComponent', () => {
  let component: EffectsEditBoxSettingsSectionVolumeInputBaseComponent;
  let fixture: ComponentFixture<EffectsEditBoxSettingsSectionVolumeInputBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsEditBoxSettingsSectionVolumeInputBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsEditBoxSettingsSectionVolumeInputBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
