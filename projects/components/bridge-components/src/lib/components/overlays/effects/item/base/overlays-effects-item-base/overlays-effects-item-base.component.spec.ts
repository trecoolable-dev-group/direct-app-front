import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaysEffectsItemBaseComponent } from './overlays-effects-item-base.component';

describe('OverlaysEffectsItemBaseComponent', () => {
  let component: OverlaysEffectsItemBaseComponent;
  let fixture: ComponentFixture<OverlaysEffectsItemBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaysEffectsItemBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaysEffectsItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
