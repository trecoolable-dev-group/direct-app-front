import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsListChatItemBaseComponent } from './events-list-chat-item-base.component';

describe('EventsListChatItemBaseComponent', () => {
  let component: EventsListChatItemBaseComponent;
  let fixture: ComponentFixture<EventsListChatItemBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsListChatItemBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsListChatItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
