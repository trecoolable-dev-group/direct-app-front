import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'common-elements-genders-select-overlay-option-item',
    templateUrl: './genders-select-overlay-option-item.component.html',
    styleUrls: ['./genders-select-overlay-option-item.component.css']
})
export class GendersSelectOverlayOptionItemComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}








    /// INFO ///
    //==============================

    //gender label
    @Input('Label') Label = '';

    //gender value
    @Input('Value') Value = '';

    //active flag
    @Input('Active') Active = false;

    //select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  event
     * @Does:    sets splash
     *           triggers on select
     */
    public selectGender(event) {
        let model = this;
        //splash
        model.splash(event);
        //trigger select event
        model.onSelect.emit({ label: model.Label, value: model.Value });
    }
    //end selectGender()


    //==============================
    /// END INFO ///


















    /// SPLASH ///
    //=======================================

    //splash element
    @ViewChild('SplashElement') SplashElement: any;


    /*
     * @Params:  event
     * @Does:    triggers splash
     */
    public splash(event) {
        let model = this;
        //trigger splash
        if (model.SplashElement && model.SplashElement.splash) model.SplashElement.splash(event);
    }
    //end handleClick()


    //=======================================
    /// END SPLASH ///














}