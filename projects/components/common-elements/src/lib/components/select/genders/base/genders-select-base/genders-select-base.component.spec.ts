import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GendersSelectBaseComponent } from './genders-select-base.component';

describe('GendersSelectBaseComponent', () => {
  let component: GendersSelectBaseComponent;
  let fixture: ComponentFixture<GendersSelectBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GendersSelectBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GendersSelectBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
