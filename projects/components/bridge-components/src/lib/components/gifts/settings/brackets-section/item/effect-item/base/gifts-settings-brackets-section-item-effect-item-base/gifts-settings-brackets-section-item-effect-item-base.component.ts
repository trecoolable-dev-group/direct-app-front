import { Component, OnInit, Input } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-gifts-settings-brackets-section-item-effect-item-base',
    templateUrl: './gifts-settings-brackets-section-item-effect-item-base.component.html',
    styleUrls: ['./gifts-settings-brackets-section-item-effect-item-base.component.css']
})
export class GiftsSettingsBracketsSectionItemEffectItemBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set effect
        this.setEffect();
    }


    //controls
    public Effects = this.BridgeControl.Effects;





    /// BRACKET ///
    //===========================

    //bracket item
    @Input('BracketItem') BracketItem = null;

    //===========================
    /// END BRACKET ///







    /// EFFECT ///
    //==============================

    //effect id
    @Input('EffectId') EffectId = '';

    //effect item
    public EffectItem = null;


    /*
     * @Params:  none
     * @Does:    fetches effect info
     *           sets effect    
     */
    public async setEffect() {
        let model = this;
        //check has id
        if (!model.EffectId) return;
        //set effect item
        let effect = model.BridgeControl.Effects.EffectItem(model.EffectId);
        //fetch info
        let fetchOp: any = await effect.fetchInfo();
        //check if got info
        if (fetchOp.success) {
            //set effect
            model.EffectItem = effect;
        }
    }
    //end setEffect()

    //==============================
    /// END EFFECT ///


}