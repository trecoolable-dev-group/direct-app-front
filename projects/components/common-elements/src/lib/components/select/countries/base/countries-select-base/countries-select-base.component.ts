import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'common-elements-standard-countries-select-base',
    templateUrl: './countries-select-base.component.html',
    styleUrls: ['./countries-select-base.component.css']
})
export class CountriesSelectBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}







    /// INPUTS ///
    //============================

    //dropdown class
    @Input('DropdownClass') DropdownClass = 'large-height flex-row margin-med';

    //label content
    @Input('Label') Label = '';

    //label class
    @Input('LabelClass') LabelClass = '';

    //icon class
    @Input('IconClass') IconClass = 'std-square contained margin-side-med';

    //icon type
    @Input('IconType') IconType = '';

    //icon source
    @Input('IconSrc') IconSrc = '';

    //value class
    @Input('ValueClass') ValueClass = 'bold-text margin-side-med margin-vert';

    //drop class
    @Input('DropClass') DropClass = 'small-square icon-holder right-carat-icon fill-true-white margin-side-med margin-vert';

    //value content
    @Input('Value') Value = '';

    //splash color
    @Input('SplashColor') SplashColor = 'rgba(225,220,235,0.05)';

    //============================
    /// END INPUTS ///














    /// DISPLAY ///
    //=================================

    //overlay opened
    public opened = -1;


    /*
     * @Params:  none
     * @Does:    toggles overlay opened
     */
    public toggleOverlayOpened() {
        let model = this;
        //check if is opened
        if (model.opened != 1) {
            //set opened
            model.opened = 1;
        } else {
            //set closed
            model.opened = 0;
            //await to remove
            setTimeout(() => {
                model.opened = -1;
            }, 200);
        }
    }
    //end toggleOverlayOpened()

    //=================================
    /// END DISPLAY ///


















    /// SELECT ///
    //============================

    //select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  code
     * @Does:    sets value
     *           triggers on select for country code
     *           closes interface
     */
    public handleSelect(value) {
        let model = this;
        //set value
        model.Value = value;
        //emit country code
        model.onSelect.emit(value);
        //close interface
        if (model.opened == 1) model.toggleOverlayOpened();
    }
    //end handleSelect()


    //============================
    /// END SELECT ///














}