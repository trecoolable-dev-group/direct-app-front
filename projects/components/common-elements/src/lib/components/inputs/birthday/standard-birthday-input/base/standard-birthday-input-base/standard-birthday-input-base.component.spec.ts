import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardBirthdayInputBaseComponent } from './standard-birthday-input-base.component';

describe('StandardBirthdayInputBaseComponent', () => {
  let component: StandardBirthdayInputBaseComponent;
  let fixture: ComponentFixture<StandardBirthdayInputBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardBirthdayInputBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardBirthdayInputBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
