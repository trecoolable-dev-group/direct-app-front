import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftsSettingsBracketsSectionItemEffectItemBaseComponent } from './gifts-settings-brackets-section-item-effect-item-base.component';

describe('GiftsSettingsBracketsSectionItemEffectItemBaseComponent', () => {
  let component: GiftsSettingsBracketsSectionItemEffectItemBaseComponent;
  let fixture: ComponentFixture<GiftsSettingsBracketsSectionItemEffectItemBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftsSettingsBracketsSectionItemEffectItemBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftsSettingsBracketsSectionItemEffectItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
