import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardDropdownBoxComponent } from './standard-dropdown-box.component';

describe('StandardDropdownBoxComponent', () => {
  let component: StandardDropdownBoxComponent;
  let fixture: ComponentFixture<StandardDropdownBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardDropdownBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardDropdownBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
