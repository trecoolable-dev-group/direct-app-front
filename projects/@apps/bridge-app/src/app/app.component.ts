import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'bridge-app-root',
    templateUrl: './app.component.html',
    styleUrls: [
        './app.component.css',
        '../styles/commons.css',
        '../styles/dark.css',
        '../icons/icons.css'
    ],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    title = 'MattyBot by TreCoolable!';
}