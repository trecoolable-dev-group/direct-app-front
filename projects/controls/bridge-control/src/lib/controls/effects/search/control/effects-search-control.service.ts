import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';

export class EffectsSearchControlService {

    constructor(private EffectsDb: ItemsDbService) {}




    /// DB ///
    //===========================

    //collections
    private LOCAL_EFFECTS_COL = 'local_effects_col';
    private EFFECTS_DATA_COL = 'effects_data_col';
    private EFFECTS_SETTINGS_COL = 'effects_settings_col';
    private EFFECTS_TRIGGERS_COL = 'effects_triggers_col';

    //===========================
    /// END DB ///











    /// SEARCH ///
    //============================

    //results
    public results = [];

    //name input
    public name_input = '';

    //types filter
    public types = [
        { name: 'audio', active: 1 },
        { name: 'video', active: 1 },
        { name: 'gif', active: 1 },
    ];

    //previous search string
    private previous = '';


    /*
     * @Params:  more
     * @Does:    fetches results based on search
     * @Return:  effects
     */
    public async searchEffects(more = true) {
        let model = this;
        //set search size
        let size = 25;
        //create search string
        let search_string = model.name_input + JSON.stringify(model.types);
        //check if is same
        if (search_string == model.previous && !more) return reply(true, 'same query', { effects: model.results.slice(0, size) });
        //check if is not same
        if (search_string != model.previous) model.results = [];
        //set type filters
        let types = model.types.map(item => { return item.name });
        //set blank search
        let search = null;
        //fetch effects
        search = await model.EffectsDb.DB[model.LOCAL_EFFECTS_COL].where('name').startsWithAnyOfIgnoreCase([model.name_input]).offset(model.results.length).limit(size).and(item => { return (types.indexOf(item.type) != -1) }).toArray();
        //set results
        let results = search.map(item => { return item.effect_id });
        //add results
        model.results = [...model.results, ...search.map(item => { return item.effect_id })];
        //return with results
        return reply(true, 'got effects', { effects: results });
    }
    //end searchEffects()


    //============================
    /// END SEARCH ///





}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}