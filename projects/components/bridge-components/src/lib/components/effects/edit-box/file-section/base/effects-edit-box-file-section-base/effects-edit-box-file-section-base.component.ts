import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bridge-elements-effects-edit-box-file-section-base',
  templateUrl: './effects-edit-box-file-section-base.component.html',
  styleUrls: ['./effects-edit-box-file-section-base.component.css']
})
export class EffectsEditBoxFileSectionBaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
