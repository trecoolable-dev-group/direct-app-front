import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
    selector: 'bridge-elements-effects-edit-box-info-section-base',
    templateUrl: './effects-edit-box-info-section-base.component.html',
    styleUrls: ['./effects-edit-box-info-section-base.component.css']
})
export class EffectsEditBoxInfoSectionBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}




    /// EFFECT ///
    //============================

    //effect item
    @Input('EffectItem') EffectItem = null;

    //============================
    /// END EFFECT ///









    /// SAVING ///
    //==============================

    //loading lottie
    @ViewChild('LoadingLottie') LoadingLottie;

    //save status
    public status = '';


    /*
     * @Params:  none
     * @Does:    saves inputs
     */
    public async saveInputs() {
        let model = this;
        //check if is busy
        if (model.status == 'busy') return;
        //set status
        model.status = 'busy';
        //set loading lottie
        model.LoadingLottie.playLoading();
        //save effect
        let submitOp = await model.EffectItem.saveInfo();
        //check if success
        if (!submitOp.success) {
            //set error status
            model.status = 'error';
            //did not add tier, play error
            model.LoadingLottie.playError();
        } else {
            //set success status
            model.status = 'success';
            //added tier, play success
            model.LoadingLottie.playSuccess();
        }
        //wait to unset status
        await wait(2000);
        //unset submit status
        model.status = '';
    }
    //end saveInputs()

    //==============================
    /// END SAVING ///





}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};

//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}