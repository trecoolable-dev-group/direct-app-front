import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';
import { AccountsItemControlService } from '@controls/bridge-control/src/lib/controls/accounts/item/control/accounts-item-control.service';

@Component({
    selector: 'bridge-elements-accounts-item-settings-box-base',
    templateUrl: './accounts-item-settings-box-base.component.html',
    styleUrls: ['./accounts-item-settings-box-base.component.css']
})
export class AccountsItemSettingsBoxBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set account
        this.setAccount();
    }


    //controls
    public Accounts = this.BridgeControl.Accounts;






    /// DISPLAY ///
    //============================

    //opened flag
    @Input('Opened') Opened = -1;

    //close output
    @Output('onClose') onClose = new EventEmitter < any > ();


    /*
     * @Params:  none
     * @Does:    triggers close
     */
    public close() {
        let model = this;
        //trigger close
        model.onClose.emit();
    }
    //end close()

    //============================
    /// END DISPLAY ///













    /// SECTIONS ///
    //=============================

    //current section
    public section = 'voice';

    //sections
    public sections = [
        'voice',
        'chat',
        'effects'
    ];


    /*
     * @Params:  section
     * @Does:    changes section
     */
    public async changeSection(section) {
        let model = this;
        //set section
        model.section = section;
    }
    //end changeSection()


    //=============================
    /// END SECTIONS ///













    /// ACCOUNT ///
    //==============================

    //platform
    @Input('Platform') Platform = '';

    //platform id
    @Input('PlatformId') PlatformId = '';

    //account
    public Account: AccountsItemControlService = null;


    /*
     * @Params:  none
     * @Does:    fetches account
     *           sets account
     */
    public async setAccount() {
        let model = this;
        //set account
        let account = model.Accounts.AccountItem(model.PlatformId, model.Platform);
        //fetch account info
        let fetchOp = await account.fetchInfo();
        //check if got info
        if (fetchOp.success) {
            //set account
            model.Account = account;
        }
    }
    //end setAccount()



    /*
     * @Params:  none
     * @Does:    checks if is highlight
     *           sets highlight
     *           saves voice settings
     */
    public toggleHIghlight() {
        let model = this;
        //check if is highlighted
        if (model.Account.Voice.settings.highlight != 1) {
            //set highlight
            model.Account.Voice.settings_inputs.highlight = 1;
        } else {
            //set unhighlight
            model.Account.Voice.settings_inputs.highlight = 0;
        }
        //save voice settings
        model.Account.Voice.saveSettings();
    }
    //end toggleHighlight()


    //==============================
    /// END ACCOUNT ///






}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d };
}


function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}