import { TestBed } from '@angular/core/testing';

import { GuestsControlService } from './guests-control.service';

describe('GuestsControlService', () => {
  let service: GuestsControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GuestsControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
