import { Injectable, Inject } from '@angular/core';
import { EventsControlService } from './controls/events/control/events-control.service';
import { AudienceControlService } from './controls/audience/control/audience-control.service';
import { AccountsControlService } from './controls/accounts/control/accounts-control.service';
import { EffectsControlService } from './controls/effects/control/effects-control.service';
import { VoicesControlService } from './controls/voices/control/voices-control.service';
import { GiftsControlService } from './controls/gifts/control/gifts-control.service';
import { TutorialsControlService } from './controls/tutorials/control/tutorials-control.service';

@Injectable({
    providedIn: 'root'
})
export class BridgeControlService {

    constructor(@Inject('env') private ENV: any) {
        //set externals
        this.setEventsExternals();
    }




    /// ACCOUNTS ///
    //=====================================

    public Accounts = new AccountsControlService();

    //=====================================
    /// END ACCOUNTS ///












    /// AUDIENCE ///
    //=====================================

    public Audience = new AudienceControlService();

    //=====================================
    /// END AUDIENCE ///











    /// EFFECTS ///
    //=====================================

    public Effects = new EffectsControlService();

    //=====================================
    /// END EFFECTS ///











    /// EVENTS ///
    //==================================

    public Events = new EventsControlService();


    /*
     * @Params:  none
     * @Does:    sets events externals
     */
    public setEventsExternals() {
        let model = this;
        //set on event
        model.Events.onEvent = (event) => {
            //handle event
            model.handleEvent(event);
        };
    }
    //end setEventsExternals()



    /*
     * @Params:   event
     * @Does:     handles events and routes info to controls
     */
    public async handleEvent(event) {
        let model = this;
        console.log(event);
        //set account
        model.setEventAccountInfo(event);
        //check if is chat
        if (event.type && event.type == 'chat') {
            //handle chat event
            model.handleChatEvent(event);
        }
        //check if is gift
        if (event.type && event.type == 'gift') {
            //handle gift event
            model.handleGiftEvent(event);
        }
    }
    //end handleEvent()



    /*
     * @Params:  none
     * @Does:    checks if chat is trigger
     *           if is trigger, plays trigger
     */
    public async handleChatEvent(event) {
        let model = this;
        //set message
        let content = event.content;
        //is chat fetch trigger
        let triggerOp: any = await model.Effects.Triggers.fetchEffectsByPhrase(content.slice(0, 32));
        //check if success and has trigger
        if (triggerOp.success && triggerOp.data.effects && triggerOp.data.effects.length) {
            //set effect index
            let idx = Math.floor(Math.random() * triggerOp.data.effects.length) + 1;
            //is effect trigger, play effect
            let effect_id = triggerOp.data.effects[idx - 1];
            //play effect
            model.Effects.Playing.addPlaying(effect_id);
            return;
        }
        //set account data
        let account = model.Accounts.AccountItem(event.platform_id, event.platform);
        //fetch account info
        let accountOp = await account.fetchInfo();
        //check if is highlighted
        if (model.Voices.settings.voices_enabled || account.Voice.settings.highlight) {
            //set speech content
            let speech_content = content;
            //check has names enabled
            if (model.Voices.settings.names_enabled) content = account.info.name.slice(0, 32) + ' says ' + content;
            //read content
            model.Voices.addSpeechItem(content, account.Voice.settings);
        }
        //add to events
        model.Events.list.push(event);
        //scroll events
        model.Events.scrollEvents();
    }
    //end handleChatEvent()



    public async handleGiftEvent(event) {
        let model = this;
        //add to events
        model.Events.list.push(event);
        //check if has value
        if (event.content && event.content.value) {
            //fetch bracket for item
            let bracketOp: any = await model.Gifts.Brackets.fetchBracketByValue(event.platform, event.content.value);
            //check if has bracket
            if (bracketOp.success && bracketOp.data.bracket != null) {
                //check has effect
                if (bracketOp.data.bracket.effects.length) {
                    //set effect index
                    let idx = Math.floor(Math.random() * bracketOp.data.bracket.effects.length) + 1;
                    //is effect trigger, play effect
                    let effect_id = bracketOp.data.bracket.effects[idx - 1];
                    //add to playing
                    model.Effects.Playing.addPlaying(effect_id);
                }
            }
        }
        //scroll events
        model.Events.scrollEvents();
    }



    public async setEventAccountInfo(event) {
        let model = this;
        //set account data
        let account = model.Accounts.AccountItem(event.platform_id, event.platform);
        //check if is younow
        if (event.platform == 'younow') {
            //set username
            account.info.name = event.platform_info.username;
            //set profile pic
            account.info.profile_pic = 'https://ynassets.younow.com/user/live/' + event.platform_id + '/' + event.platform_id + '.jpg';
            //save info
            account.saveInfo();
        }
    }


    //==================================
    /// END EVENTS ///








    /// GIFTS ///
    //===============================

    public Gifts = new GiftsControlService();

    //===============================
    /// END GIFTS ///








    /// TUTORIALS ///
    //===============================

    public Tutorials = new TutorialsControlService();

    //===============================
    /// END TUTORIALS ///











    /// VOICES ///
    //===============================

    public Voices = new VoicesControlService();

    //===============================
    /// END VOICES ///





}