import { EventsYounowControlService } from '../younow/control/events-younow-control.service';


export class EventsControlService {

    constructor() {
        //set younow
        this.setYounowExternals();
    }





    /// EXTERNALS ///
    //==================================

    /*
     * @Params:  event
     * @Does:    passes event to external
     */
    public onEvent(event) {
        console.log('on event');
    }
    //end onEvent()


    //==================================
    /// END EXTERNALS ///







    /// LIST ///
    //============================

    //events list
    public list = [];

    //chat element
    public EventsListElement = null;


    /*
     * @Params:  none
     * @Does:    scrolls events
     */
    public scrollEvents() {
        let model = this;
        //scroll events
        setTimeout(() => {
            //set scroll down
            model.EventsListElement.nativeElement.scrollTop = model.EventsListElement.nativeElement.scrollHeight;
        }, 200);
    }
    //end scrollEvents()

    //============================
    /// END LIST ///











    /// YOUNOW ///
    //==============================

    public Younow = new EventsYounowControlService();


    /*
     * @Params:  none
     * @Does:    sets externals for younow
     */
    public setYounowExternals() {
        let model = this;
        //set on event
        model.Younow.onEvent = (event) => {
            //pass to external
            model.onEvent(event);
        };
    }
    //end setYounowExternals()

    //==============================
    /// END YOUNOW ///




}