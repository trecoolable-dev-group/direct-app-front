import { TestBed } from '@angular/core/testing';

import { EffectsSearchControlService } from './effects-search-control.service';

describe('EffectsSearchControlService', () => {
  let service: EffectsSearchControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EffectsSearchControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
