import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'bridge-elements-effects-edit-box-info-section-file-input-base',
    templateUrl: './effects-edit-box-info-section-file-input-base.component.html',
    styleUrls: ['./effects-edit-box-info-section-file-input-base.component.css']
})
export class EffectsEditBoxInfoSectionFileInputBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}




    /// EFFECT ///
    //============================

    //effect item
    @Input('EffectItem') EffectItem = null;

    //============================
    /// END EFFECT ///












    /// FILE ///
    //===============================

    //file input element
    @ViewChild('FileInput') FileInput: ElementRef;


    /*
     * @Params:  none
     * @Does:    triggers file input
     */
    public triggerFileInput() {
        //trigger file input
        this.FileInput.nativeElement.click();
    }
    //end triggerFileInput()



    /*
     * @Params:  event
     * @Does:    checks if file is too large
     *           if too large, sets tier icon error
     *           if filesize valid, sets tier icon input
     */
    public handleFileInput(evt) {
        let model = this;
        //set file
        let file = evt.target.files[0];
        //set file input
        model.EffectItem.handleFileInput(file);
    }
    //end handleFileInput()


    //===============================
    /// END FILE ///



}