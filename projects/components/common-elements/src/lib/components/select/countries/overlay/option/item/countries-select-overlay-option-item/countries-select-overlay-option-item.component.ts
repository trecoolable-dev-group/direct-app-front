import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'common-elements-countries-select-overlay-option-item',
    templateUrl: './countries-select-overlay-option-item.component.html',
    styleUrls: ['./countries-select-overlay-option-item.component.css']
})
export class CountriesSelectOverlayOptionItemComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}






    /// INFO ///
    //==============================

    //country code
    @Input('Code') Code = '';

    //active flag
    @Input('Active') Active = false;

    //select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  event
     * @Does:    sets splash
     *           triggers on select
     */
    public selectCountry(event) {
        let model = this;
        //splash
        model.splash(event);
        //trigger select event
        model.onSelect.emit(model.Code);
    }
    //end selectCountry()


    //==============================
    /// END INFO ///


















    /// SPLASH ///
    //=======================================

    //splash element
    @ViewChild('SplashElement') SplashElement: any;


    /*
     * @Params:  event
     * @Does:    triggers splash
     */
    public splash(event) {
        let model = this;
        //trigger splash
        if (model.SplashElement && model.SplashElement.splash) model.SplashElement.splash(event);
    }
    //end handleClick()


    //=======================================
    /// END SPLASH ///










}