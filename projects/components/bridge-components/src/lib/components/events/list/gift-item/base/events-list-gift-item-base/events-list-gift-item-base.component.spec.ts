import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsListGiftItemBaseComponent } from './events-list-gift-item-base.component';

describe('EventsListGiftItemBaseComponent', () => {
  let component: EventsListGiftItemBaseComponent;
  let fixture: ComponentFixture<EventsListGiftItemBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsListGiftItemBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsListGiftItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
