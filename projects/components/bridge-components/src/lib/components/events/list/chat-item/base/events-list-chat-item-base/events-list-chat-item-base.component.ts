import { Component, OnInit, Input } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';
import { AccountsItemControlService } from '@controls/bridge-control/src/lib/controls/accounts/item/control/accounts-item-control.service';

@Component({
    selector: 'bridge-elements-events-list-chat-item-base',
    templateUrl: './events-list-chat-item-base.component.html',
    styleUrls: ['./events-list-chat-item-base.component.css']
})
export class EventsListChatItemBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set account
        this.setAccount();
    }


    //controls
    public Accounts = this.BridgeControl.Accounts;






    /// EVENT ///
    //============================

    //event
    @Input('EventItem') EventItem = null;

    //============================
    /// END EVENT ///











    /// ACCOUNT ///
    //==============================

    //account
    public Account: AccountsItemControlService = null;


    /*
     * @Params:  none
     * @Does:    fetches account
     *           sets account
     */
    public async setAccount() {
        let model = this;
        //set account
        let account = model.Accounts.AccountItem(model.EventItem.platform_id, model.EventItem.platform);
        //fetch account info
        let fetchOp = await account.fetchInfo();
        //check if got info
        if (fetchOp.success) {
            //set account
            model.Account = account;
        }
    }
    //end setAccount()



    /*
     * @Params:  none
     * @Does:    checks if is highlight
     *           sets highlight
     *           saves voice settings
     */
    public toggleHIghlight() {
        let model = this;
        //check if is highlighted
        if (model.Account.Voice.settings.highlight != 1) {
            //set highlight
            model.Account.Voice.settings_inputs.highlight = 1;
        } else {
            //set unhighlight
            model.Account.Voice.settings_inputs.highlight = 0;
        }
        //save voice settings
        model.Account.Voice.saveSettings();
    }
    //end toggleHighlight()


    //==============================
    /// END ACCOUNT ///








    /// ACCOUNT SETTINGS ///
    //==============================

    //account settings opened
    public settings_opened = -1;


    /*
     * @Params:  none
     * @Does:    toggles settings opened
     */
    public async toggleSettingsOpened() {
        let model = this;
        //check if settings is opened
        if (model.settings_opened != 1) {
            //set opened
            model.settings_opened = 1;
        } else {
            //set to closed
            model.settings_opened = 0;
            //await to remove
            await wait(200);
            //remove settings
            model.settings_opened = -1;
        }
    }
    //end toggleSettingsOpened()


    //==============================
    /// END ACCOUNT SETTINGS ///










}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d };
}


function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}