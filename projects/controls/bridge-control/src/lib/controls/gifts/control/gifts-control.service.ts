import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';
import { ItemMessageService } from '@controls/bridge-control/src/lib/tools/item-message/item-message.service';
import { GiftsBracketsControlService } from '../brackets/control/gifts-brackets-control.service';


export class GiftsControlService {

    constructor() {}





    /// DB ///
    //===========================

    //db name
    private db_name = 'gifts_db';

    //collections
    private GIFTS_BRACKETS_COL = 'gifts_brackets_col';

    //voices db stores
    private db_stores = {
        [this.GIFTS_BRACKETS_COL]: 'bracket_id,platform,min,max',
    }

    //effects db
    private GiftsDB = new ItemsDbService(this.db_name, this.db_stores, 2);


    //===========================
    /// END DB ///










    /// BRACKETS ///
    //================================

    public Brackets = new GiftsBracketsControlService(this.GiftsDB);

    //================================
    /// END BRACKETS ///





}