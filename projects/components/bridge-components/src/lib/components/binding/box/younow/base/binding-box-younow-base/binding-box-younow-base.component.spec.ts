import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindingBoxYounowBaseComponent } from './binding-box-younow-base.component';

describe('BindingBoxYounowBaseComponent', () => {
  let component: BindingBoxYounowBaseComponent;
  let fixture: ComponentFixture<BindingBoxYounowBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindingBoxYounowBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindingBoxYounowBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
