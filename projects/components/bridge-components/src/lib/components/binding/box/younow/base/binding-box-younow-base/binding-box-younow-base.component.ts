import { Component, OnInit } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-binding-box-younow-base',
    templateUrl: './binding-box-younow-base.component.html',
    styleUrls: ['./binding-box-younow-base.component.css']
})
export class BindingBoxYounowBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {}



    //controls
    public Younow = this.BridgeControl.Events.Younow;


}