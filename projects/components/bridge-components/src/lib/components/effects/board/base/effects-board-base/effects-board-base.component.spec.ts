import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsBoardBaseComponent } from './effects-board-base.component';

describe('EffectsBoardBaseComponent', () => {
  let component: EffectsBoardBaseComponent;
  let fixture: ComponentFixture<EffectsBoardBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsBoardBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsBoardBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
