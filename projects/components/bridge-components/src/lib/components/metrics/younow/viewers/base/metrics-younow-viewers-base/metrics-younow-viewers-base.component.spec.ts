import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricsYounowViewersBaseComponent } from './metrics-younow-viewers-base.component';

describe('MetricsYounowViewersBaseComponent', () => {
  let component: MetricsYounowViewersBaseComponent;
  let fixture: ComponentFixture<MetricsYounowViewersBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricsYounowViewersBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricsYounowViewersBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
