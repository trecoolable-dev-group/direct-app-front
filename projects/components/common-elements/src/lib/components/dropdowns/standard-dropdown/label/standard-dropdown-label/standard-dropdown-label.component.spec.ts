import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardDropdownLabelComponent } from './standard-dropdown-label.component';

describe('StandardDropdownLabelComponent', () => {
  let component: StandardDropdownLabelComponent;
  let fixture: ComponentFixture<StandardDropdownLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardDropdownLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardDropdownLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
