import { TestBed } from '@angular/core/testing';

import { MetricsYounowControlService } from './metrics-younow-control.service';

describe('MetricsYounowControlService', () => {
  let service: MetricsYounowControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MetricsYounowControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
