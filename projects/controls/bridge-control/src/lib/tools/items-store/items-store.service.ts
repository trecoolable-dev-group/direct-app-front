import { Injectable } from '@angular/core';


export class ItemsStoreService {

    constructor(public key: string, public limit: number = 50) {}



    //items
    public items: { item: any, touched: number } [] = [];


    /*
     * @Params:  identifier
     * @Does:    fetches item from items by key index
     * @Return:  item
     */
    public fetchItem(identifier) {
        let model = this;
        //get index of identifier
        let idx = model.items.map(item => { return item.item[model.key] }).indexOf(identifier);
        //check if has item
        if (idx == -1) return null;
        //increment touched
        model.items[idx].touched++;
        //has item, return item
        return model.items[idx].item;
    }
    //end fetchItem()



    /*
     * @Params:  identifier, item
     * @Does:    adds item to items
     * @Return:  none
     */
    public addItem(identifier, item) {
        let model = this;
        //set identifier
        item[model.key] = identifier;
        //add item to items
        model.items.push({ item: item, touched: Math.floor(Math.random() * (999 - 100) + 100) / 1000 });
        //checks if is over limit
        if (model.items.length >= model.limit) {
            //has over limit items
            let minIdx = model.items.map(target => { return target.touched }).indexOf(Math.min(...model.items.map(target => { return target.touched })));
            //remove from items
            if (minIdx != -1) model.items.splice(minIdx, 1);
        }
    }
    //end addItem()



    /*
     * @Params:  identifier
     * @Does:    removes item from items by index
     * @Return:  none
     */
    public removeItem(identifier) {
        let model = this;
        //get index of identifier
        let idx = model.items.map(item => { return item.item[model.key] }).indexOf(identifier);
        //check if has item
        if (idx != -1) model.items.splice(idx, 1);
        return;
    }
    //end removeItem()

}