import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bridge-elements-overlays-younow-likes-settings-box-base',
  templateUrl: './overlays-younow-likes-settings-box-base.component.html',
  styleUrls: ['./overlays-younow-likes-settings-box-base.component.css']
})
export class OverlaysYounowLikesSettingsBoxBaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
