import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsSelectBoxBaseComponent } from './effects-select-box-base.component';

describe('EffectsSelectBoxBaseComponent', () => {
  let component: EffectsSelectBoxBaseComponent;
  let fixture: ComponentFixture<EffectsSelectBoxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsSelectBoxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsSelectBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
