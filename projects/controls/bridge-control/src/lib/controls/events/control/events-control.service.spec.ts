import { TestBed } from '@angular/core/testing';

import { EventsControlService } from './events-control.service';

describe('EventsControlService', () => {
  let service: EventsControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventsControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
