import { Component, OnInit, Input } from '@angular/core';
import { EffectsItemControlService } from '@controls/bridge-control/src/lib/controls/effects/item/control/effects-item-control.service';

@Component({
    selector: 'bridge-elements-effects-edit-box-triggers-section-item-base',
    templateUrl: './effects-edit-box-triggers-section-item-base.component.html',
    styleUrls: ['./effects-edit-box-triggers-section-item-base.component.css']
})
export class EffectsEditBoxTriggersSectionItemBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {
        //set trigger
        this.setTrigger();
    }





    /// EFFECT ///
    //===========================

    //effect item
    @Input('EffectItem') EffectItem: EffectsItemControlService = null;

    //===========================
    /// END EFFECT ///









    /// TRIGGER //
    //=========================

    //trigger item
    @Input('Trigger') Trigger = null;

    //inputs
    public inputs = null;


    /*
     * @Params:  none
     * @Does:    sets inputs from trigger
     */
    public setTrigger() {
        let model = this;
        //set inputs
        model.inputs = { ...model.Trigger };
    }
    //end setTrigger()



    /*
     * @Params:  none
     * @Does:    saves trigger to effect item
     */
    public saveTrigger() {
        let model = this;
        //save trigger
        model.EffectItem.Triggers.updateTrigger(model.Trigger.trigger_id, model.inputs.phrase);
    }
    //end saveTrigger()



    /*
     * @Params:  none
     * @Does:    checks if is same trigger
     */
    public checkChanged() {
        let model = this;
        //check if is changed
        return (model.inputs.phrase != model.Trigger.phrase);
    }
    //end checkChanged()



    /*
     * @Params:  none
     * @Does:    removes trigger from effect
     */
    public removeTrigger() {
        let model = this;
        //remove from effect
        model.EffectItem.Triggers.removeTrigger(model.Trigger.trigger_id);
    }
    //end removeTrigger()

    //=========================
    /// END TRIGGER //







}