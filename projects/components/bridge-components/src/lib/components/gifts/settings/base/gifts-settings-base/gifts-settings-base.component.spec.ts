import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftsSettingsBaseComponent } from './gifts-settings-base.component';

describe('GiftsSettingsBaseComponent', () => {
  let component: GiftsSettingsBaseComponent;
  let fixture: ComponentFixture<GiftsSettingsBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftsSettingsBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftsSettingsBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
