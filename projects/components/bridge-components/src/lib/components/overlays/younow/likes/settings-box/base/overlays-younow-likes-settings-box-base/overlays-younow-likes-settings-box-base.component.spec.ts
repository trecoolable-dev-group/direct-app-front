import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaysYounowLikesSettingsBoxBaseComponent } from './overlays-younow-likes-settings-box-base.component';

describe('OverlaysYounowLikesSettingsBoxBaseComponent', () => {
  let component: OverlaysYounowLikesSettingsBoxBaseComponent;
  let fixture: ComponentFixture<OverlaysYounowLikesSettingsBoxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaysYounowLikesSettingsBoxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaysYounowLikesSettingsBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
