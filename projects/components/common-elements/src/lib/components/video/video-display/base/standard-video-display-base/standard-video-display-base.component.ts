import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'common-elements-standard-video-display-base',
    templateUrl: './standard-video-display-base.component.html',
    styleUrls: ['./standard-video-display-base.component.css']
})
export class StandardVideoDisplayBaseComponent implements OnInit {

    constructor() {}

    ngOnInit() {
        //set stream
        this.setStream();
    }





    /// INPUTS ///
    //==============================================

    //cover flag
    @Input('Cover') Cover = true;

    //controls flag
    @Input('Controls') Controls = true;

    //muted flag
    @Input('Muted') Muted = true;

    //loop flag
    @Input('Loop') Loop = false;


    //==============================================
    /// END INPUTS ///











    /// STREAM ///
    //=================================

    //input stream
    @Input('Stream') Stream = null;

    //input src
    @Input('Src') Src = null;

    //media display element
    @ViewChild('VideoElement', { static: true }) MediaElement: ElementRef;


    /*
     * @Params:  none
     * @Does:    sets media stream and plays on video element
     */
    public setStream() {
        let model = this;
        //check if stream is null
        if (model.Stream == null && model.Src == null) return;
        //set native element
        let video = model.MediaElement.nativeElement;
        //set attributes
        video.setAttribute('autoplay', '');
        video.setAttribute('playsinline', '');
        //check if is muted
        if (model.Muted) video.muted = true;
        //check if has controls
        if (model.Controls) video.controls = true;
        //check if is loop
        if (model.Loop) video.setAttribute('loop', 'true');
        //create url
        if (model.Stream != null) video.srcObject = model.Stream;
        if (model.Src != null) video.src = model.Src;
        //load video and play
        video.load();
        video.play();
        //attempt to play video again
        setTimeout(() => {
            //play video
            video.play()
        }, 1500);
    }
    //end setStream()


    //=================================
    /// END STREAM ///





}