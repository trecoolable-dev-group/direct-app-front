import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageBaseComponent } from '@components/bridge-components/src/lib/components/home/page/base/home-page-base/home-page-base.component';


const routes: Routes = [{
    path: '**',
    component: HomePageBaseComponent
}];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: false, relativeLinkResolution: 'corrected', scrollPositionRestoration: 'enabled' })],
    exports: [RouterModule]
})
export class AppRoutingModule {}