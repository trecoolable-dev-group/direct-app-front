import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaysYounowTopFansBaseComponent } from './overlays-younow-top-fans-base.component';

describe('OverlaysYounowTopFansBaseComponent', () => {
  let component: OverlaysYounowTopFansBaseComponent;
  let fixture: ComponentFixture<OverlaysYounowTopFansBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaysYounowTopFansBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaysYounowTopFansBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
