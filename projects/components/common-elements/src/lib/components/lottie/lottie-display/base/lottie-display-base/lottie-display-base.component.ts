import { Component, OnInit, ViewChild, ElementRef, EventEmitter, AfterViewInit, Input, Output } from '@angular/core';
import Lottie from 'lottie-web';

@Component({
    selector: 'common-elements-lottie-display-base',
    templateUrl: './lottie-display-base.component.html',
    styleUrls: ['./lottie-display-base.component.css']
})
export class LottieDisplayBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}






    /*
     * @Params:  none
     * @Does:    sets lottie var to lottie obj
     */
    ngAfterViewInit() {
        let model = this
        //set lottie
        model.LottieObj = Lottie.loadAnimation({
            container: model.LottieElement.nativeElement,
            renderer: 'svg',
            loop: model.Loop,
            autoplay: model.Autoplay,
            path: model.Path
        });
        //check if has start and end
        if (model.Start != -1 && model.End != -1) {
            //stop animation
            model.LottieObj.stop();
            //set on loaded
            model.LottieObj.addEventListener('DOMLoaded', function() {
                //set segments
                model.LottieObj.playSegments([model.Start, model.End], true);
                //set loop
                model.LottieObj.loop = model.Loop;
            });
        }
        //trigger onload with lottie obj
        model.onLoad.emit(model.LottieObj);
    }
    //end ngAfterViewInit()



    //lottie path input
    @Input('Path') Path = '';

    //autoplay flag
    @Input('Autoplay') Autoplay = true;

    //loop flag
    @Input('Loop') Loop = false;

    //start frame
    @Input('Start') Start = -1;

    //end frame
    @Input('End') End = -1;

    //lottie element
    @ViewChild('LottieElement') LottieElement: ElementRef;

    //lottie object
    public LottieObj: any = null;

    //on load output
    @Output() onLoad = new EventEmitter();



    /*
     * @Params:  none
     * @Does:    plays lottie file
     */
    public play() {
        let model = this;
        //set segments
        model.LottieObj.goToAndPlay(0, true);
        //set on end
        model.LottieObj.onComplete = () => {
            //set on complete
            model.onComplete();
        }
    }
    //end play()



    public onComplete = () => {
        console.log('completed')
    }




}