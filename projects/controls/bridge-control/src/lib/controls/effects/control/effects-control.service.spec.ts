import { TestBed } from '@angular/core/testing';

import { EffectsControlService } from './effects-control.service';

describe('EffectsControlService', () => {
  let service: EffectsControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EffectsControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
