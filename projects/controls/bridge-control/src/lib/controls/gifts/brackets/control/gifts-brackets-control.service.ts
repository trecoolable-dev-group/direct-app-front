import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';
import { ItemsStoreService } from '@controls/bridge-control/src/lib/tools/items-store/items-store.service';
import { GiftsBracketsItemControlService } from '../item/control/gifts-brackets-item-control.service';


export class GiftsBracketsControlService {

    constructor(private GiftsDb: ItemsDbService) {
        //set standard
        this.setStandard();
    }





    /// DB ///
    //===========================

    //collections
    private GIFTS_BRACKETS_COL = 'gifts_brackets_col';

    //===========================
    /// END DB ///









    /// BRACKETS ///
    //==================================

    //brackets
    public BracketItems = new ItemsStoreService('bracket_id', 70);


    /*
     * @Params:  bracket_id, platform
     * @Does:    fetches bracket item
     *           if no item, adds bracket to brackets
     * @Return:  bracket
     */
    public BracketItem(bracket_id, platform): GiftsBracketsItemControlService {
        let model = this;
        //fetch bracket
        let bracket = model.BracketItems.fetchItem(bracket_id);
        //check if has bracket
        if (bracket == null) {
            //create new bracket
            bracket = new GiftsBracketsItemControlService(model.GiftsDb, bracket_id);
            //set platform
            bracket.platform = platform;
            //add to brackets
            model.BracketItems.addItem(bracket_id, bracket);
        }
        //return with bracket
        return bracket;
    }
    //end BracketItem()


    //==================================
    /// END BRACKETS ///















    /// PLATFORMS ///
    //=====================================

    //platforms
    public platforms = {
        younow: []
    };

    /*
     * @Params:  platform
     * @Does:    fetches platform brackets
     */
    public async fetchPlatformBrackets(platform) {
        let model = this;
        //check if has brackets
        if (model.platforms[platform] && model.platforms[platform].length) return reply(true, 'has brackets', { brackets: model.platforms[platform] });
        //fetch brackets
        let fetchOp: any = await model.GiftsDb.findItemsByKey('platform', platform, model.GIFTS_BRACKETS_COL, 10);
        //check if got brackets
        if (!fetchOp.success) return reply(false, 'could not get brackets');
        //set brackets
        model.platforms[platform] = [...model.platforms[platform], ...fetchOp.data.items.map(item => { return item.bracket_id })];
    }
    //end fetchPlatformBrackets()



    /*
     * @Params: platform, value
     * @Does:   fetches brackets for platform where value is between
     */
    public async fetchBracketByValue(platform, value) {
        let model = this;
        //fetch bracket
        let fetchOp = await model.GiftsDb.DB[model.GIFTS_BRACKETS_COL].where('platform').equals(platform).and(item => { return (item.min <= value && item.max >= value) }).toArray();
        //check if has items
        if (fetchOp.length) return reply(true, 'got bracket', { bracket: fetchOp[0] });
        //no bracket
        return reply(true, 'no bracket', { bracket: null });
    }
    //end fetchBracketByValue()

    //=====================================
    /// END PLATFORMS ///












    /// STANDARD ///
    //================================

    /*
     * @Params:  none
     * @Does:    checks if has standard bracket
     */
    public async setStandard() {
        let model = this;
        //standard
        let standard = [
            { bracket_id: 'yn_bracket01', platform: 'younow', unit: 'bars', min: 10, max: 50, effects: ['ding'] },
            { bracket_id: 'yn_bracket02', platform: 'younow', unit: 'bars', min: 51, max: 200, effects: ['anotha one'] },
            { bracket_id: 'yn_bracket03', platform: 'younow', unit: 'bars', min: 201, max: 500, effects: ['tre yay'] },
            { bracket_id: 'yn_bracket04', platform: 'younow', unit: 'bars', min: 501, max: 1500, effects: ['oh man'] },
            { bracket_id: 'yn_bracket05', platform: 'younow', unit: 'bars', min: 1501, max: 5000, effects: ['yessir'] },
            { bracket_id: 'yn_bracket06', platform: 'younow', unit: 'bars', min: 5001, max: 25000, effects: ['dolphin'] },
        ];
        //iterate through brackets
        for (let i = standard.length - 1; i >= 0; i--) {
            let item = standard[i];
            //set bracket
            let bracket = model.BracketItem(item.bracket_id, item.platform);
            //check if has bracket
            let fetchOp: any = await bracket.fetchInfo();
            //check if has info
            if (!fetchOp.success && fetchOp.data.info == null) {
                //does not have bracket, set info
                bracket.info_inputs.unit = item.unit;
                bracket.info_inputs.min = item.min;
                bracket.info_inputs.max = item.max;
                bracket.info_inputs.effects = item.effects;
                //save info
                bracket.saveInfo();
                //add to brackets
                model.platforms[item.platform].unshift(bracket.bracket_id);
            }
        }
    }
    //end setStandard()

    //================================
    /// END STANDARD ///







}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}