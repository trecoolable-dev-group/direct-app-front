import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-effects-select-box-item-base',
    templateUrl: './effects-select-box-item-base.component.html',
    styleUrls: ['./effects-select-box-item-base.component.css']
})
export class EffectsSelectBoxItemBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set effect
        this.setEffect();
    }


    //controls
    public Effects = this.BridgeControl.Effects;
    public Playing = this.BridgeControl.Effects.Playing;






    /// EDIT DISPLAY ///
    //=================================

    //overlay edit_opened
    public edit_opened = -1;


    /*
     * @Params:  none
     * @Does:    toggles overlay edit_opened
     */
    public toggleEditOpened() {
        let model = this;
        //check if is edit_opened
        if (model.edit_opened != 1) {
            //set edit_opened
            model.edit_opened = 1;
        } else {
            //set closed
            model.edit_opened = 0;
            //await to remove
            setTimeout(() => {
                model.edit_opened = -1;
            }, 200);
        }
    }
    //end toggleEditOpened()

    //=================================
    /// END EDIT DISPLAY ///











    /// EFFECT ///
    //==============================

    //effect id
    @Input('EffectId') EffectId = '';

    //effect item
    public EffectItem = null;


    /*
     * @Params:  none
     * @Does:    fetches effect info
     *           sets effect    
     */
    public async setEffect() {
        let model = this;
        //check has id
        if (!model.EffectId) return;
        //set effect item
        let effect = model.BridgeControl.Effects.EffectItem(model.EffectId);
        //fetch info
        let fetchOp: any = await effect.fetchInfo();
        //check if got info
        if (fetchOp.success) {
            //set effect
            model.EffectItem = effect;
        }
    }
    //end setEffect()

    //==============================
    /// END EFFECT ///












    /// SELECT ///
    //===============================

    //select output
    @Output('onSelect') onSelect = new EventEmitter < any > ();


    /*
     * @Params:  none
     * @Does:    triggers on select with effect id
     */
    public handleSelect() {
        let model = this;
        //trigger on select
        model.onSelect.emit(model.EffectId);
    }
    //end handleSelect()

    //===============================
    /// END SELECT ///




}