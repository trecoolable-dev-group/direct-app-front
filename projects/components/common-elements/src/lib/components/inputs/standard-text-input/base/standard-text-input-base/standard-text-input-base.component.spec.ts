import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardTextInputBaseComponent } from './standard-text-input-base.component';

describe('StandardTextInputBaseComponent', () => {
  let component: StandardTextInputBaseComponent;
  let fixture: ComponentFixture<StandardTextInputBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardTextInputBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardTextInputBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
