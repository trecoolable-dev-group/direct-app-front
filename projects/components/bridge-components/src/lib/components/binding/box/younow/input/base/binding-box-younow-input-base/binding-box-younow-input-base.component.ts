import { Component, OnInit } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-binding-box-younow-input-base',
    templateUrl: './binding-box-younow-input-base.component.html',
    styleUrls: ['./binding-box-younow-input-base.component.css']
})
export class BindingBoxYounowInputBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {}



    //controls
    public Younow = this.BridgeControl.Events.Younow;




}