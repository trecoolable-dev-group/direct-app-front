import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaysEffectsItemGifBaseComponent } from './overlays-effects-item-gif-base.component';

describe('OverlaysEffectsItemGifBaseComponent', () => {
  let component: OverlaysEffectsItemGifBaseComponent;
  let fixture: ComponentFixture<OverlaysEffectsItemGifBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaysEffectsItemGifBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaysEffectsItemGifBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
