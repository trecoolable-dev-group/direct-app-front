import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
    selector: 'common-elements-standard-dropdown-option',
    templateUrl: './standard-dropdown-option.component.html',
    styleUrls: ['./standard-dropdown-option.component.css']
})
export class StandardDropdownOptionComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}









    /// INPUTS ///
    //===================================

    //label
    @Input('Label') Label = '';

    //icon type
    @Input('IconType') IconType = '';

    //icon class
    @Input('IconClass') IconClass = '';

    //icon src
    @Input('IconSrc') IconSrc = '';

    //icon name
    @Input('IconName') IconName = '';

    //icon fill
    @Input('IconFill') IconFill = '';

    //value
    @Input('Value') Value = '';

    //active
    @Input('Active') Active = false;

    //on select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ();


    /*
     * @Params:  event
     * @Does:    triggers on select
     */
    public selectItem(event) {
        let model = this;
        //check if has value
        if (model.Value == undefined) return;
        //splash
        model.splash(event);
        //trigger on select
        model.onSelect.emit({ label: model.Label, value: model.Value });
    }
    //end selectItem()


    //===================================
    /// END INPUTS ///





















    /// SPLASH ///
    //=======================================

    //splash element
    @ViewChild('SplashElement') SplashElement: any;


    /*
     * @Params:  event
     * @Does:    triggers splash
     */
    public splash(event) {
        let model = this;
        //trigger splash
        if (model.SplashElement && model.SplashElement.splash) model.SplashElement.splash(event);
    }
    //end handleClick()


    //=======================================
    /// END SPLASH ///



}