import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GendersSelectOverlayOptionItemComponent } from './genders-select-overlay-option-item.component';

describe('GendersSelectOverlayOptionItemComponent', () => {
  let component: GendersSelectOverlayOptionItemComponent;
  let fixture: ComponentFixture<GendersSelectOverlayOptionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GendersSelectOverlayOptionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GendersSelectOverlayOptionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
