import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';

@Component({
    selector: 'common-elements-voices-select-overlay-base',
    templateUrl: './voices-select-overlay-base.component.html',
    styleUrls: ['./voices-select-overlay-base.component.css']
})
export class VoicesSelectOverlayBaseComponent implements OnInit {

    constructor(private cd: ChangeDetectorRef) {
        //set voice options
        this.setVoiceOptions();
    }

    ngOnInit(): void {}




    /// DISPLAY ///
    //================================

    //opened flag
    @Input('Opened') Opened = -1;

    //close event
    @Output('onClose') onClose: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  none
     * @Does:    sets opened to false
     */
    public close() {
        let model = this;
        //set opened to close
        model.Opened = 0;
        //emit close event
        model.onClose.emit();
    }
    //end close()

    //================================
    /// END DISPLAY ///












    /// VOICES ///
    //============================

    //voices
    public voices = [];

    //value input
    @Input('Value') Value = '';

    //select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  code
     * @Does:    sets value
     *           triggers on select for country code
     *           closes interface
     */
    public handleSelect(value) {
        let model = this;
        //set value
        model.Value = value;
        //emit country code
        model.onSelect.emit(value);
        //close interface
        model.close();
    }
    //end handleSelect()



    /*
     * @Params:  none
     * @Does:    sets voice options
     */
    public async setVoiceOptions() {
        let model = this;
        try {
            //check if has voices
            if (model.voices.length) return;
            //set voices
            let voices = speechSynthesis.getVoices();
            //check if has voices
            if (!voices.length) {
                //set voices listener
                speechSynthesis.onvoiceschanged = () => {
                    //set voices
                    voices = speechSynthesis.getVoices();
                    //set voices
                    model.voices = voices;
                    //detect changes
                    model.cd.detectChanges();
                }
            } else {
                //set voices
                model.voices = voices;
                //detect changes
                model.cd.detectChanges();
            }
        } catch (error) { console.error(error) }
    }
    //end setVoiceOptions()


    //============================
    /// END VOICES ///





}