import { TestBed } from '@angular/core/testing';

import { AccountsControlService } from './accounts-control.service';

describe('AccountsControlService', () => {
    let service: AccountsControlService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(AccountsControlService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});