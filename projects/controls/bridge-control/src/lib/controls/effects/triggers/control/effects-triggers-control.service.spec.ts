import { TestBed } from '@angular/core/testing';

import { EffectsTriggersControlService } from './effects-triggers-control.service';

describe('EffectsTriggersControlService', () => {
  let service: EffectsTriggersControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EffectsTriggersControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
