import { TestBed } from '@angular/core/testing';

import { EventsListControlService } from './events-list-control.service';

describe('EventsListControlService', () => {
  let service: EventsListControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventsListControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
