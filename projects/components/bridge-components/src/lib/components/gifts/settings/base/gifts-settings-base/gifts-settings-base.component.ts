import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'bridge-elements-gifts-settings-base',
    templateUrl: './gifts-settings-base.component.html',
    styleUrls: ['./gifts-settings-base.component.css']
})
export class GiftsSettingsBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}







    /// SECTIONS ///
    //=============================

    //current section
    public section = 'brackets';

    //sections
    public sections = [
        'brackets'
    ];


    /*
     * @Params:  section
     * @Does:    changes section
     */
    public async changeSection(section) {
        let model = this;
        //set section
        model.section = section;
    }
    //end changeSection()


    //=============================
    /// END SECTIONS ///






}