import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'common-elements-standard-birthday-input-base',
    templateUrl: './standard-birthday-input-base.component.html',
    styleUrls: ['./standard-birthday-input-base.component.css']
})
export class StandardBirthdayInputBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {
        //set month
        if (this.MonthValue != 0 && !isNaN(this.MonthValue)) {
            //set month
            this.month = this.months[this.MonthValue - 1];
        }
        //set date
        if (this.DateValue != 0 && !isNaN(this.DateValue)) {
            //set date
            this.date = { label: this.DateValue + '', value: this.DateValue };
        }
        //set year
        if (this.YearValue != 0 && !isNaN(this.YearValue)) {
            //set year
            this.year = { label: this.YearValue + '', value: this.YearValue };
        }
    }








    /// INPUTS ///
    //==============================

    //month input
    @Input('MonthValue') MonthValue = 0;

    //date input
    @Input('DateValue') DateValue = 0;

    //year input
    @Input('YearValue') YearValue = 0;


    //==============================
    /// END INPUTS ///










    /// VALUES ///
    //=======================================

    //month
    public month = { label: 'Select', value: 0 }

    //date
    public date = { label: 'Select', value: 0 }

    //year
    public year = { label: 'Select', value: 0 }

    //=======================================
    /// END VALUES ///















    /// OPTIONS ///
    //================================

    //dropdown type
    public dropdown_type = '';

    //dropdown value
    public dropdown_value: any = '';

    //dropdown
    public dropdown = false;

    //options
    public options = [];

    //months
    public months = [
        { icon: '', label: 'January', value: 1 },
        { icon: '', label: 'Feruary', value: 2 },
        { icon: '', label: 'March', value: 3 },
        { icon: '', label: 'April', value: 4 },
        { icon: '', label: 'May', value: 5 },
        { icon: '', label: 'June', value: 6 },
        { icon: '', label: 'July', value: 7 },
        { icon: '', label: 'August', value: 8 },
        { icon: '', label: 'September', value: 9 },
        { icon: '', label: 'October', value: 10 },
        { icon: '', label: 'November', value: 11 },
        { icon: '', label: 'December', value: 12 },
    ];

    //select event
    @Output('onChange') onChange: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  item
     * @Does:    handles select
     *           sets value based on type
     */
    public handleSelect(item) {
        let model = this;
        //set month
        if (model.dropdown_type == 'month') model.month = item;
        //set date
        if (model.dropdown_type == 'date') model.date = item;
        //set year
        if (model.dropdown_type == 'year') model.year = item;
        //close dropdown
        model.dropdown = false;
        //trigger on change
        model.onChange.emit({ month: model.month.value, date: model.date.value, year: model.year.value });
    }
    //end handleSelect()


    /*
     * @Params:  event
     * @Does:    opens month dropdown
     */
    public openMonths(event) {
        let model = this;
        //set dropdown type
        model.dropdown_type = 'month';
        //set drop value
        model.dropdown_value = model.month.value;
        //set month options
        model.options = model.months;
        //open dropdown
        model.dropdown = true;
    }
    //end openMonths()



    /*
     * @Params:  event
     * @Does:    opens date dropdown
     */
    public openDates(event) {
        let model = this;
        //set dropdown type
        model.dropdown_type = 'date';
        //set drop value
        model.dropdown_value = model.date.value;
        //set date options
        model.options = [];
        //iterate to add options
        for (let i = 1; i <= 31; i++) {
            //add to options
            model.options.push({ label: i, value: i });
        }
        //open dropdown
        model.dropdown = true;
    }
    //end openDates()



    /*
     * @Params:  event
     * @Does:    opens year dropdown
     */
    public openYears(event) {
        let model = this;
        //set dropdown type
        model.dropdown_type = 'year';
        //set drop value
        model.dropdown_value = model.year.value;
        //set year options
        model.options = [];
        //iterate to add options
        for (let i = 1969; i <= 2007; i++) {
            //add to options
            model.options.unshift({ label: i, value: i });
        }
        //open dropdown
        model.dropdown = true;
    }
    //end openYears()



    //================================
    /// END OPTIONS ///




}