import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsListStatusItemBaseComponent } from './events-list-status-item-base.component';

describe('EventsListStatusItemBaseComponent', () => {
  let component: EventsListStatusItemBaseComponent;
  let fixture: ComponentFixture<EventsListStatusItemBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsListStatusItemBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsListStatusItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
