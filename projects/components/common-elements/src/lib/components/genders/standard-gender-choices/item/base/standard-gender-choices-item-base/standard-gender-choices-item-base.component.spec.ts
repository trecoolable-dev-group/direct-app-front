import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardGenderChoicesItemBaseComponent } from './standard-gender-choices-item-base.component';

describe('StandardGenderChoicesItemBaseComponent', () => {
  let component: StandardGenderChoicesItemBaseComponent;
  let fixture: ComponentFixture<StandardGenderChoicesItemBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardGenderChoicesItemBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardGenderChoicesItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
