import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
    selector: 'common-elements-standard-gender-choices-item-base',
    templateUrl: './standard-gender-choices-item-base.component.html',
    styleUrls: ['./standard-gender-choices-item-base.component.css']
})
export class StandardGenderChoicesItemBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}





    /// STYLES ///
    //============================

    //choices class
    @Input('ChoiceClass') ChoiceClass = 'std-height padding-side-med margin-side-small margin-vert';

    //icon class
    @Input('IconClass') IconClass = 'small-square margin-side-small margin-vert';

    //label class
    @Input('LabelClass') LabelClass = 'bold-text margin-side-small margin-vert';

    //============================
    /// END STYLES ///









    /// INFO ///
    //================================

    //label text
    @Input('Label') Label = '';

    //value
    @Input('Value') Value = '';

    //icon
    @Input('Icon') Icon = '';

    //icon color
    @Input('Color') Color = '#fff';

    //active
    @Input('Active') Active = false;


    //================================
    /// END INFO ///
















    /// SPLASH ///
    //=======================================

    //splash element
    @ViewChild('SplashElement') SplashElement: any;


    /*
     * @Params:  none
     * @Does:    triggers splash
     *           starts matchmaking skip
     */
    public handleClick(event) {
        let model = this;
        //trigger splash
        if (model.SplashElement && model.SplashElement.splash) model.SplashElement.splash(event);


    }
    //end handleClick()


    //=======================================
    /// END SPLASH ///













}