import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaysEffectsItemVideoBaseComponent } from './overlays-effects-item-video-base.component';

describe('OverlaysEffectsItemVideoBaseComponent', () => {
  let component: OverlaysEffectsItemVideoBaseComponent;
  let fixture: ComponentFixture<OverlaysEffectsItemVideoBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaysEffectsItemVideoBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaysEffectsItemVideoBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
