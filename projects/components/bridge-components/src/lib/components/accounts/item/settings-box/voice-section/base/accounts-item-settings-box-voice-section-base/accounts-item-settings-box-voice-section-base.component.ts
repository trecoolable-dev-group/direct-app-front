import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
    selector: 'bridge-elements-accounts-item-settings-box-voice-section-base',
    templateUrl: './accounts-item-settings-box-voice-section-base.component.html',
    styleUrls: ['./accounts-item-settings-box-voice-section-base.component.css']
})
export class AccountsItemSettingsBoxVoiceSectionBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}






    /// ACCOUNT ///
    //===============================

    //account
    @Input('Account') Account = null;


    /*
     * @Params:  none
     * @Does:    checks if is highlight
     *           sets highlight
     *           saves voice settings
     */
    public toggleHIghlight() {
        let model = this;
        //check if is highlighted
        if (model.Account.Voice.settings.highlight != 1) {
            //set highlight
            model.Account.Voice.settings_inputs.highlight = 1;
        } else {
            //set unhighlight
            model.Account.Voice.settings_inputs.highlight = 0;
        }
        //save voice settings
        model.Account.Voice.saveSettings();
    }
    //end toggleHighlight()

    //===============================
    /// END ACCOUNT ///













    /// VOICES ///
    //============================

    //user settings opened
    public voices_opened = -1;


    /*
     * @Params:  none
     * @Does:    toggles settings opened
     */
    public async toggleVoicesOpened() {
        let model = this;
        //check if settings is opened
        if (model.voices_opened != 1) {
            //set opened
            model.voices_opened = 1;
        } else {
            //set to closed
            model.voices_opened = 0;
            //await to remove
            await wait(200);
            //remove settings
            model.voices_opened = -1;
        }
    }
    //end toggleVoicesOpened()



    /*
     * @Params:  voice_name
     * @Does:    sets default voice
     */
    public handleVoiceSelect(voice_name) {
        let model = this;
        //set voice
        model.Account.Voice.settings_inputs.voice = voice_name;
        //save settings
        model.Account.Voice.saveSettings();
    }
    //end handleVoiceSelect()

    //============================
    /// END VOICES ///



















    /// SAVING ///
    //==============================

    //loading lottie
    @ViewChild('LoadingLottie') LoadingLottie;

    //save status
    public status = '';


    /*
     * @Params:  none
     * @Does:    saves inputs
     */
    public async saveInputs() {
        let model = this;
        //check if is busy
        if (model.status == 'busy') return;
        //set status
        model.status = 'busy';
        //set loading lottie
        model.LoadingLottie.playLoading();
        //save effect
        let submitOp = await model.Account.Voice.saveSettings();
        //check if success
        if (!submitOp.success) {
            //set error status
            model.status = 'error';
            //did not add tier, play error
            model.LoadingLottie.playError();
        } else {
            //set success status
            model.status = 'success';
            //added tier, play success
            model.LoadingLottie.playSuccess();
        }
        //wait to unset status
        await wait(2000);
        //unset submit status
        model.status = '';
    }
    //end saveInputs()

    //==============================
    /// END SAVING ///



}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d };
}


function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}