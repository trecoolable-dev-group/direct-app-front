import { Component, OnInit } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-binding-box-base',
    templateUrl: './binding-box-base.component.html',
    styleUrls: ['./binding-box-base.component.css']
})
export class BindingBoxBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //check if binding completed
        this.checkBindingHint();
    }



    //controls
    public Tutorials = this.BridgeControl.Tutorials;






    /// HINT DISPLAY ///
    //=================================

    //display id
    public display_id = Math.random().toString(36).slice(2).toUpperCase();

    //hint title
    public hint_title = 'Stream Binding';

    //hint content
    public hint_content = 'Bind Mattybot with your YouNow stream by entering your stream name and hitting connect.';

    //hint image
    public hint_image = 'https://i.imgur.com/afi7nlp.gif';

    //overlay hint_opened
    public hint_opened = -1;


    /*
     * @Params:  none
     * @Does:    checks if binding tutorial has been completed
     */
    public async checkBindingHint() {
        let model = this;
        //check if binding completed
        let checkOp: any = await model.Tutorials.checkCompleted('stream_binding');
        //check completed
        if (checkOp.success && !checkOp.data.completed) {
            //set display item
            model.Tutorials.addDisplayItem('stream_binding', model.display_id);
            //toggle opened
            model.toggleHintOpened();
        }
    }
    //end checkBindingHint()



    /*
     * @Params:  force
     * @Does:    toggles overlay add_opened
     */
    public toggleHintOpened(force = false) {
        let model = this;
        //check if is hint_opened
        if (model.hint_opened != 1) {
            //set display id
            if (force) model.Tutorials.display_id = model.display_id;
            //set add_opened
            model.hint_opened = 1;
        } else {
            //set closed
            model.hint_opened = 0;
            //await to remove
            setTimeout(() => {
                model.hint_opened = -1;
                //add to completed
                model.Tutorials.addCompleted('stream_binding');
                //remove from display items
                model.Tutorials.removeDisplayItem(model.display_id);
            }, 200);
        }
    }
    //end toggleHintOpened()

    //=================================
    /// END HINT DISPLAY ///









}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}