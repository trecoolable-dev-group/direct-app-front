import { Component, OnInit } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-events-settings-voices-section-base',
    templateUrl: './events-settings-voices-section-base.component.html',
    styleUrls: ['./events-settings-voices-section-base.component.css']
})
export class EventsSettingsVoicesSectionBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {}



    //controls
    public Voices = this.BridgeControl.Voices;











    /// VOICES ///
    //============================

    //user settings opened
    public voices_opened = -1;


    /*
     * @Params:  none
     * @Does:    toggles settings opened
     */
    public async toggleVoicesOpened() {
        let model = this;
        //check if settings is opened
        if (model.voices_opened != 1) {
            //set opened
            model.voices_opened = 1;
        } else {
            //set to closed
            model.voices_opened = 0;
            //await to remove
            await wait(200);
            //remove settings
            model.voices_opened = -1;
        }
    }
    //end toggleVoicesOpened()



    /*
     * @Params:  voice_name
     * @Does:    sets default voice
     */
    public handleVoiceSelect(voice_name) {
        let model = this;
        //set voice
        model.Voices.settings_inputs.default_voice = voice_name;
        //save settings
        model.Voices.saveSettings();
    }
    //end handleVoiceSelect()

    //============================
    /// END VOICES ///




}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d };
}


function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}