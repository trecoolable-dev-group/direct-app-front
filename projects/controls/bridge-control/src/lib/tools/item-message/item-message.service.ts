import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ItemMessageService {

    constructor() {}



    //message
    public message = '';

    //type
    public type = '';

    //status
    public status = '';

    //message changed timestamp
    public changed = 0;

    //remove timeout
    public removeTimeout = null;


    /*
     * @Params:  message, type, status, duration
     * @Does:    sets message
     *           sets message changed
     *           sets timeout to remove message
     * @Return:  null
     */
    public setMessage(message, type = '', status: any = 'error', duration = 5000) {
        let model = this;
        //check if has timeout
        if (model.removeTimeout) {
            //clear tiemout
            window.clearTimeout(model.removeTimeout);
            //unset timeout
            model.removeTimeout = null;
        }
        //set message
        model.message = message;
        //set type
        model.type = (type) ? type : '';
        //set status
        if (status === true || status == 'success') {
            //is success, set success
            model.status = 'success'
        } else if (status === false || status == 'error') {
            //set error
            model.status = 'error';
        } else {
            //set no status
            model.status = '';
        }
        //set local changed time
        let changed = Date.now();
        //set changed
        model.changed = changed;
        //set timeout to remove
        model.removeTimeout = setTimeout(() => {
            //check if is same changed
            if (model.changed != changed) return;
            //is same change, remove message
            model.message = '';
            //remove type
            model.type = '';
            //remove status
            model.status = '';
            //remove changed
            model.changed = 0;
            //clear tiemout
            window.clearTimeout(model.removeTimeout);
            //unset timeout
            model.removeTimeout = null;
        }, duration);
    }
    //end setMessage()



    /*
     * @Params:  none
     * @Does:    clears message
     */
    public clearMessage() {
        let model = this;
        //is same change, remove message
        model.message = '';
        //remove type
        model.type = '';
        //remove status
        model.status = '';
        //remove changed
        model.changed = 0;
        //check if has timeout
        if (model.removeTimeout != null) {
            //clear tiemout
            window.clearTimeout(model.removeTimeout);
            //unset timeout
            model.removeTimeout = null;
        }
    }
    //end clearMessage()



}