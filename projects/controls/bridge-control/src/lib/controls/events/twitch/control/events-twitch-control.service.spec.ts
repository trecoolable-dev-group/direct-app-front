import { TestBed } from '@angular/core/testing';

import { EventsTwitchControlService } from './events-twitch-control.service';

describe('EventsTwitchControlService', () => {
  let service: EventsTwitchControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventsTwitchControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
