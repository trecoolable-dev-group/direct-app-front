import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bridge-elements-events-settings-audience-section-base',
  templateUrl: './events-settings-audience-section-base.component.html',
  styleUrls: ['./events-settings-audience-section-base.component.css']
})
export class EventsSettingsAudienceSectionBaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
