import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';
import { ItemMessageService } from '@controls/bridge-control/src/lib/tools/item-message/item-message.service';


export class EffectsItemTriggersControlService {

    constructor(private EffectsDb: ItemsDbService, public effect_id: string) {}




    /// DB ///
    //===========================

    //collections
    private LOCAL_EFFECTS_COL = 'local_effects_col';
    private EFFECTS_DATA_COL = 'effects_data_col';
    private EFFECTS_SETTINGS_COL = 'effects_settings_col';
    private EFFECTS_TRIGGERS_COL = 'effects_triggers_col';

    //===========================
    /// END DB ///










    /// MESSAGE ///
    //=================================

    //message
    public message = new ItemMessageService();

    //messages
    public input_messages = {};

    //=================================
    /// END MESSAGE ///










    /// TRIGGERS ///
    //=========================

    //triggers
    public triggers = [];


    /*
     * @Params:  more
     * @Does:    fetches triggers with effect_id
     */
    public async fetchTriggers(more = true) {
        let model = this;
        try {
            //check if has triggers and is more
            if (model.triggers.length && !more) return reply(true, 'has triggers', { triggers: model.triggers });
            //fetch triggers
            let fetchOp = await model.EffectsDb.DB[model.EFFECTS_TRIGGERS_COL].where('effect_id').equals(model.effect_id).offset(model.triggers.length).limit(15).toArray();
            //set items
            model.triggers = [...model.triggers, ...fetchOp];
            //return with triggers
            return reply(true, 'got triggers', { triggers: fetchOp });
        } catch (error) {
            //return error
            return reply(false, 'could not fetch triggers');
        }
    }
    //end fetchItemTriggers()



    /*
     * @Params: phrase
     * @Does:   adds trigger phrase to effect item
     */
    public async addTrigger(phrase) {
        let model = this;
        //create trigger
        let item = {
            trigger_id: 'trig_' + model.effect_id + '_' + Date.now(),
            effect_id: model.effect_id,
            phrase: phrase
        }
        //add trigger
        let addOp = await model.EffectsDb.upsertItem(item, model.EFFECTS_TRIGGERS_COL);
        //add to triggers
        model.triggers.push(item);
        //return added
        return reply(true, 'added trigger', { trigger_id: item.trigger_id });
    }
    //end addItemTrigger()



    /*
     * @Params: trigger_id, phrase
     * @Does:   updates trigger phrase
     */
    public async updateTrigger(trigger_id, phrase) {
        let model = this;
        //set error reply
        let error = null;
        //check if is empty phrase
        if (!phrase.length) error = reply(false, 'phrase cannot be blank');
        //check if is long phrase
        if (phrase.length > 32) error = reply(false, 'phrase cannot be longer than 32 characters');
        //get index of trigger
        let idx = model.triggers.map(item => { return item.trigger_id }).indexOf(trigger_id);
        //check if has item
        if (idx == -1) error = reply(false, 'no matching trigger');
        //check if has error
        if (error != null) {
            //set message
            model.message.setMessage(error.message, '', false, 5000);
            //set item error
            model.input_messages[trigger_id] = { ...model.message };
            //return with error
            return error;
        }
        //create trigger
        let item = {
            trigger_id: trigger_id,
            effect_id: model.effect_id,
            phrase: phrase
        }
        //add trigger
        let addOp = await model.EffectsDb.upsertItem(item, model.EFFECTS_TRIGGERS_COL);
        //update trigger item
        model.triggers[idx] = item;
        //return added
        return reply(true, 'updated trigger', { trigger_id: item.trigger_id });
    }
    //end updateTrigger()




    /*
     * @Params: trigger_id
     * @Does:   removes trigger from effect item
     */
    public async removeTrigger(trigger_id) {
        let model = this;
        //get index of trigger
        let idx = model.triggers.map(item => { return item.trigger_id }).indexOf(trigger_id);
        //check if has item
        if (idx == -1) return reply(false, 'no matching trigger');
        //remove trigger
        let removeOp = await model.EffectsDb.deleteItem(trigger_id, model.EFFECTS_TRIGGERS_COL);
        //remove from triggers
        model.triggers.splice(idx, 1);
        //return removed
        return reply(true, 'removed trigger');
    }
    //end removeItemTrigger()


    //=========================
    /// END TRIGGERS ///





}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}