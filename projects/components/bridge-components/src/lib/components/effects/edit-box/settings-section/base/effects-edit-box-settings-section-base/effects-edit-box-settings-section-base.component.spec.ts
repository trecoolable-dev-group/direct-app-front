import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsEditBoxSettingsSectionBaseComponent } from './effects-edit-box-settings-section-base.component';

describe('EffectsEditBoxSettingsSectionBaseComponent', () => {
  let component: EffectsEditBoxSettingsSectionBaseComponent;
  let fixture: ComponentFixture<EffectsEditBoxSettingsSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsEditBoxSettingsSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsEditBoxSettingsSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
