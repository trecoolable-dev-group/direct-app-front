import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsEditBoxBaseComponent } from './effects-edit-box-base.component';

describe('EffectsEditBoxBaseComponent', () => {
  let component: EffectsEditBoxBaseComponent;
  let fixture: ComponentFixture<EffectsEditBoxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsEditBoxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsEditBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
