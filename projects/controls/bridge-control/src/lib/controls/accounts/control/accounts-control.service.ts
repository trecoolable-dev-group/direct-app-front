import { ItemsStoreService } from '@controls/bridge-control/src/lib/tools/items-store/items-store.service';
import { AccountsItemControlService } from '../item/control/accounts-item-control.service';
import { ItemsDbService } from '../../../tools/items-db/items-db.service';

export class AccountsControlService {

    constructor() {}







    /// DB ///
    //===========================

    //db name
    private db_name = 'accounts_db';

    //collections
    private ACCOUNTS_COL = 'accounts_col';
    private ACCOUNTS_THEMES_COL = 'accounts_themes_col';
    private ACCOUNTS_VOICES_COL = 'accounts_voices_col';

    //effects db stores
    private db_stores = {
        [this.ACCOUNTS_COL]: '&identifier,platform,name',
        [this.ACCOUNTS_THEMES_COL]: '&identifier',
        [this.ACCOUNTS_VOICES_COL]: '&identifier',
    }

    //effects db
    private AccountsDB = new ItemsDbService(this.db_name, this.db_stores, 2);


    //===========================
    /// END DB ///













    /// ACCOUNTS ///
    //===========================

    //accounts
    public AccountItems = new ItemsStoreService('identifier', 100);


    /*
     * @Params:  platform_id, platform
     * @Does:    fetches account item
     *           if no item, adds account to accounts
     * @Return:  audience
     */
    public AccountItem(platform_id, platform): AccountsItemControlService {
        let model = this;
        //fetch account
        let account = model.AccountItems.fetchItem(platform_id + platform);
        //check if has account
        if (account == null) {
            //create new account
            account = new AccountsItemControlService(model.AccountsDB, platform_id + platform);
            //set identifier
            account.identifier = platform_id + platform;
            //set platform_id
            account.platform_id = platform_id;
            //set platform
            account.platform = platform;
            //add to accounts
            model.AccountItems.addItem(platform_id + platform, account);
        }
        //return with account
        return account;
    }
    //end AccountItem()


    //===========================
    /// END ACCOUNTS ///





}