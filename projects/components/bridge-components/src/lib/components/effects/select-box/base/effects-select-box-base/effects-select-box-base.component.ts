import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-effects-select-box-base',
    templateUrl: './effects-select-box-base.component.html',
    styleUrls: ['./effects-select-box-base.component.css']
})
export class EffectsSelectBoxBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set initial effects
        this.setEffects();
    }



    //controls
    public Effects = this.BridgeControl.Effects;








    /// EFFECTS ///
    //=============================

    //list
    public effects_list = [];

    //search input
    public search_input = '';


    /*
     * @Params:  none
     * @Does:    sets effects
     */
    public async setEffects() {
        let model = this;
        //fetch effects
        let fetchOp: any = await model.Effects.Local.searchEffects(model.search_input, model.effects_list.length);
        //check if got effects
        if (fetchOp.success) {
            //set effects
            model.effects_list = fetchOp.data.effects;
        }
    }
    //end setEffects()



    /*
     * @Params:  none
     * @Does:    handles change and searches effects
     */
    public async handleSearchChange() {
        let model = this;
        //unset effects
        model.effects_list = [];
        //fetch effects
        let fetchOp: any = await model.Effects.Local.searchEffects(model.search_input, model.effects_list.length);
        //check if got effects
        if (fetchOp.success) {
            //set effects
            model.effects_list = fetchOp.data.effects;
        }
    }
    //end handleSearchChange()

    //=============================
    /// END EFFECTS ///









    /// SELECT ///
    //===============================

    //select output
    @Output('onSelect') onSelect = new EventEmitter < any > ();


    /*
     * @Params:  effect_id
     * @Does:    triggers on select with effect id
     */
    public handleSelect(effect_id) {
        let model = this;
        //trigger on select
        model.onSelect.emit(effect_id);
    }
    //end handleSelect()

    //===============================
    /// END SELECT ///










    /// DISPLAY ///
    //================================

    //opened flag
    @Input('Opened') Opened = -1;

    //close event
    @Output('onClose') onClose: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  none
     * @Does:    sets opened to false
     */
    public close(force = false) {
        let model = this;
        //set opened to close
        model.Opened = 0;
        //emit close event
        model.onClose.emit();
    }
    //end close()

    //================================
    /// END DISPLAY ///










}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};

//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}