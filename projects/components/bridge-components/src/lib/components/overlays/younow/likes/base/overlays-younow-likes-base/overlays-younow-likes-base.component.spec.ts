import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaysYounowLikesBaseComponent } from './overlays-younow-likes-base.component';

describe('OverlaysYounowLikesBaseComponent', () => {
  let component: OverlaysYounowLikesBaseComponent;
  let fixture: ComponentFixture<OverlaysYounowLikesBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaysYounowLikesBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaysYounowLikesBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
