import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';

@Component({
    selector: 'common-elements-splash-base',
    templateUrl: './splash-base.component.html',
    styleUrls: ['./splash-base.component.css']
})
export class SplashBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}







    /// STYLES ///
    //====================================

    //splash color
    @Input('SplashColor') SplashColor = 'rgba(20, 20, 22, 0.2)';

    //splash size
    @Input('SplashSize') SplashSize = 'large';

    //====================================
    /// END STYLES ///











    /// SPLASH ///
    //=================================

    //splash target element
    @ViewChild('TargetElement') TargetElement: ElementRef;

    //splash event array
    public splash_events = [];


    /*
     * @Params:  event
     * @Does:    gets position of mouse in target
     *           adds splash event
     */
    public splash(event) {
        let model = this;
        //get offset top from browser
        let { left, top } = this.TargetElement.nativeElement.getBoundingClientRect()
        //get mouse coordinates
        let x = event.x - left;
        let y = event.y - top;
        //add splash event
        model.splash_events.push({ x: x, y: y });
        //check if has many events, splice extra
        if (model.splash_events.length > 3) model.splash_events.splice(0, 1);
    }
    //end splash()


    //=================================
    /// END SPLASH ///










}