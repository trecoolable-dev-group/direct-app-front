import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CommonElementsComponent } from './common-elements.component';


import { RadioToggleBaseComponent } from './components/toggles/radio-toggle/base/radio-toggle-base/radio-toggle-base.component';
import { SplashBaseComponent } from './components/decorations/splash/base/splash-base/splash-base.component';
import { StandardButtonBaseComponent } from './components/buttons/standard-button/standard-button-base/standard-button-base.component';
import { LoadTextBaseComponent } from './components/text/load-text/base/load-text-base/load-text-base.component';
import { StandardCheckboxBaseComponent } from './components/checkbox/standard-checkbox/base/standard-checkbox-base/standard-checkbox-base.component';
import { DoubleRangeBaseComponent } from './components/range/double-range/base/double-range-base/double-range-base.component';
import { StandardGenderChoicesBaseComponent } from './components/genders/standard-gender-choices/base/standard-gender-choices-base/standard-gender-choices-base.component';
import { StandardGenderChoicesItemBaseComponent } from './components/genders/standard-gender-choices/item/base/standard-gender-choices-item-base/standard-gender-choices-item-base.component';
import { CountriesSelectOverlayBaseComponent } from './components/select/countries/overlay/base/countries-select-overlay-base/countries-select-overlay-base.component';
import { CountriesSelectOverlayOptionItemComponent } from './components/select/countries/overlay/option/item/countries-select-overlay-option-item/countries-select-overlay-option-item.component';
import { StandardDropdownLabelComponent } from './components/dropdowns/standard-dropdown/label/standard-dropdown-label/standard-dropdown-label.component';
import { StandardDropdownBoxComponent } from './components/dropdowns/standard-dropdown/box/standard-dropdown-box/standard-dropdown-box.component';
import { StandardBirthdayInputBaseComponent } from './components/inputs/birthday/standard-birthday-input/base/standard-birthday-input-base/standard-birthday-input-base.component';
import { StandardDropdownOptionComponent } from './components/dropdowns/standard-dropdown/option/standard-dropdown-option/standard-dropdown-option.component';
import { GendersSelectOverlayBaseComponent } from './components/select/genders/overlay/base/genders-select-overlay-base/genders-select-overlay-base.component';
import { GendersSelectOverlayOptionItemComponent } from './components/select/genders/overlay/option/item/genders-select-overlay-option-item/genders-select-overlay-option-item.component';
import { StandardTextInputBaseComponent } from './components/inputs/standard-text-input/base/standard-text-input-base/standard-text-input-base.component';
import { CountriesSelectBaseComponent } from './components/select/countries/base/countries-select-base/countries-select-base.component';
import { GendersSelectBaseComponent } from './components/select/genders/base/genders-select-base/genders-select-base.component';
import { StandardVideoDisplayBaseComponent } from './components/video/video-display/base/standard-video-display-base/standard-video-display-base.component';

import { CountryObjPipe } from './pipes/country/country-obj/country-obj.pipe';
import { SafeUrlPipe } from './pipes/safe-url/safe-url.pipe';
import { SlideToggleBaseComponent } from './components/toggles/slide-toggle/base/slide-toggle-base/slide-toggle-base.component';
import { SingleRangeBaseComponent } from './components/range/single-range/base/single-range-base/single-range-base.component';
import { VoicesSelectOverlayBaseComponent } from './components/select/voices/overlay/base/voices-select-overlay-base/voices-select-overlay-base.component';
import { VoicesSelectOverlayOptionItemComponent } from './components/select/voices/overlay/option/item/voices-select-overlay-option-item/voices-select-overlay-option-item.component';

import { LottieDisplayBaseComponent } from './components/lottie/lottie-display/base/lottie-display-base/lottie-display-base.component';
import { LoadingAnimationBaseComponent } from './components/lottie/loading-animation/base/loading-animation-base/loading-animation-base.component';



@NgModule({
    declarations: [
        CommonElementsComponent,
        RadioToggleBaseComponent,
        SplashBaseComponent,
        StandardButtonBaseComponent,
        LoadTextBaseComponent,
        StandardCheckboxBaseComponent,
        DoubleRangeBaseComponent,
        StandardGenderChoicesBaseComponent,
        StandardGenderChoicesItemBaseComponent,
        CountriesSelectOverlayBaseComponent,
        CountriesSelectOverlayOptionItemComponent,
        CountryObjPipe,
        StandardDropdownLabelComponent,
        StandardDropdownBoxComponent,
        StandardBirthdayInputBaseComponent,
        StandardDropdownOptionComponent,
        GendersSelectOverlayBaseComponent,
        GendersSelectOverlayOptionItemComponent,
        StandardTextInputBaseComponent,
        CountriesSelectBaseComponent,
        GendersSelectBaseComponent,
        StandardVideoDisplayBaseComponent,
        SafeUrlPipe,
        SlideToggleBaseComponent,
        SingleRangeBaseComponent,
        VoicesSelectOverlayBaseComponent,
        VoicesSelectOverlayOptionItemComponent,
        LottieDisplayBaseComponent,
        LoadingAnimationBaseComponent
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        CommonElementsComponent,
        RadioToggleBaseComponent,
        SplashBaseComponent,
        StandardButtonBaseComponent,
        LoadTextBaseComponent,
        StandardCheckboxBaseComponent,
        DoubleRangeBaseComponent,
        StandardGenderChoicesBaseComponent,
        CountriesSelectOverlayBaseComponent,
        StandardDropdownLabelComponent,
        StandardDropdownBoxComponent,
        StandardBirthdayInputBaseComponent,
        GendersSelectOverlayBaseComponent,
        StandardTextInputBaseComponent,
        CountriesSelectBaseComponent,
        GendersSelectBaseComponent,
        StandardVideoDisplayBaseComponent,
        SlideToggleBaseComponent,
        SingleRangeBaseComponent,
        VoicesSelectOverlayBaseComponent,
        LottieDisplayBaseComponent,
        LoadingAnimationBaseComponent,

        CountryObjPipe,
        SafeUrlPipe
    ]
})
export class CommonElementsModule {}