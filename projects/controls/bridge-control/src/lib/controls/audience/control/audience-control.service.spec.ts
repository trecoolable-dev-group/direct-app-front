import { TestBed } from '@angular/core/testing';

import { AudienceControlService } from './audience-control.service';

describe('AudienceControlService', () => {
  let service: AudienceControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AudienceControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
