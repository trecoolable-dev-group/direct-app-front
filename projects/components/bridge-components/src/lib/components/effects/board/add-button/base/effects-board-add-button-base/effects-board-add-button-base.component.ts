import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bridge-elements-effects-board-add-button-base',
  templateUrl: './effects-board-add-button-base.component.html',
  styleUrls: ['./effects-board-add-button-base.component.css']
})
export class EffectsBoardAddButtonBaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
