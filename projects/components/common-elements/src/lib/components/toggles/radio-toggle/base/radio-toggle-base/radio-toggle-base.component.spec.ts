import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioToggleBaseComponent } from './radio-toggle-base.component';

describe('RadioToggleBaseComponent', () => {
  let component: RadioToggleBaseComponent;
  let fixture: ComponentFixture<RadioToggleBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioToggleBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioToggleBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
