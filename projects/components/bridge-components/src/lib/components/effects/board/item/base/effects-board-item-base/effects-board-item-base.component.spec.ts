import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsBoardItemBaseComponent } from './effects-board-item-base.component';

describe('EffectsBoardItemBaseComponent', () => {
  let component: EffectsBoardItemBaseComponent;
  let fixture: ComponentFixture<EffectsBoardItemBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsBoardItemBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsBoardItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
