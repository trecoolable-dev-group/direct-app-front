import { TestBed } from '@angular/core/testing';

import { EffectsPlayingControlService } from './effects-playing-control.service';

describe('EffectsPlayingControlService', () => {
  let service: EffectsPlayingControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EffectsPlayingControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
