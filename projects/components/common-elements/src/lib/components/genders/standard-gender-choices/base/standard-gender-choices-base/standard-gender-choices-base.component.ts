import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'common-elements-standard-gender-choices-base',
    templateUrl: './standard-gender-choices-base.component.html',
    styleUrls: ['./standard-gender-choices-base.component.css']
})
export class StandardGenderChoicesBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}








    /// STYLES ///
    //============================

    //choices class
    @Input('ChoiceClass') ChoiceClass = 'std-height padding-side-med margin-side-small margin-vert';

    //icon class
    @Input('IconClass') IconClass = 'small-square margin-side-small margin-vert';

    //label class
    @Input('LabelClass') LabelClass = 'bold-text margin-side-small margin-vert';

    //============================
    /// END STYLES ///













    /// CHOICES ///
    //==================================

    //select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ();

    //current value
    @Input('Value') Value = '';

    //choices
    public choices = [
        { label: 'Female', value: 'f', color: '#ff48a8', icon: 'https://static.thenounproject.com/png/429451-200.png' },
        { label: 'Male', value: 'm', color: '#24b2ff', icon: 'https://static.thenounproject.com/png/1258541-200.png' },
        { label: 'Any', value: '', color: '#8b47ff', icon: 'https://static.thenounproject.com/png/31479-200.png' },
    ];


    /*
     * @Params:  value
     * @Does:    sets choice
     */
    public setChoice(value) {
        let model = this;
        //get index of value
        let idx = model.choices.map(item => { return item.value }).indexOf(value);
        //check if has item
        if (idx == -1) return;
        //has index, set value
        model.Value = value;
        //emit select
        model.onSelect.emit(value);
    }
    //end setChoice()

    //==================================
    /// END CHOICES ///








}