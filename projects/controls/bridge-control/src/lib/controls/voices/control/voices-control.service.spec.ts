import { TestBed } from '@angular/core/testing';

import { VoicesControlService } from './voices-control.service';

describe('VoicesControlService', () => {
  let service: VoicesControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VoicesControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
