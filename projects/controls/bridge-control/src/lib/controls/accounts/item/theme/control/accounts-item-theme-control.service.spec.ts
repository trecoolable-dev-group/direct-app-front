import { TestBed } from '@angular/core/testing';

import { AccountsItemThemeControlService } from './accounts-item-theme-control.service';

describe('AccountsItemThemeControlService', () => {
  let service: AccountsItemThemeControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountsItemThemeControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
