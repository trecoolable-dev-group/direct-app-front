import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoubleRangeBaseComponent } from './double-range-base.component';

describe('DoubleRangeBaseComponent', () => {
  let component: DoubleRangeBaseComponent;
  let fixture: ComponentFixture<DoubleRangeBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoubleRangeBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoubleRangeBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
