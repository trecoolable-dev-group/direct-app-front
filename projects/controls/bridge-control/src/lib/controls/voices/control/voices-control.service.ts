import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';
import { ItemMessageService } from '@controls/bridge-control/src/lib/tools/item-message/item-message.service';


export class VoicesControlService {

    constructor() {
        //set voices
        this.setVoiceOptions();
        //set settings
        this.fetchSettings();
    }




    /// DB ///
    //===========================

    //db name
    private db_name = 'voices_db';

    //collections
    private VOICES_SETTINGS_COL = 'voices_settings_col';

    //voices db stores
    private db_stores = {
        [this.VOICES_SETTINGS_COL]: '&_id',
    }

    //effects db
    private VoicesDB = new ItemsDbService(this.db_name, this.db_stores, 2);


    //===========================
    /// END DB ///










    /// MESSAGE ///
    //=================================

    //message
    public message = new ItemMessageService();

    //messages
    public input_messages = {};

    //=================================
    /// END MESSAGE ///













    /// VOICES ///
    //============================

    //settings
    public settings = {
        voices_enabled: false,
        default_voice: '',
        volume: 0.7,
        character_limit: 120,
        spam_filter: true,
        names_enabled: false,
    };

    //settings inputs
    public settings_inputs = { ...this.settings };

    //has info flag
    public has_info = false;


    /*
     * @Params:  none
     * @Does:    checks if has settings
     *           fetches stored voice settings
     * @return:  settings
     */
    public async fetchSettings() {
        let model = this;
        //fetch settings
        let fetchOp: any = await model.VoicesDB.findItemsByKey('_id', 'settings', model.VOICES_SETTINGS_COL, 1);
        //check if has settings
        if (!fetchOp.data.items.length) return reply(true, 'no settings', { settings: model.settings });
        //has settings, set settings
        let settings = fetchOp.data.items[0];
        //set keys
        let keys = Object.keys(model.settings);
        //iterate and set keys
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //set key
            if (settings[key] != undefined) model.settings[key] = settings[key];
        }
        //set inputs
        model.settings_inputs = { ...model.settings };
        //return with key
        return reply(true, 'got settings', { settings: model.settings });
    }
    //end fetchSettings()



    /*
     * @Params:  none
     * @Does:    saves settings
     */
    public saveSettings() {
        let model = this;
        //unset message
        model.message.clearMessage();
        //fetch changes
        let keys = model.fetchChanges();
        //set info to inputs
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //set changes
            model.settings[key] = model.settings_inputs[key];
        }
        //set save item
        let item = {
            _id: 'settings',
            ...model.settings
        };
        //save settings
        model.VoicesDB.upsertItem(item, model.VOICES_SETTINGS_COL);
        //set saved message
        model.message.setMessage('saved settings', '', true, 5000);
        //return saved
        return reply(true, 'saved settings');
    }
    //end saveSettings()



    /*
     * @Params:  key, value
     * @Does:    validates input
     *           sets input key
     *           sets has change
     */
    public handleChange(key, value) {
        let model = this;
        //set input key
        model.settings_inputs[key] = value;
    }
    //end handleChange()



    /*
     * @Params:  none
     * @Does:    checks if has changes
     * @Return:  changes
     */
    public fetchChanges() {
        let model = this;
        //set changes array
        let changes = [];
        //change keys
        let keys = [
            'voices_enabled',
            'default_voice',
            'volume',
            'character_limit',
            'spam_filter',
            'names_enabled',
        ];
        //iterate through keys
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //check fi is different
            if (model.settings[key] != model.settings_inputs[key]) changes.push(key);
        }
        //return changes
        return changes;
    }
    //end fetchChanges()



    /*
     * @Params:  none
     * @Does:    resets inputs to original
     */
    public resetChanges() {
        let model = this;
        //set settings to previous
        model.settings_inputs = { ...model.settings };
    }
    //end resetChanges()



    /*
     * @Params: none
     * @Does:   toggles voices enabled
     */
    public toggleVoices() {
        let model = this;
        //toggle voices
        model.settings_inputs.voices_enabled = !model.settings_inputs.voices_enabled;
        //save settings
        model.saveSettings();
    }
    //end toggleVoices()



    /*
     * @Params: none
     * @Does:   toggles names enabled
     */
    public toggleNames() {
        let model = this;
        //toggle voices
        model.settings_inputs.names_enabled = !model.settings_inputs.names_enabled;
        //save settings
        model.saveSettings();
    }
    //end toggleNames()



    /*
     * @Params:  none
     * @Does:    toggles spam enabled
     */
    public toggleSpam() {
        let model = this;
        //toggle voices
        model.settings_inputs.spam_filter = !model.settings_inputs.spam_filter;
        //save settings
        model.saveSettings();
    }
    //end toggleSpam()


    //============================
    /// END VOICES ///














    /// SPEECH ///
    //==============================

    private speech_items = [];

    //playing items
    private playing_item = null;


    /*
     * @Params:  content, options, ref
     * @Does:    adds speech item to items
     *           checks if has playing over overlap limit
     *           if no overlap, plays speech item
     *           checks if is over limit, slices speech items
     */
    public addSpeechItem(content, options = null, ref = Math.random().toString(36).slice(2)) {
        let model = this;
        try {
            //check if options is null
            if (options == null) options = {};
            //format content
            content = model.formatContent(content)
            //check if no content
            if (!content.length) return;
            //set utterance
            let utter = new SpeechSynthesisUtterance(content);
            //set options
            utter = model.applyVoiceOptions(utter, options);
            //set on end
            utter.onend = () => {
                //unset item
                model.playing_item = null;
                //play next
                model.nextSpeechItem();
            };
            //set speech item
            let item = {
                ref: ref,
                utter: utter
            };
            //add item to speech items
            model.speech_items.push(item);
            //check if has more than 10 items
            if (model.speech_items.length > 15) model.speech_items = model.speech_items.slice(1, 16);
            //play speech item
            model.nextSpeechItem();
        } catch (error) {
            console.log('voice error');
        }
    }
    //end addSpeechItem()



    /*
     * @Params:  none
     * @Does:    checks if has currently playing items
     *           plays item, sets handler for remove
     */
    public nextSpeechItem() {
        let model = this;
        try {
            //check if has speech item
            if (!model.speech_items.length) return;
            //is under overlap limit, pop item
            let item = model.speech_items.pop();
            //set playing
            model.playing_item = item;
            //resume speech
            speechSynthesis.resume();
            //play speech
            speechSynthesis.speak(item.utter);
        } catch (error) {
            console.log('lupe');
        }
    }
    //end nextSpeechItem()



    /*
     * @Params:  none
     * @Does:    stops first currently playing item
     *           triggers next speech item
     */
    public skipItem() {
        let model = this;
        try {
            //check if has playing
            if (model.playing_item == null) return;
            //stop utterance
            speechSynthesis.pause();
            //unset item
            model.playing_item = null;
            //resume speech
            speechSynthesis.resume();
            //play next
            model.nextSpeechItem();
        } catch (error) {
            console.log('luper');
        }
    }
    //end skipItem()



    /*
     * @Params:  utter, options
     * @Does:    sets voice options to utterance
     * @Return:  utter
     */
    public applyVoiceOptions(utter, options: any = {}) {
        let model = this;
        //check if has voice name
        if (options.voice) {
            //get index of name
            let idx = model.voices.map(item => { return item.name }).indexOf(options.voice);
            //set voice
            if (idx != -1) utter.voice = model.voices[idx];
        }
        //check if has default voice
        else if (model.settings.default_voice.length) {
            //set default voice
            let idx = model.voices.map(item => { return item.name }).indexOf(model.settings.default_voice);
            //set default voice
            if (idx != -1) utter.voice = model.voices[idx];
        }
        //check if has voice pitch
        if (options.pitch) utter.pitch = options.pitch;
        //check if has speed
        if (options.speed) utter.rate = options.speed;
        //set rate
        utter.volume = model.settings.volume;
        //return utter
        return utter;
    }
    //end applyVoiceOptions()



    /*
     * @Params:  content
     * @Does:    formats content and applies transformations
     * @Return:  content
     */
    public formatContent(content) {
        let model = this;
        //remove emojis
        content = content.replace(/[^\p{L}\p{N}\p{P}\p{Z}]/gu, '');
        //filter for spam
        content = model.filterSpam(content);
        //check if is hashtag
        if (content.split('#').join('').split(' ').length == 1 && content == content.toUpperCase()) {
            //replace with hashtag
            content = content.replace('#', 'big thanks ');
        }
        //set to limit
        content = content.slice(0, model.settings.character_limit);
        //unset event tracker
        content = content.replace('event tracker', ', trecoolable\'s mattybot, ');
        //unset phoque
        content = content.replace('phoque', 'fuck');
        //unset put sea
        content = content.replace('put sea', 'pussy');
        //unset mattybot
        content = content.replace('mattybot', 'the best freaking stream addon mattybot');
        //unset purple site
        content = content.replace('purple site', 'twitch');
        //check if is a link
        if (content.includes('http')) content = 'link';
        //return content
        return content;
    }
    //end formatContent()



    /*
     * @Params:  content
     * @Does:    filters spam from content
     * @Return:  content
     */
    public filterSpam(content) {
        let model = this;
        //check if no spam filter
        if (!model.settings.spam_filter) return content;
        //check if content is under
        if (content.length < 20) return content;
        //split into parts
        let parts = content.split(' ');
        //check if has one part and longer
        if (parts.length == 1 && parts[0].length > 26) {
            //trim parts
            parts[0] = parts[0].slice(0, 10);
        }
        //iterate through parts
        for (let i = 0; i < parts.length; i++) {
            //set item
            let item = parts[i];
            //check if has more than 1
            if (parts.filter(slice => { return (slice == item) }).length > 5) parts[i] = '';
            //check if is not long
            if (item.length > 15) {
                //set parts repeat items
                let single = item.split(item.slice(0, 1)).filter(slice => { return (slice.length) });
                let double = item.split(item.slice(0, 2)).filter(slice => { return (slice.length) });
                let triple = item.split(item.slice(0, 3)).filter(slice => { return (slice.length) });
                //check if has spam
                if (single.length < 3 || double.length < 3 || triple.length < 3) {
                    //is spam, set item
                    if (single.length < double.length && single.length < triple.length) {
                        //set single as content
                        parts[i] = single[0];
                    } else if (double.length < triple.length) {
                        //set double as content
                        parts[i] = double[0];
                    } else {
                        //set triple as content
                        parts[i] = triple[0];
                    }
                }
            }
        }
        //return with content
        return parts.join(' ');
    }
    //end fiterSpam()

    //==============================
    /// END SPEECH ///











    /// VOICE OPTIONS ///
    //===============================

    //available voices
    public voices = [];


    /*
     * @Params:  none
     * @Does:    sets voice options
     */
    public async setVoiceOptions() {
        let model = this;
        try {
            //check if has voices
            if (model.voices.length) return;
            //set voices
            let voices = speechSynthesis.getVoices();
            //check if has voices
            if (!voices.length) {
                //set voices listener
                speechSynthesis.onvoiceschanged = () => {
                    //set voices
                    voices = speechSynthesis.getVoices();
                    //set voices
                    model.voices = voices;
                }
            } else {
                //set voices
                model.voices = voices;
            }
        } catch (error) { console.error(error) }
    }
    //end setVoiceOptions()

    //===============================
    /// END VOICE OPTIONS ///





}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}