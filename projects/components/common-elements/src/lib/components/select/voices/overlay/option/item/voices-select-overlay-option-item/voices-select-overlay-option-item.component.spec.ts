import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoicesSelectOverlayOptionItemComponent } from './voices-select-overlay-option-item.component';

describe('VoicesSelectOverlayOptionItemComponent', () => {
  let component: VoicesSelectOverlayOptionItemComponent;
  let fixture: ComponentFixture<VoicesSelectOverlayOptionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoicesSelectOverlayOptionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoicesSelectOverlayOptionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
