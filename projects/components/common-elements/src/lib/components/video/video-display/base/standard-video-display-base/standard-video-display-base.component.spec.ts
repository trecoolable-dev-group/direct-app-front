import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardVideoDisplayBaseComponent } from './standard-video-display-base.component';

describe('StandardVideoDisplayBaseComponent', () => {
  let component: StandardVideoDisplayBaseComponent;
  let fixture: ComponentFixture<StandardVideoDisplayBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardVideoDisplayBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardVideoDisplayBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
