import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'common-elements-single-range-base',
    templateUrl: './single-range-base.component.html',
    styleUrls: ['./single-range-base.component.css']
})
export class SingleRangeBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}




    /// STYLES ///
    //===========================

    //bar height
    @Input('BarHeight') BarHeight = 25;

    //bar class
    @Input('BarClass') BarClass = 'light-round';

    //bar class
    @Input('DotSize') DotSize = '';

    //bar color
    @Input('BarColor') BarColor = 'green';

    //min color
    @Input('MinColor') MinColor = 'red';

    //max color
    @Input('MaxColor') MaxColor = 'yellow';

    //label class
    @Input('LabelClass') LabelClass = '';

    //values class
    @Input('ValuesClass') ValuesClass = '';


    //===========================
    /// END STYLES ///










    /// VALUES ///
    //===============================

    //label
    @Input('Label') Label = '';

    //current top value
    @Input('TopValue') TopValue = 50;

    //step value
    @Input('Step') Step = 1;

    //max value
    @Input('MaxValue') MaxValue = 100;

    //max warn
    @Input('MaxWarn') MaxWarn = -1;

    //min value
    @Input('MinValue') MinValue = 0;

    //min warn
    @Input('MinWarn') MinWarn = -1;

    //===============================
    /// END VALUES ///













    /// CHANGE ///
    //=================================

    //top event
    @Output('onTop') onTop: EventEmitter < any > = new EventEmitter < any > ();

    //top ping flag
    public top_ping = false;


    /*
     *
     * @Does:    checks if is 
     */
    public async handleTopChange() {
        let model = this;
        //check if is over max warn
        if (model.TopValue > model.MaxWarn && model.MaxWarn != -1) model.TopValue = model.MaxWarn;
        //trigger on top
        model.onTop.emit(model.TopValue);
    }
    //end handleTopChange()


    //=================================
    /// END CHANGE ///







}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}