import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricsYounowLikesBaseComponent } from './metrics-younow-likes-base.component';

describe('MetricsYounowLikesBaseComponent', () => {
  let component: MetricsYounowLikesBaseComponent;
  let fixture: ComponentFixture<MetricsYounowLikesBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricsYounowLikesBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricsYounowLikesBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
