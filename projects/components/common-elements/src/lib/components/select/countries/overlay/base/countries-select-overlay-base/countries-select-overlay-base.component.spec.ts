import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountriesSelectOverlayBaseComponent } from './countries-select-overlay-base.component';

describe('CountriesSelectOverlayBaseComponent', () => {
  let component: CountriesSelectOverlayBaseComponent;
  let fixture: ComponentFixture<CountriesSelectOverlayBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountriesSelectOverlayBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountriesSelectOverlayBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
