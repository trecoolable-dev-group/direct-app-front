import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsSettingsAudienceSectionBaseComponent } from './events-settings-audience-section-base.component';

describe('EventsSettingsAudienceSectionBaseComponent', () => {
  let component: EventsSettingsAudienceSectionBaseComponent;
  let fixture: ComponentFixture<EventsSettingsAudienceSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsSettingsAudienceSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsSettingsAudienceSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
