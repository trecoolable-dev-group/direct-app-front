import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaysEffectsItemAudioBaseComponent } from './overlays-effects-item-audio-base.component';

describe('OverlaysEffectsItemAudioBaseComponent', () => {
  let component: OverlaysEffectsItemAudioBaseComponent;
  let fixture: ComponentFixture<OverlaysEffectsItemAudioBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaysEffectsItemAudioBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaysEffectsItemAudioBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
