import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'bridge-elements-events-box-base',
    templateUrl: './events-box-base.component.html',
    styleUrls: ['./events-box-base.component.css']
})
export class EventsBoxBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}





    /// LIST ///
    //==========================

    //mode
    public mode = 'events';


    /*
     * @Params:  mode
     * @Does:    changes mode
     */
    public async changeMode(mode) {
        let model = this;
        //change mode
        model.mode = mode;
    }
    //end changeMode()

    //==========================
    /// END LIST ///






}