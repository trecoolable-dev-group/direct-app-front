import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bridge-elements-events-settings-filters-section-base',
  templateUrl: './events-settings-filters-section-base.component.html',
  styleUrls: ['./events-settings-filters-section-base.component.css']
})
export class EventsSettingsFiltersSectionBaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
