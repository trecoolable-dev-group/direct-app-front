import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleRangeBaseComponent } from './single-range-base.component';

describe('SingleRangeBaseComponent', () => {
  let component: SingleRangeBaseComponent;
  let fixture: ComponentFixture<SingleRangeBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleRangeBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleRangeBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
