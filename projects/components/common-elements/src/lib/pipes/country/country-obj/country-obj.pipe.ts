import { Pipe, PipeTransform } from '@angular/core';
import * as CountriesList from 'projects/reference/countries.json'

@Pipe({
    name: 'countryObj'
})
export class CountryObjPipe implements PipeTransform {

    transform(code: any): { code: String, name: String, flag: String } {
        //check if is undefined
        if (code == undefined) return {
            code: '',
            name: 'unknown',
            flag: ''
        };
        //set lowercase
        code = code.toLowerCase();
        //get idx of country
        let idx = CountriesList.countries.map(item => { return item.code }).indexOf(code);
        //return with obj
        return {
            code: code,
            name: (idx != -1) ? CountriesList.countries[idx].name : 'unkown',
            flag: (idx != -1) ? 'https://www.countryflags.io/' + code + '/shiny/64.png' : 'https://i.pinimg.com/originals/dd/d5/0c/ddd50c7fd01a3a3927b932d8a5d4857c.png'
        }
    }

}