import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricsYounowBaseComponent } from './metrics-younow-base.component';

describe('MetricsYounowBaseComponent', () => {
  let component: MetricsYounowBaseComponent;
  let fixture: ComponentFixture<MetricsYounowBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricsYounowBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricsYounowBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
