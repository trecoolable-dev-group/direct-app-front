import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftsSettingsBracketsSectionItemBaseComponent } from './gifts-settings-brackets-section-item-base.component';

describe('GiftsSettingsBracketsSectionItemBaseComponent', () => {
  let component: GiftsSettingsBracketsSectionItemBaseComponent;
  let fixture: ComponentFixture<GiftsSettingsBracketsSectionItemBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftsSettingsBracketsSectionItemBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftsSettingsBracketsSectionItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
