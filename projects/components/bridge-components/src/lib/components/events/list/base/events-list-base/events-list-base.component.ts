import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-events-list-base',
    templateUrl: './events-list-base.component.html',
    styleUrls: ['./events-list-base.component.css']
})
export class EventsListBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        let model = this;
        setTimeout(() => {
            //set events list element
            this.Events.EventsListElement = this.EventsListElement;
        }, 2000);
    }



    //controls
    public Events = this.BridgeControl.Events;




    /// LIST ///
    //=======================

    //events list
    @ViewChild('EventsListElement') EventsListElement: ElementRef;

    //=======================
    /// END LIST ///



}