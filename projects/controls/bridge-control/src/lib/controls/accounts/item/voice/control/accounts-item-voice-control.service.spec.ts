import { TestBed } from '@angular/core/testing';

import { AccountsItemVoiceControlService } from './accounts-item-voice-control.service';

describe('AccountsItemVoiceControlService', () => {
  let service: AccountsItemVoiceControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountsItemVoiceControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
