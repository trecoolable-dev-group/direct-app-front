import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';


import { BridgeComponentsModule } from '@components/bridge-components/src/public-api';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BridgeComponentsModule
    ],
    providers: [{
        provide: 'env',
        useValue: environment,
        useFactory: BridgeControlServiceFactory
    }],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}

//bridge service factory
export function BridgeControlServiceFactory() {
    return new BridgeControlService(environment);
}