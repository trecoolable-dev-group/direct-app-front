import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GendersSelectOverlayBaseComponent } from './genders-select-overlay-base.component';

describe('GendersSelectOverlayBaseComponent', () => {
  let component: GendersSelectOverlayBaseComponent;
  let fixture: ComponentFixture<GendersSelectOverlayBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GendersSelectOverlayBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GendersSelectOverlayBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
