import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindingBoxBaseComponent } from './binding-box-base.component';

describe('BindingBoxBaseComponent', () => {
  let component: BindingBoxBaseComponent;
  let fixture: ComponentFixture<BindingBoxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindingBoxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindingBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
