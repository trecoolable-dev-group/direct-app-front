import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsItemSettingsBoxChatSectionBaseComponent } from './accounts-item-settings-box-chat-section-base.component';

describe('AccountsItemSettingsBoxChatSectionBaseComponent', () => {
  let component: AccountsItemSettingsBoxChatSectionBaseComponent;
  let fixture: ComponentFixture<AccountsItemSettingsBoxChatSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsItemSettingsBoxChatSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsItemSettingsBoxChatSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
