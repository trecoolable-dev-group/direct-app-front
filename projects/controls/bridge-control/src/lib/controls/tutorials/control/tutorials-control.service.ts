import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';


export class TutorialsControlService {

    constructor() {}




    /// DB ///
    //===========================

    //db name
    private db_name = 'tutorials_db';

    //collections
    private TUTS_COMPLETED_COL = 'tuts_completed_col';

    //voices db stores
    private db_stores = {
        [this.TUTS_COMPLETED_COL]: '&tutorial_id',
    }

    //effects db
    private TutorialsDB = new ItemsDbService(this.db_name, this.db_stores, 2);


    //===========================
    /// END DB ///













    /// DISPLAY ///
    //============================

    //display id
    public display_id = '';

    //display items
    public display_items = [];


    /*
     * @Params:  tutorial_id, display_id
     * @Does:    adds display tutorial item
     */
    public addDisplayItem(tutorial_id, display_id) {
        let model = this;
        //get index of item with tutorial id
        let idx = model.display_items.map(item => { return item.tutorial_id }).indexOf(tutorial_id);
        //check if has display item with tutorial
        if (idx != -1) return;
        //does not have display id
        model.display_items.push({ tutorial_id: tutorial_id, display_id: display_id });
        //check if has display
        if (!model.display_id.length) model.display_id = display_id;
    }
    //end addDisplayItem()



    /*
     * @Params:  display_id
     * @Does:    removes display item
     */
    public removeDisplayItem(display_id) {
        let model = this;
        //get index of item with display id
        let idx = model.display_items.map(item => { return item.display_id }).indexOf(display_id);
        //check if has display
        if (idx == -1) return;
        //unset display id
        model.display_id = '';
        //remove item
        model.display_items.splice(idx, 1);
        //check if has next item
        if (model.display_items.length) {
            //set display id
            model.display_id = model.display_items[0].display_id;
        }
    }
    //end removeDisplayItem()


    //============================
    /// END DISPLAY ///














    /// ITEMS ///
    //============================

    /*
     * @Params:  tutorial_id
     * @Does:    checks if tutorial id is in completed
     * @Return:  completed
     */
    public async checkCompleted(tutorial_id) {
        let model = this;
        //fetch completed
        let fetchOp: any = await model.TutorialsDB.findItemsByKey('tutorial_id', tutorial_id, model.TUTS_COMPLETED_COL);
        //check if success
        if (!fetchOp.success) return reply(false, 'could not get completed');
        //check if has item
        return reply(true, 'got completed', { completed: (fetchOp.data.items.length) ? true : false });
    }
    //end checkCompleted()



    /*
     * @Params:  tutorial_id
     * @Does:    adds or updates tutorial_id in completed
     * @Return:  completed
     */
    public addCompleted(tutorial_id) {
        let model = this;
        //add to completed
        model.TutorialsDB.upsertItem({ tutorial_id: tutorial_id, completed: Date.now() }, model.TUTS_COMPLETED_COL);
    }
    //end addCompleted()


    //============================
    /// END ITEMS ///







}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}