import { TestBed } from '@angular/core/testing';

import { ChatsControlService } from './chats-control.service';

describe('ChatsControlService', () => {
  let service: ChatsControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChatsControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
