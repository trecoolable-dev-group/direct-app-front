import { TestBed } from '@angular/core/testing';

import { EffectsItemSettingsControlService } from './effects-item-settings-control.service';

describe('EffectsItemSettingsControlService', () => {
  let service: EffectsItemSettingsControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EffectsItemSettingsControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
