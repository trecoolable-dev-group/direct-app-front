import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonElementsModule } from '@components/common-elements/src/public-api';

//home
import { HomePageBaseComponent } from './components/home/page/base/home-page-base/home-page-base.component';
import { BindingBoxBaseComponent } from './components/binding/box/base/binding-box-base/binding-box-base.component';
import { BindingBoxYounowBaseComponent } from './components/binding/box/younow/base/binding-box-younow-base/binding-box-younow-base.component';
import { BindingBoxYounowInputBaseComponent } from './components/binding/box/younow/input/base/binding-box-younow-input-base/binding-box-younow-input-base.component';
import { BindingBoxYounowInfoBaseComponent } from './components/binding/box/younow/info/base/binding-box-younow-info-base/binding-box-younow-info-base.component';
import { EventsListBaseComponent } from './components/events/list/base/events-list-base/events-list-base.component';
import { EventsListChatItemBaseComponent } from './components/events/list/chat-item/base/events-list-chat-item-base/events-list-chat-item-base.component';
import { EventsListGiftItemBaseComponent } from './components/events/list/gift-item/base/events-list-gift-item-base/events-list-gift-item-base.component';
import { EventsListStatusItemBaseComponent } from './components/events/list/status-item/base/events-list-status-item-base/events-list-status-item-base.component';
import { MetricsYounowBaseComponent } from './components/metrics/younow/base/metrics-younow-base/metrics-younow-base.component';
import { MetricsYounowLikesBaseComponent } from './components/metrics/younow/likes/base/metrics-younow-likes-base/metrics-younow-likes-base.component';
import { MetricsYounowViewersBaseComponent } from './components/metrics/younow/viewers/base/metrics-younow-viewers-base/metrics-younow-viewers-base.component';
import { MetricsYounowFansBaseComponent } from './components/metrics/younow/fans/base/metrics-younow-fans-base/metrics-younow-fans-base.component';
import { EventsSettingsBaseComponent } from './components/events/settings/base/events-settings-base/events-settings-base.component';
import { EventsSettingsVoicesSectionBaseComponent } from './components/events/settings/voices-section/base/events-settings-voices-section-base/events-settings-voices-section-base.component';
import { EventsSettingsSpamSectionBaseComponent } from './components/events/settings/spam-section/base/events-settings-spam-section-base/events-settings-spam-section-base.component';
import { EventsSettingsAudienceSectionBaseComponent } from './components/events/settings/audience-section/base/events-settings-audience-section-base/events-settings-audience-section-base.component';
import { EventsBoxBaseComponent } from './components/events/box/base/events-box-base/events-box-base.component';
import { EventsSettingsFiltersSectionBaseComponent } from './components/events/settings/filters-section/base/events-settings-filters-section-base/events-settings-filters-section-base.component';
import { OverlaysBoxBaseComponent } from './components/overlays/box/base/overlays-box-base/overlays-box-base.component';
import { OverlaysYounowLikesBaseComponent } from './components/overlays/younow/likes/base/overlays-younow-likes-base/overlays-younow-likes-base.component';
import { OverlaysYounowLikesSettingsBoxBaseComponent } from './components/overlays/younow/likes/settings-box/base/overlays-younow-likes-settings-box-base/overlays-younow-likes-settings-box-base.component';
import { OverlaysYounowTopFansBaseComponent } from './components/overlays/younow/top-fans/base/overlays-younow-top-fans-base/overlays-younow-top-fans-base.component';
import { OverlaysEffectsBaseComponent } from './components/overlays/effects/base/overlays-effects-base/overlays-effects-base.component';
import { OverlaysEffectsItemBaseComponent } from './components/overlays/effects/item/base/overlays-effects-item-base/overlays-effects-item-base.component';
import { AccountsItemSettingsBoxBaseComponent } from './components/accounts/item/settings-box/base/accounts-item-settings-box-base/accounts-item-settings-box-base.component';
import { AccountsItemSettingsBoxVoiceSectionBaseComponent } from './components/accounts/item/settings-box/voice-section/base/accounts-item-settings-box-voice-section-base/accounts-item-settings-box-voice-section-base.component';
import { AccountsItemSettingsBoxChatSectionBaseComponent } from './components/accounts/item/settings-box/chat-section/base/accounts-item-settings-box-chat-section-base/accounts-item-settings-box-chat-section-base.component';
import { AccountsItemSettingsBoxEffectsSectionBaseComponent } from './components/accounts/item/settings-box/effects-section/base/accounts-item-settings-box-effects-section-base/accounts-item-settings-box-effects-section-base.component';
import { EffectsBoardBaseComponent } from './components/effects/board/base/effects-board-base/effects-board-base.component';
import { EffectsBoardItemBaseComponent } from './components/effects/board/item/base/effects-board-item-base/effects-board-item-base.component';
import { EffectsSelectBoxBaseComponent } from './components/effects/select-box/base/effects-select-box-base/effects-select-box-base.component';
import { EffectsSelectBoxItemBaseComponent } from './components/effects/select-box/item/base/effects-select-box-item-base/effects-select-box-item-base.component';
import { EffectsEditBoxBaseComponent } from './components/effects/edit-box/base/effects-edit-box-base/effects-edit-box-base.component';
import { EffectsEditBoxFileSectionBaseComponent } from './components/effects/edit-box/file-section/base/effects-edit-box-file-section-base/effects-edit-box-file-section-base.component';
import { EffectsEditBoxInfoSectionBaseComponent } from './components/effects/edit-box/info-section/base/effects-edit-box-info-section-base/effects-edit-box-info-section-base.component';
import { EffectsEditBoxSettingsSectionBaseComponent } from './components/effects/edit-box/settings-section/base/effects-edit-box-settings-section-base/effects-edit-box-settings-section-base.component';
import { EffectsEditBoxTriggersSectionBaseComponent } from './components/effects/edit-box/triggers-section/base/effects-edit-box-triggers-section-base/effects-edit-box-triggers-section-base.component';
import { EffectsEditBoxInfoSectionFileInputBaseComponent } from './components/effects/edit-box/info-section/file-input/base/effects-edit-box-info-section-file-input-base/effects-edit-box-info-section-file-input-base.component';
import { EffectsEditBoxSettingsSectionDurationInputBaseComponent } from './components/effects/edit-box/settings-section/duration-input/base/effects-edit-box-settings-section-duration-input-base/effects-edit-box-settings-section-duration-input-base.component';
import { EffectsEditBoxSettingsSectionPositionInputBaseComponent } from './components/effects/edit-box/settings-section/position-input/base/effects-edit-box-settings-section-position-input-base/effects-edit-box-settings-section-position-input-base.component';
import { EffectsEditBoxSettingsSectionVolumeInputBaseComponent } from './components/effects/edit-box/settings-section/volume-input/base/effects-edit-box-settings-section-volume-input-base/effects-edit-box-settings-section-volume-input-base.component';
import { EffectsEditBoxTriggersSectionItemBaseComponent } from './components/effects/edit-box/triggers-section/item/base/effects-edit-box-triggers-section-item-base/effects-edit-box-triggers-section-item-base.component';
import { EffectsAddBoxBaseComponent } from './components/effects/add-box/base/effects-add-box-base/effects-add-box-base.component';
import { EffectsAddBoxFileInputBaseComponent } from './components/effects/add-box/file-input/base/effects-add-box-file-input-base/effects-add-box-file-input-base.component';
import { OverlaysEffectsItemVideoBaseComponent } from './components/overlays/effects/item/video/base/overlays-effects-item-video-base/overlays-effects-item-video-base.component';
import { OverlaysEffectsItemGifBaseComponent } from './components/overlays/effects/item/gif/base/overlays-effects-item-gif-base/overlays-effects-item-gif-base.component';
import { OverlaysEffectsItemAudioBaseComponent } from './components/overlays/effects/item/audio/base/overlays-effects-item-audio-base/overlays-effects-item-audio-base.component';
import { GiftsBoxBaseComponent } from './components/gifts/box/base/gifts-box-base/gifts-box-base.component';
import { GiftsSettingsBaseComponent } from './components/gifts/settings/base/gifts-settings-base/gifts-settings-base.component';
import { GiftsSettingsBracketsSectionBaseComponent } from './components/gifts/settings/brackets-section/base/gifts-settings-brackets-section-base/gifts-settings-brackets-section-base.component';
import { GiftsSettingsBracketsSectionItemBaseComponent } from './components/gifts/settings/brackets-section/item/base/gifts-settings-brackets-section-item-base/gifts-settings-brackets-section-item-base.component';
import { GiftsSettingsBracketsSectionItemEffectItemBaseComponent } from './components/gifts/settings/brackets-section/item/effect-item/base/gifts-settings-brackets-section-item-effect-item-base/gifts-settings-brackets-section-item-effect-item-base.component';
import { TutorialsMiniBoxBaseComponent } from './components/tutorials/mini-box/base/tutorials-mini-box-base/tutorials-mini-box-base.component';
import { EffectsBoardAddButtonBaseComponent } from './components/effects/board/add-button/base/effects-board-add-button-base/effects-board-add-button-base.component';



@NgModule({
    declarations: [
        HomePageBaseComponent,
        BindingBoxBaseComponent,
        BindingBoxYounowBaseComponent,
        BindingBoxYounowInputBaseComponent,
        BindingBoxYounowInfoBaseComponent,
        EventsListBaseComponent,
        EventsListChatItemBaseComponent,
        EventsListGiftItemBaseComponent,
        EventsListStatusItemBaseComponent,
        MetricsYounowBaseComponent,
        MetricsYounowLikesBaseComponent,
        MetricsYounowViewersBaseComponent,
        MetricsYounowFansBaseComponent,
        EventsSettingsBaseComponent,
        EventsSettingsVoicesSectionBaseComponent,
        EventsSettingsSpamSectionBaseComponent,
        EventsSettingsAudienceSectionBaseComponent,
        EventsBoxBaseComponent,
        EventsSettingsFiltersSectionBaseComponent,
        OverlaysBoxBaseComponent,
        OverlaysYounowLikesBaseComponent,
        OverlaysYounowLikesSettingsBoxBaseComponent,
        OverlaysYounowTopFansBaseComponent,
        OverlaysEffectsBaseComponent,
        OverlaysEffectsItemBaseComponent,
        AccountsItemSettingsBoxBaseComponent,
        AccountsItemSettingsBoxVoiceSectionBaseComponent,
        AccountsItemSettingsBoxChatSectionBaseComponent,
        AccountsItemSettingsBoxEffectsSectionBaseComponent,
        EffectsBoardBaseComponent,
        EffectsBoardItemBaseComponent,
        EffectsSelectBoxBaseComponent,
        EffectsSelectBoxItemBaseComponent,
        EffectsEditBoxBaseComponent,
        EffectsEditBoxFileSectionBaseComponent,
        EffectsEditBoxInfoSectionBaseComponent,
        EffectsEditBoxSettingsSectionBaseComponent,
        EffectsEditBoxTriggersSectionBaseComponent,
        EffectsEditBoxInfoSectionFileInputBaseComponent,
        EffectsEditBoxSettingsSectionDurationInputBaseComponent,
        EffectsEditBoxSettingsSectionPositionInputBaseComponent,
        EffectsEditBoxSettingsSectionVolumeInputBaseComponent,
        EffectsEditBoxTriggersSectionItemBaseComponent,
        EffectsAddBoxBaseComponent,
        EffectsAddBoxFileInputBaseComponent,
        OverlaysEffectsItemVideoBaseComponent,
        OverlaysEffectsItemGifBaseComponent,
        OverlaysEffectsItemAudioBaseComponent,
        GiftsBoxBaseComponent,
        GiftsSettingsBaseComponent,
        GiftsSettingsBracketsSectionBaseComponent,
        GiftsSettingsBracketsSectionItemBaseComponent,
        GiftsSettingsBracketsSectionItemEffectItemBaseComponent,
        TutorialsMiniBoxBaseComponent,
        EffectsBoardAddButtonBaseComponent,

    ],
    imports: [
        BrowserModule,
        FormsModule,
        CommonElementsModule
    ],
    exports: [
        HomePageBaseComponent,


    ]
})
export class BridgeComponentsModule {}