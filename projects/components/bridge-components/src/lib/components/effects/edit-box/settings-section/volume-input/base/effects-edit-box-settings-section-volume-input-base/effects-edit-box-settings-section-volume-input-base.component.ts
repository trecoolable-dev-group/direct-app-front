import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'bridge-elements-effects-edit-box-settings-section-volume-input-base',
    templateUrl: './effects-edit-box-settings-section-volume-input-base.component.html',
    styleUrls: ['./effects-edit-box-settings-section-volume-input-base.component.css']
})
export class EffectsEditBoxSettingsSectionVolumeInputBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}



    /// EFFECT ///
    //============================

    //effect item
    @Input('EffectItem') EffectItem = null;

    //============================
    /// END EFFECT ///




}