import { TestBed } from '@angular/core/testing';

import { BridgeControlService } from './bridge-control.service';

describe('BridgeControlService', () => {
  let service: BridgeControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BridgeControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
