import { Component, OnInit } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-overlays-effects-base',
    templateUrl: './overlays-effects-base.component.html',
    styleUrls: ['./overlays-effects-base.component.css']
})
export class OverlaysEffectsBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {}


    //controls
    public Playing = this.BridgeControl.Effects.Playing;



}