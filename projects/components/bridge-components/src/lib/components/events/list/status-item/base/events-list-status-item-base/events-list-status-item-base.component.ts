import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bridge-elements-events-list-status-item-base',
  templateUrl: './events-list-status-item-base.component.html',
  styleUrls: ['./events-list-status-item-base.component.css']
})
export class EventsListStatusItemBaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
