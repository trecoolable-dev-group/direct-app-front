import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardButtonBaseComponent } from './standard-button-base.component';

describe('StandardButtonBaseComponent', () => {
  let component: StandardButtonBaseComponent;
  let fixture: ComponentFixture<StandardButtonBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardButtonBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardButtonBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
