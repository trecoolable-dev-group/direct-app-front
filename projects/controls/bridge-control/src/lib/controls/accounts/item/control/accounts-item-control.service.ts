import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';
import { AccountsItemVoiceControlService } from '../voice/control/accounts-item-voice-control.service';
import { AccountsItemThemeControlService } from '../theme/control/accounts-item-theme-control.service';
import { AccountsItemActivityControlService } from '../activity/control/accounts-item-activity-control.service';

export class AccountsItemControlService {

    constructor(private AccountsDb: ItemsDbService, public identifier: string) {}








    /// DB ///
    //===========================

    //collections
    private ACCOUNTS_COL = 'accounts_col';
    private ACCOUNTS_SETTINGS_COL = 'accounts_settings_col';


    //===========================
    /// END DB ///










    /// INFO ///
    //==========================

    //platform
    public platform = '';

    //platform_id
    public platform_id = '';

    //info
    public info = {
        name: this.platform + ' user',
        profile_pic: '',
        platform_info: {}
    };

    //has info flag
    public has_info = false;


    /*
     * @Params:  none
     * @Does:    fetches platform info
     */
    public async fetchInfo() {
        let model = this;
        //check if has info
        if (model.has_info) return reply(true, 'has info', { info: model.info, has_info: model.has_info, voice: model.Voice.settings });
        //set fetch op
        let fetchOp = null;
        //check if has stored info
        let storedOp: any = await model.AccountsDb.findItemsByKey('identifier', model.identifier, model.ACCOUNTS_COL, 1);
        //check if got stored
        if (storedOp.data.items.length) {
            //set stored info
            let info = storedOp.data.items[0];
            //set info
            model.info = info;
            //set has info
            model.has_info = true;
        }
        //fetch voice settings
        let voiceOp: any = await model.Voice.fetchSettings();
        //return fetch
        return reply(true, 'no info', { info: model.info, has_info: model.has_info, voice: model.Voice.settings });
    }
    //end fetchInfo()



    /*
     * @Params:  none
     * @Does:    saves account to accounts db
     */
    public async saveInfo() {
        let model = this;
        //set item
        let item = {
            identifier: model.identifier,
            platform: model.platform,
            ...model.info
        };
        //save to accounts
        let saveOp = await model.AccountsDb.upsertItem(item, model.ACCOUNTS_COL);
        //return saved
        return reply(true, 'saved info');
    }
    //end saveInfo()


    //==========================
    /// END INFO ///










    /// ACTIVITY ///
    //============================

    public Activity = new AccountsItemActivityControlService(this.identifier);

    //============================
    /// END ACTIVITY ///








    /// THEME ///
    //============================

    public Theme = new AccountsItemThemeControlService(this.AccountsDb, this.identifier);

    //============================
    /// END THEME ///








    /// VOICE ///
    //============================

    public Voice = new AccountsItemVoiceControlService(this.AccountsDb, this.identifier);

    //============================
    /// END VOICE ///




}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}