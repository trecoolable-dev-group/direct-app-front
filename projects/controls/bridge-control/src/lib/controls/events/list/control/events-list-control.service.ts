export class EventsListControlService {

    constructor() {}



    //events 
    public events = [];


    /*
     * @Params:  type, platform, platform, identifier, content
     * @Does:    adds event to events
     */
    public addEvent(type, platform, platform_id, identifier, content) {
        let model = this;
        //create event item
        let event = {
            type: type,
            platform: platform,
            platform_id: platform_id,
            identifier: identifier,
            content: content
        };
        //add event
        model.events.push(event);
    }
    //end addEvent()



}