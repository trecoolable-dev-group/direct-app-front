import { TestBed } from '@angular/core/testing';

import { GuestsItemControlService } from './guests-item-control.service';

describe('GuestsItemControlService', () => {
  let service: GuestsItemControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GuestsItemControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
