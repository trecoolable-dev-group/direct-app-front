import { TestBed } from '@angular/core/testing';

import { EventsYounowControlService } from './events-younow-control.service';

describe('EventsYounowControlService', () => {
  let service: EventsYounowControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventsYounowControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
