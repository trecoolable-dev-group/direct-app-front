import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsItemSettingsBoxEffectsSectionBaseComponent } from './accounts-item-settings-box-effects-section-base.component';

describe('AccountsItemSettingsBoxEffectsSectionBaseComponent', () => {
  let component: AccountsItemSettingsBoxEffectsSectionBaseComponent;
  let fixture: ComponentFixture<AccountsItemSettingsBoxEffectsSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsItemSettingsBoxEffectsSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsItemSettingsBoxEffectsSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
