import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsEditBoxInfoSectionFileInputBaseComponent } from './effects-edit-box-info-section-file-input-base.component';

describe('EffectsEditBoxInfoSectionFileInputBaseComponent', () => {
  let component: EffectsEditBoxInfoSectionFileInputBaseComponent;
  let fixture: ComponentFixture<EffectsEditBoxInfoSectionFileInputBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsEditBoxInfoSectionFileInputBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsEditBoxInfoSectionFileInputBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
