import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';
import { ItemMessageService } from '@controls/bridge-control/src/lib/tools/item-message/item-message.service';


export class GiftsBracketsItemControlService {

    constructor(private GiftsDb: ItemsDbService, public bracket_id: string) {}







    /// DB ///
    //===========================

    //collections
    private GIFTS_BRACKETS_COL = 'gifts_brackets_col';

    //===========================
    /// END DB ///









    /// MESSAGE ///
    //=================================

    //message
    public message = new ItemMessageService();

    //messages
    public input_messages = {};

    //=================================
    /// END MESSAGE ///









    /// INFO ///
    //===========================

    //platform
    public platform = '';

    //info
    public info = {
        unit: '',
        min: 5,
        max: 10,
        effects: []
    }

    //info inputs
    public info_inputs = { ...this.info };

    //has info flag
    public has_info = false;


    /*
     * @Params:  none
     * @Does:    checks if has info
     *           fetches info from effects data
     *           fetches settings from effects settings
     * @Return:  effect
     */
    public async fetchInfo() {
        let model = this;
        //check if has info
        if (model.has_info) return reply(true, 'has info', { info: model.info });
        //fetch info
        let fetchOp: any = await model.GiftsDb.findItemsByKey('bracket_id', this.bracket_id, model.GIFTS_BRACKETS_COL);
        //check if got info
        if (!fetchOp.data.items.length) return reply(false, 'no matching bracket', { info: null });
        //has bracket, set info
        let info = fetchOp.data.items[0];
        //set info
        model.info = info;
        //set inputs
        model.info_inputs = { ...info };
        //set has info
        model.has_info = true;
        //return with info
        return reply(true, 'got info', { info: model.info });
    }
    //end fetchInfo()



    /*
     * @Params:  none
     * @Does:    saves effect info
     */
    public async saveInfo() {
        let model = this;
        //fetch changes
        let keys = model.fetchChanges();
        //set info to inputs
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //set changes
            model.info[key] = model.info_inputs[key];
        }
        //set info item
        let item = {
            bracket_id: model.bracket_id,
            platform: model.platform,
            ...model.info
        };
        //set has info
        model.has_info = true;
        //save to info
        model.GiftsDb.upsertItem(item, model.GIFTS_BRACKETS_COL);
        //set message
        model.message.setMessage('saved info', '', true, 5000);
        //return saved
        return reply(true, 'saved info');
    }
    //end saveInfo()



    /*
     * @Params:  key, value
     * @Does:    validates input
     *           sets input key
     */
    public async handleChange(key, value) {
        let model = this;
        //set input key
        model.info_inputs[key] = value;
    }
    //end handleChange()



    /*
     * @Params:  none
     * @Does:    checks if has changes
     * @Return:  changes
     */
    public fetchChanges() {
        let model = this;
        //set changes array
        let changes = [];
        //change keys
        let keys = [
            'unit',
            'min',
            'max',
            'effects'
        ];
        //check if is not saved
        if (!model.has_info) return keys;
        //iterate through keys
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //check if is different
            if (JSON.stringify(model.info[key]) != JSON.stringify(model.info_inputs[key])) changes.push(key);
        }
        //return changes
        return changes;
    }
    //end fetchChanges()



    /*
     * @Params:  none
     * @Does:    resets inputs to original
     */
    public resetChanges() {
        let model = this;
        //set info to previous
        model.info_inputs = { ...model.info };
        //save info
        model.saveInfo();
    }
    //end resetChanges()

    //===========================
    /// END INFO ///














    /// EFFECTS ///
    //=========================

    /*
     * @Params:  effect_id
     * @Does:    checks if has effect
     *           if no effect, adds effect and saves
     */
    public addEffect(effect_id) {
        let model = this;
        //check if has many effects
        if (model.info_inputs.effects.length >= 3) return;
        //get index of effect
        let idx = model.info_inputs.effects.indexOf(effect_id);
        //check if has effect
        if (idx != -1) return;
        //does not have effect, add effect
        model.info_inputs.effects.push(effect_id);
        //save info
        model.saveInfo();
    }
    //end addEffect()



    /*
     * @Params:  effect_id
     * @Does:    checks if has effect
     *           if has effect, removes effect and saves
     */
    public removeEffect(effect_id) {
        let model = this;
        //get index of effect
        let idx = model.info_inputs.effects.indexOf(effect_id);
        //check if has effect
        if (idx == -1) return;
        //does not have effect, remove effect
        model.info_inputs.effects.splice(idx, 1);
        //save info
        model.saveInfo();
    }
    //end removeEffect()


    //=========================
    /// END EFFECTS ///




}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}