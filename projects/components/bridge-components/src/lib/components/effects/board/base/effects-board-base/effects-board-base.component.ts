import { Component, OnInit } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-effects-board-base',
    templateUrl: './effects-board-base.component.html',
    styleUrls: ['./effects-board-base.component.css'],
    host: {
        class: 'height-100 width-100 flex-column'
    }
})
export class EffectsBoardBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set initial effects
        this.setEffects();
        //check has hint
        this.checkCustomHint();
    }


    //controls
    public Effects = this.BridgeControl.Effects;
    public Tutorials = this.BridgeControl.Tutorials;








    /// EFFECTS ///
    //=============================

    //list
    public effects_list = [];


    /*
     * @Params:  none
     * @Does:    sets effects
     */
    public async setEffects() {
        let model = this;
        //fetch effects
        let fetchOp: any = await model.Effects.Local.fetchEffects();
        //check if got effects
        if (fetchOp.success) {
            //set effects
            model.effects_list = model.Effects.Local.effects;
        }
    }
    //end setEffects()

    //=============================
    /// END EFFECTS ///

















    /// ADD DISPLAY ///
    //=================================

    //overlay add_opened
    public add_opened = -1;


    /*
     * @Params:  none
     * @Does:    toggles overlay add_opened
     */
    public toggleAddOpened() {
        let model = this;
        //check if is add_opened
        if (model.add_opened != 1) {
            //set add_opened
            model.add_opened = 1;
        } else {
            //set closed
            model.add_opened = 0;
            //await to remove
            setTimeout(() => {
                model.add_opened = -1;
            }, 200);
        }
    }
    //end toggleAddOpened()



    /*
     * @Params:  effect_id
     * @Does:    adds effect item to list
     */
    public handleAdded(effect_id) {
        let model = this;
        //add to effects
        model.effects_list.push(effect_id);
        //toggle opened
        model.toggleAddOpened();
    }
    //end handleAdded()

    //=================================
    /// END ADD DISPLAY ///
















    /// HINT DISPLAY ///
    //=================================

    //display id
    public display_id = Math.random().toString(36).slice(2).toUpperCase();

    //hint title
    public hint_title = 'Custom Effects';

    //hint content
    public hint_content = 'You can add your own custom effects with audio, video, or gif files.';

    //hint image
    public hint_image = 'https://i.imgur.com/43yDvYm.gif';

    //overlay add_hint_opened
    public add_hint_opened = -1;


    /*
     * @Params:  none
     * @Does:    checks if binding tutorial has been completed
     */
    public async checkCustomHint() {
        let model = this;
        //check if binding completed
        let checkOp: any = await model.Tutorials.checkCompleted('add_effects');
        //check completed
        if (checkOp.success && !checkOp.data.completed) {
            //await to add
            await wait(2000);
            //set display item
            model.Tutorials.addDisplayItem('add_effects', model.display_id);
            //toggle opened
            model.toggleAddHintOpened();
        }
    }
    //end checkCustomHint()



    /*
     * @Params:  force
     * @Does:    toggles overlay add_opened
     */
    public toggleAddHintOpened(force = false) {
        let model = this;
        //check if is add_hint_opened
        if (model.add_hint_opened != 1) {
            //set display id
            if (force) model.Tutorials.display_id = model.display_id;
            //set add_opened
            model.add_hint_opened = 1;
        } else {
            //set closed
            model.add_hint_opened = 0;
            //await to remove
            setTimeout(() => {
                model.add_hint_opened = -1;
                //add to completed
                model.Tutorials.addCompleted('add_effects');
                //remove from display items
                model.Tutorials.removeDisplayItem(model.display_id);
            }, 200);
        }
    }
    //end toggleAddHintOpened()

    //=================================
    /// END HINT DISPLAY ///









}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}