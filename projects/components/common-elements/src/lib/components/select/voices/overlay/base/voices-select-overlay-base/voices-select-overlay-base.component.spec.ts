import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoicesSelectOverlayBaseComponent } from './voices-select-overlay-base.component';

describe('VoicesSelectOverlayBaseComponent', () => {
  let component: VoicesSelectOverlayBaseComponent;
  let fixture: ComponentFixture<VoicesSelectOverlayBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoicesSelectOverlayBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoicesSelectOverlayBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
