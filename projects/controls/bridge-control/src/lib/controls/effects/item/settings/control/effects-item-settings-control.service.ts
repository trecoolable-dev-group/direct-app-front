import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';
import { ItemMessageService } from '@controls/bridge-control/src/lib/tools/item-message/item-message.service';


export class EffectsItemSettingsControlService {

    constructor(private EffectsDb: ItemsDbService, public effect_id: string) {}






    /// DB ///
    //===========================

    //collections
    private LOCAL_EFFECTS_COL = 'local_effects_col';
    private EFFECTS_DATA_COL = 'effects_data_col';
    private EFFECTS_SETTINGS_COL = 'effects_settings_col';
    private EFFECTS_TRIGGERS_COL = 'effects_triggers_col';

    //===========================
    /// END DB ///









    /// MESSAGE ///
    //=================================

    //message
    public message = new ItemMessageService();

    //messages
    public input_messages = {};

    //=================================
    /// END MESSAGE ///











    /// SETTINGS ///
    //==============================

    //settings
    public settings = {
        volume: 1,
        duration_start: 0,
        duration_end: 5,
        scale_x: 100,
        scale_y: 100,
        offset_x: 0,
        offset_y: 0,
    };

    //settings inputs
    public settings_inputs = { ...this.settings };

    //has info flag
    public has_info = false;


    /*
     * @Params:  none
     * @Does:    checks if has settings
     *           fetches stored settings
     * @return:  settings
     */
    public async fetchSettings() {
        let model = this;
        //check has info
        if (model.has_info) return reply(true, 'has settings', { settings: model.settings });
        //fetch info
        let fetchOp: any = await model.EffectsDb.findItemsByKey('effect_id', model.effect_id, model.EFFECTS_SETTINGS_COL);
        //check if got settings
        if (!fetchOp.data.items.length) return reply(true, 'no settings', model.settings);
        //has settings, set settings
        let settings = fetchOp.data.items[0];
        //set settings keys
        let keys = Object.keys(model.settings);
        //iterate through keys
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //check if has key
            if (settings[key] != undefined) model.settings[key] = settings[key];
        }
        //set inputs
        model.settings_inputs = { ...model.settings };
        //return with settings
        return reply(true, 'got settings', { settings: model.settings });
    }
    //end fetchSettings()



    /*
     * @Params:  none
     * @Does:    saves settings
     */
    public saveSettings() {
        let model = this;
        //unset message
        model.message.clearMessage();
        //fetch changes
        let keys = model.fetchChanges();
        //set info to inputs
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //set changes
            model.settings[key] = model.settings_inputs[key];
        }
        //set settings item
        let item = {
            effect_id: model.effect_id,
            ...model.settings
        };
        //save to settings
        model.EffectsDb.upsertItem(item, model.EFFECTS_SETTINGS_COL);
        //set saved message
        model.message.setMessage('saved settings', '', true, 5000);
        //return saved
        return reply(true, 'saved settings');
    }
    //end saveSettings()



    /*
     * @Params:  key, value
     * @Does:    validates input
     *           sets input key
     *           sets has change
     */
    public handleChange(key, value) {
        let model = this;
        //set input key
        model.settings_inputs[key] = value;
    }
    //end handleChange()



    /*
     * @Params:  none
     * @Does:    checks if has changes
     * @Return:  changes
     */
    public fetchChanges() {
        let model = this;
        //set changes array
        let changes = [];
        //change keys
        let keys = [
            'volume',
            'duration_start',
            'duration_end',
            'scale_x',
            'scale_y',
            'offset_x',
            'offset_y',
        ];
        //iterate through keys
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //check if is different
            if (JSON.stringify(model.settings[key]) != JSON.stringify(model.settings_inputs[key])) changes.push(key);
        }
        //return changes
        return changes;
    }
    //end fetchChanges()



    /*
     * @Params:  none
     * @Does:    resets inputs to original
     */
    public resetChanges() {
        let model = this;
        //set settings to previous
        model.settings_inputs = { ...model.settings };
    }
    //end resetChanges()

    //==============================
    /// END SETTINGS ///






}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}