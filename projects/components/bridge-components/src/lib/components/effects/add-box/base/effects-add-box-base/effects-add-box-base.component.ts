import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { BridgeControlService } from '@controls/bridge-control/src/public-api';

@Component({
    selector: 'bridge-elements-effects-add-box-base',
    templateUrl: './effects-add-box-base.component.html',
    styleUrls: ['./effects-add-box-base.component.css']
})
export class EffectsAddBoxBaseComponent implements OnInit {

    constructor(public BridgeControl: BridgeControlService) {}

    ngOnInit(): void {
        //set effect
        this.setEffect();
    }


    //controls
    public Effects = this.BridgeControl.Effects;








    /// DISPLAY ///
    //================================

    //opened flag
    @Input('Opened') Opened = -1;

    //close event
    @Output('onClose') onClose: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  none
     * @Does:    sets opened to false
     */
    public close() {
        let model = this;
        //set opened to close
        model.Opened = 0;
        //emit close event
        model.onClose.emit();
    }
    //end close()

    //================================
    /// END DISPLAY ///










    /// EFFECT ///
    //===============================

    //effect item
    public EffectItem = null;


    /*
     * @Params:  none
     * @Does:    sets effect
     */
    public setEffect() {
        let model = this;
        //set effect
        model.EffectItem = model.Effects.EffectItem('effect_' + Date.now());
    }
    //end setEffect()

    //===============================
    /// END EFFECT ///













    /// SAVING ///
    //==============================

    //added event
    @Output('onAdd') onAdd: EventEmitter < any > = new EventEmitter < any > ()

    //loading lottie
    @ViewChild('LoadingLottie') LoadingLottie;

    //save status
    public status = '';


    /*
     * @Params:  none
     * @Does:    saves inputs
     */
    public async saveInputs() {
        let model = this;
        //check if is busy
        if (model.status == 'busy') return;
        //set status
        model.status = 'busy';
        //set loading lottie
        model.LoadingLottie.playLoading();
        //save effect
        let submitOp = await model.EffectItem.saveInfo();
        //check if success
        if (!submitOp.success) {
            //set error status
            model.status = 'error';
            //did not add tier, play error
            model.LoadingLottie.playError();
        } else {
            //set success status
            model.status = 'success';
            //added tier, play success
            model.LoadingLottie.playSuccess();
            //add to local
            await model.Effects.Local.addEffect(model.EffectItem.effect_id, model.EffectItem.info.name, model.EffectItem.info.type);
            //emit added
            model.onAdd.emit(model.EffectItem.effect_id);
        }
        //wait to unset status
        await wait(2000);
        //unset submit status
        model.status = '';
    }
    //end saveInputs()

    //==============================
    /// END SAVING ///





}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};

//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}