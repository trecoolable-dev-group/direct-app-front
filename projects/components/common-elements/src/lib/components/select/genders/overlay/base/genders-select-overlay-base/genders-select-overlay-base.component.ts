import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as GendersList from 'projects/reference/genders.json'

@Component({
    selector: 'common-elements-genders-select-overlay-base',
    templateUrl: './genders-select-overlay-base.component.html',
    styleUrls: ['./genders-select-overlay-base.component.css']
})
export class GendersSelectOverlayBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}






    /// DISPLAY ///
    //================================

    //opened flag
    @Input('Opened') Opened = -1;

    //close event
    @Output('onClose') onClose: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  none
     * @Does:    sets opened to false
     */
    public close() {
        let model = this;
        //set opened to close
        model.Opened = 0;
        //emit close event
        model.onClose.emit();
    }
    //end close()

    //================================
    /// END DISPLAY ///













    /// GENDERS ///
    //============================

    //genders
    public genders = GendersList.genders;

    //selected item
    public selected = { label: 'Select Gender', value: '' };

    //value input
    @Input('Value') Value = '';

    //select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  gender
     * @Does:    sets value
     *           triggers on select for country code
     *           closes interface
     */
    public handleSelect(gender) {
        let model = this;
        //set value
        model.selected = gender;
        //emit gender
        model.onSelect.emit(gender);
        //close interface
        model.close();
    }
    //end handleSelect()


    //============================
    /// END GENDERS ///












}