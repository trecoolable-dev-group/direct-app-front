import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'bridge-elements-home-page-base',
    templateUrl: './home-page-base.component.html',
    styleUrls: ['./home-page-base.component.css'],
    host: {
        class: 'height-100 width-100 flex-row contained'
    }
})
export class HomePageBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}





    /// BOX ///
    //=====================

    //box section
    public box_section = 'events';

    //box sections
    public box_sections = [
        'events',
        'gifts'
    ];


    /*
     * @Params:  section
     * @Does:    changes box section
     */
    public changeBoxSection(section) {
        let model = this;
        //change section
        model.box_section = section;
    }
    //end changeBoxSection()

    //=====================
    /// END BOX ///



}