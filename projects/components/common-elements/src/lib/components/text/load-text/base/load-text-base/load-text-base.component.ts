import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'common-elements-load-text-base',
    templateUrl: './load-text-base.component.html',
    styleUrls: ['./load-text-base.component.css']
})
export class LoadTextBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}






    /// DISPLAY ///
    //=============================

    //random delay
    public delay = Math.floor(Math.random() * 30) + 1;

    //=============================
    /// END DISPLAY ///








    /// INPUTS ///
    //===============================

    //placeholder text
    @Input('Placeholder') Placeholder = 'placeholder';

    //text style
    @Input('TextClass') TextClass = '';

    //text content
    @Input('Text') Text = '';

    //===============================
    /// END INPUTS ///




}