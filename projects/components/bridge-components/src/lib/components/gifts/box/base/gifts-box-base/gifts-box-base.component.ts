import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bridge-elements-gifts-box-base',
  templateUrl: './gifts-box-base.component.html',
  styleUrls: ['./gifts-box-base.component.css']
})
export class GiftsBoxBaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
