import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsEditBoxFileSectionBaseComponent } from './effects-edit-box-file-section-base.component';

describe('EffectsEditBoxFileSectionBaseComponent', () => {
  let component: EffectsEditBoxFileSectionBaseComponent;
  let fixture: ComponentFixture<EffectsEditBoxFileSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsEditBoxFileSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsEditBoxFileSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
