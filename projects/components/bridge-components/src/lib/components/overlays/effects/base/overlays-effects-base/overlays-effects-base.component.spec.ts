import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaysEffectsBaseComponent } from './overlays-effects-base.component';

describe('OverlaysEffectsBaseComponent', () => {
  let component: OverlaysEffectsBaseComponent;
  let fixture: ComponentFixture<OverlaysEffectsBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaysEffectsBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaysEffectsBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
