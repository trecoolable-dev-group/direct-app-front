import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as CountriesList from 'projects/reference/countries.json'

@Component({
    selector: 'common-elements-countries-select-overlay-base',
    templateUrl: './countries-select-overlay-base.component.html',
    styleUrls: ['./countries-select-overlay-base.component.css']
})
export class CountriesSelectOverlayBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}







    /// DISPLAY ///
    //================================

    //opened flag
    @Input('Opened') Opened = -1;

    //close event
    @Output('onClose') onClose: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  none
     * @Does:    sets opened to false
     */
    public close() {
        let model = this;
        //set opened to close
        model.Opened = 0;
        //emit close event
        model.onClose.emit();
    }
    //end close()

    //================================
    /// END DISPLAY ///












    /// COUNTRIES ///
    //============================

    //countries
    public countries = CountriesList.countries.slice(0, 8);

    //value input
    @Input('Value') Value = '';

    //select event
    @Output('onSelect') onSelect: EventEmitter < any > = new EventEmitter < any > ()


    /*
     * @Params:  code
     * @Does:    sets value
     *           triggers on select for country code
     *           closes interface
     */
    public handleSelect(value) {
        let model = this;
        //set value
        model.Value = value;
        //emit country code
        model.onSelect.emit(value);
        //close interface
        model.close();
    }
    //end handleSelect()



    /*
     * @Params:  none
     * @Does:    shows all countries
     */
    public showAll() {
        let model = this;
        //set countries
        model.countries = CountriesList.countries;
    }
    //end showAll()


    //============================
    /// END COUNTRIES ///




}