import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricsYounowFansBaseComponent } from './metrics-younow-fans-base.component';

describe('MetricsYounowFansBaseComponent', () => {
  let component: MetricsYounowFansBaseComponent;
  let fixture: ComponentFixture<MetricsYounowFansBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricsYounowFansBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricsYounowFansBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
