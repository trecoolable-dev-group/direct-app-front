import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftsSettingsBracketsSectionBaseComponent } from './gifts-settings-brackets-section-base.component';

describe('GiftsSettingsBracketsSectionBaseComponent', () => {
  let component: GiftsSettingsBracketsSectionBaseComponent;
  let fixture: ComponentFixture<GiftsSettingsBracketsSectionBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftsSettingsBracketsSectionBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftsSettingsBracketsSectionBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
