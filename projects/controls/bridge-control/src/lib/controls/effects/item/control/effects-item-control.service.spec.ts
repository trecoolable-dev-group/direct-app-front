import { TestBed } from '@angular/core/testing';

import { EffectsItemControlService } from './effects-item-control.service';

describe('EffectsItemControlService', () => {
  let service: EffectsItemControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EffectsItemControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
