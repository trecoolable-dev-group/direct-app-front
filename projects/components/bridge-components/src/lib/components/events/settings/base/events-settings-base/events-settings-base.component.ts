import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bridge-elements-events-settings-base',
  templateUrl: './events-settings-base.component.html',
  styleUrls: ['./events-settings-base.component.css']
})
export class EventsSettingsBaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
