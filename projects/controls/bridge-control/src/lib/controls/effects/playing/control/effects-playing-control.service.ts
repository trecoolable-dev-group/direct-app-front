export class EffectsPlayingControlService {

    constructor() {}




    //playing list
    public list = [];


    /*
     * @Params:  effect_id
     * @Does:    checks if has existing
     *           if no existing, adds item to playing
     *           if has existing, removes current and adds effect again
     *           checks if has over effects limit
     *           if over, removes latest effect
     */
    public async addPlaying(effect_id) {
        let model = this;
        //check if has existing
        let idx = model.list.indexOf(effect_id);
        //check if exists
        if (idx != -1) {
            //splice item
            model.list.splice(idx, 1);
        }
        await wait(10);
        //add to playing
        model.list.push(effect_id);
    }
    //end addPlaying()



    /*
     * @Params: effect_id
     * @Does:   triggers stop on item
     *          removes item from playing
     */
    public removePlaying(effect_id) {
        let model = this;
        //check if has existing
        let idx = model.list.indexOf(effect_id);
        //check if exists
        if (idx != -1) {
            //splice item
            model.list.splice(idx, 1);
        }
    }
    //end removePlaying()




}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};

//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}