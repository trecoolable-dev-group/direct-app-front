import { Component, OnInit, ViewChild, Input } from '@angular/core';

@Component({
    selector: 'common-elements-standard-button-base',
    templateUrl: './standard-button-base.component.html',
    styleUrls: ['./standard-button-base.component.css']
})
export class StandardButtonBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}







    /// DISPLAY ///
    //=================================

    @ViewChild('SplashElement') SplashElement: any;

    //=================================
    /// END DISPLAY ///












    /// INPUTS ///
    //============================

    //button class
    @Input('ButtonClass') ButtonClass = '';

    //icon class
    @Input('IconClass') IconClass = '';

    //icon type
    @Input('IconType') IconType = '';

    //icon name
    @Input('IconName') IconName = '';

    //icon fill
    @Input('IconFill') IconFill = '#fff';

    //icon source
    @Input('IconSrc') IconSrc = '';

    //text class
    @Input('TextClass') TextClass = '';

    //text style
    @Input('TextStyle') TextStyle = '';

    //text content
    @Input('Text') Text = '';

    //splash color
    @Input('SplashColor') SplashColor = '#fff';

    //splash size
    @Input('SplashSize') SplashSize = 'large';

    //============================
    /// END INPUTS ///










    /// CLICK EVENT ///
    //=======================================

    /*
     * @Params:  none
     * @Does:    triggers splash
     *           starts matchmaking skip
     */
    public handleClick(event) {
        let model = this;
        //trigger splash
        if (model.SplashElement && model.SplashElement.splash) model.SplashElement.splash(event);


    }
    //end handleClick()


    //=======================================
    /// END CLICK EVENT ///












}