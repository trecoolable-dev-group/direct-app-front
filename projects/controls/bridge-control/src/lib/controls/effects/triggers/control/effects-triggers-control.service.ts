import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';


export class EffectsTriggersControlService {

    constructor(private EffectsDb: ItemsDbService) {}




    /// DB ///
    //===========================

    //collections
    private LOCAL_EFFECTS_COL = 'local_effects_col';
    private EFFECTS_DATA_COL = 'effects_data_col';
    private EFFECTS_SETTINGS_COL = 'effects_settings_col';
    private EFFECTS_TRIGGERS_COL = 'effects_triggers_col';

    //===========================
    /// END DB ///








    /// FETCHING ///
    //============================

    /*
     * @Params:  phrase
     * @Does:    fetches effect ids by trigger phrase
     */
    public async fetchEffectsByPhrase(phrase) {
        let model = this;
        //fetch effects
        let fetchOp: any = await model.EffectsDb.findItemsByKey('phrase', phrase, model.EFFECTS_TRIGGERS_COL, 10);
        //check if got
        if (!fetchOp.success) return reply(false, 'could not fetch triggers');
        //check return effects
        return reply(true, 'got effects', { effects: fetchOp.data.items.map(item => { return item.effect_id }) });
    }
    //end fetchEffectsByPhrase()

    //============================
    /// END FETCHING ///






}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}