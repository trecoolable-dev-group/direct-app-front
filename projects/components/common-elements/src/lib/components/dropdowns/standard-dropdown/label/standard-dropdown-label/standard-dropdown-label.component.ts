import { Component, OnInit, ViewChild, Input } from '@angular/core';

@Component({
    selector: 'common-elements-standard-dropdown-label',
    templateUrl: './standard-dropdown-label.component.html',
    styleUrls: ['./standard-dropdown-label.component.css']
})
export class StandardDropdownLabelComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}








    /// DISPLAY ///
    //=================================

    @ViewChild('SplashElement') SplashElement: any;

    //=================================
    /// END DISPLAY ///












    /// INPUTS ///
    //============================

    //label class
    @Input('LabelClass') LabelClass = 'light-round fill-light-shade light-stroke';

    //icon class
    @Input('IconClass') IconClass = 'std-square contained margin-side-med';

    //icon type
    @Input('IconType') IconType = '';

    //icon name
    @Input('IconName') IconName = '';

    //icon fill
    @Input('IconFill') IconFill = '#fff';

    //icon source
    @Input('IconSrc') IconSrc = '';

    //value class
    @Input('ValueClass') ValueClass = 'margin-side-med margin-vert';

    //drop class
    @Input('DropClass') DropClass = 'small-square icon-holder right-carat-icon fill-true-white margin-side-med margin-vert';

    //value content
    @Input('Value') Value = '';

    //splash color
    @Input('SplashColor') SplashColor = 'rgba(225,220,235,0.05)';

    //splash size
    @Input('SplashSize') SplashSize = 'large';

    //============================
    /// END INPUTS ///










    /// CLICK EVENT ///
    //=======================================

    /*
     * @Params:  none
     * @Does:    triggers splash
     *           starts matchmaking skip
     */
    public handleClick(event) {
        let model = this;
        //trigger splash
        if (model.SplashElement && model.SplashElement.splash) model.SplashElement.splash(event);


    }
    //end handleClick()


    //=======================================
    /// END CLICK EVENT ///






}