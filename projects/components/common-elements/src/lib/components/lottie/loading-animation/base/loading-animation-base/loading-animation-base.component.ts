import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'common-elements-loading-animation-base',
    templateUrl: './loading-animation-base.component.html',
    styleUrls: ['./loading-animation-base.component.css']
})
export class LoadingAnimationBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}




    //loading lottie
    public loading_lottie = null;


    /*
     * @Params:  lottie
     * @Does:    sets loading lottie
     */
    public setLottie(lottie) {
        //set lottie
        this.loading_lottie = lottie;
    }
    //end setLottie()



    /*
     * @Params:  none
     * @Does:    plays loading animation
     */
    public playLoading() {
        let model = this;
        //play loading segments
        model.loading_lottie.playSegments([0, 70], true);
        //set loop
        model.loading_lottie.loop = true;
    }
    //end playLoading()



    /*
     * @Parmas:  none
     * @Does:    plays error segments
     */
    public playError() {
        let model = this;
        //play loading segments
        model.loading_lottie.playSegments([129, 180], true);
        //set loop
        model.loading_lottie.loop = false;
    }
    //end playError()



    /*
     * @Parmas:  none
     * @Does:    plays success segments
     */
    public playSuccess() {
        let model = this;
        //play loading segments
        model.loading_lottie.playSegments([71, 128], true);
        //set loop
        model.loading_lottie.loop = false;
    }
    //end playSuccess()


}