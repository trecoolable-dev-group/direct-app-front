export class AccountsItemActivityControlService {

    constructor(public identifier: string) {}






    /// ACTIVITY ///
    //============================

    //activity score
    public score = 0;

    //activity timeout
    private activityTimeout = null;


    /*
     * @Params:  none
     * @Does:    unsets activity timeout
     *           sets activity timeout
     */
    public incrementActivity() {
        let model = this;
        //check if has activity timeout
        if (model.activityTimeout != null) {
            //add activity score
            if (model.score < 1000) model.score++;
            //unset timeout
            window.clearTimeout(model.activityTimeout);
            //set to null
            model.activityTimeout = null;
        }
        //set timeout
        model.activityTimeout = setTimeout(() => {
            //check if has activity timeout
            if (model.activityTimeout != null) {
                //add activity score
                model.score = 0;
                //unset timeout
                window.clearTimeout(model.activityTimeout);
                //set to null
                model.activityTimeout = null;
            }
        }, (2 * 60000));
    }
    //end incrementActivity()


    //============================
    /// END ACTIVITY ///


}