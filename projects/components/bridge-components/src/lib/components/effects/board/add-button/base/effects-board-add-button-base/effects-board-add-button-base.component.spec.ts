import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsBoardAddButtonBaseComponent } from './effects-board-add-button-base.component';

describe('EffectsBoardAddButtonBaseComponent', () => {
  let component: EffectsBoardAddButtonBaseComponent;
  let fixture: ComponentFixture<EffectsBoardAddButtonBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectsBoardAddButtonBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectsBoardAddButtonBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
