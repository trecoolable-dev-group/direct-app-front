import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePageBaseComponent } from './home-page-base.component';

describe('HomePageBaseComponent', () => {
  let component: HomePageBaseComponent;
  let fixture: ComponentFixture<HomePageBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePageBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
