import { TestBed } from '@angular/core/testing';

import { MetricsControlService } from './metrics-control.service';

describe('MetricsControlService', () => {
  let service: MetricsControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MetricsControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
