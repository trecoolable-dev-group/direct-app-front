import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlaysBoxBaseComponent } from './overlays-box-base.component';

describe('OverlaysBoxBaseComponent', () => {
  let component: OverlaysBoxBaseComponent;
  let fixture: ComponentFixture<OverlaysBoxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlaysBoxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlaysBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
