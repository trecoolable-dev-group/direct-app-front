import axios from "axios";
import Pusher from 'pusher-js';


export class EventsYounowControlService {

    constructor() {}





    /// EXTERNAL ///
    //=====================================

    /*
     * @Params:  event
     * @Does:    passes event to external
     */
    public onEvent(event) {
        console.log('on younow event');
    }
    //end onEvent()


    //=====================================
    /// END EXTERNAL ///











    /// CONNECTION ///
    //========================================

    //status
    public status = '';

    //identifier
    public identifier: any = '';

    //younow id
    public younow_id = '';

    //connect timeout
    private connectTimeout = null;


    /*
     * @Params:  none
     * @Does:    subscribes to pusher
     *           sets handlers for chat and gifts
     */
    public async connect() {
        let model = this;
        try {
            //check if has timeout
            if (model.connectTimeout != null) return null;
            //set timeout
            model.connectTimeout = setTimeout(() => {
                //unset timeout
                window.clearTimeout(model.connectTimeout);
                //remove timeout
                model.connectTimeout = null;
            }, 2500);
            //check if is connected
            if (model.status.length) return null;
            //set connecting
            model.status = 'connecting';
            //check if is younow id
            if (isNaN(model.identifier + 0)) {
                //fetch younow id
                let cors = 'https://stream-cors.herokuapp.com/';
                //user link
                let url = 'https://api.younow.com/php/api/broadcast/info/curId=0/user=' + model.identifier;
                //fetch user id
                let idenOp = await axios.get(cors + url);
                //got userid and is broadcasting
                model.younow_id = idenOp.data.userId;
            } else {
                //set younow id
                model.younow_id = model.identifier;
            }
            //set pusher
            model.Pusher = new Pusher('d5b7447226fc2cd78dbb', { cluster: 'younow' });
            //set channel
            model.Channel = model.Pusher.subscribe('public-channel_' + model.younow_id);
            //handle chat messages
            model.Channel.bind('onChat', data => {
                //set comments from event
                let comments = data.message.comments;
                //iterate through comments
                comments.forEach(item => {
                    //format event
                    let event = {
                        platform: 'younow',
                        platform_id: item.userId,
                        platform_info: {
                            username: item.name,
                        },
                        type: 'chat',
                        content: item.comment,
                    };
                    //expose to external chat
                    model.onEvent(event);
                });
            });
            //handle gifts
            model.Channel.bind('onGift', data => {
                //set gift item
                let item = data.message.stageGifts[0];
                //set event
                let event = {
                    platform: 'younow',
                    platform_id: item.userId,
                    platform_info: {
                        username: item.name,
                    },
                    type: 'gift',
                    content: {
                        gift_id: item.SKU,
                        image: 'https://ynassets.younow.com/gifts/live/' + item.SKU + '/web.png?',
                        value: item.value
                    },
                };
                //expose to external gift
                model.onEvent(event);
            });
            //set status
            model.status = 'connected';
            //return success
            return reply(true, 'connected to events');
        } catch (error) {
            //return error
            return reply(false, 'error connecting to younow');
        }
    }
    //end connect()

    //========================================
    /// END CONNECTION ///










    /// PUSHER ///
    //======================================

    //pusher api
    private Pusher: Pusher = null;

    //channel
    private Channel: any = null;


    /*
     * @Params:  younow_id
     * @Does:    subscribes to pusher api
     *           sets bindings for chat and gifts
     * @Return:  success
     */
    public async subscribePusher(younow_id) {
        let model = this;
        try {
            //check if is younow id
            if (isNaN(younow_id + 0)) {
                //fetch younow id
                let cors = 'https://stream-cors.herokuapp.com/';
                //user link
                let url = 'https://api.younow.com/php/api/broadcast/info/curId=0/user=' + younow_id;
                //fetch user id
                let idenOp = await axios.get(cors + url);
                //got userid and is broadcasting
                younow_id = idenOp.data.userId;
            }
            //set pusher
            model.Pusher = new Pusher('d5b7447226fc2cd78dbb', { cluster: 'younow' });
            //set channel
            model.Channel = model.Pusher.subscribe('public-channel_' + younow_id);
            //handle chat messages
            model.Channel.bind('onChat', data => {
                //set comments from event
                let comments = data.message.comments;
                //iterate through comments
                comments.forEach(item => {
                    //format event
                    let event = {
                        type: 'chat',
                        platform: 'younow',
                        sender_id: item.userId,
                        username: item.name,
                        content: item.comment
                    };
                    //expose to external chat
                    model.onEvent(event);
                });
            });
            //handle gifts
            model.Channel.bind('onGift', data => {
                //set gift item
                let item = data.message.stageGifts[0];
                //set event
                let event = {
                    type: 'gift',
                    platform: 'younow',
                    sender_id: item.userId,
                    gift_id: item.SKU,
                    image: 'https://ynassets.younow.com/gifts/live/' + item.SKU + '/web.png?',
                    username: item.name,
                    value: item.value
                };
                //expose to external gift
                model.onEvent(event);
            });
            //return success
            return reply(true, 'connected to events');
        } catch (error) {
            return reply(false, 'error connecting to events', { error: error });
        }
    }
    //end subscribePusher()


    //======================================
    /// END PUSHER ///








}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}