import { TestBed } from '@angular/core/testing';

import { GiftsBracketsControlService } from './gifts-brackets-control.service';

describe('GiftsBracketsControlService', () => {
  let service: GiftsBracketsControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GiftsBracketsControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
