import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadTextBaseComponent } from './load-text-base.component';

describe('LoadTextBaseComponent', () => {
  let component: LoadTextBaseComponent;
  let fixture: ComponentFixture<LoadTextBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadTextBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadTextBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
