import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsBoxBaseComponent } from './events-box-base.component';

describe('EventsBoxBaseComponent', () => {
  let component: EventsBoxBaseComponent;
  let fixture: ComponentFixture<EventsBoxBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsBoxBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
