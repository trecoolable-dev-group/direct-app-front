import { ItemsDbService } from '@controls/bridge-control/src/lib/tools/items-db/items-db.service';
import { ItemMessageService } from '@controls/bridge-control/src/lib/tools/item-message/item-message.service';


export class AccountsItemVoiceControlService {

    constructor(private AccountsDb: ItemsDbService, public identifier: string) {}



    /// DB ///
    //===========================

    //collections
    private ACCOUNTS_COL = 'accounts_col';
    private ACCOUNTS_THEMES_COL = 'accounts_themes_col';
    private ACCOUNTS_VOICES_COL = 'accounts_voices_col';


    //===========================
    /// END DB ///










    /// MESSAGE ///
    //=================================

    //message
    public message = new ItemMessageService();

    //messages
    public input_messages = {};

    //=================================
    /// END MESSAGE ///











    /// SETTINGS ///
    //==============================

    //settings
    public settings = {
        highlight: 0,
        voice: '',
        pitch: 1,
        speed: 1,
    };

    //settings inputs
    public settings_inputs = { ...this.settings };

    //has info flag
    public has_info = false;


    /*
     * @Params:  none
     * @Does:    checks if has settings
     *           fetches stored voice settings
     * @return:  settings
     */
    public async fetchSettings() {
        let model = this;
        //fetch settings
        let fetchOp: any = await model.AccountsDb.findItemsByKey('identifier', model.identifier, model.ACCOUNTS_VOICES_COL, 1);
        //check if has settings
        if (!fetchOp.data.items.length) return reply(true, 'no settings', { settings: model.settings });
        //has settings, set settings
        let settings = fetchOp.data.items[0];
        //set keys
        let keys = Object.keys(model.settings);
        //iterate and set keys
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //set key
            if (settings[key] != undefined) model.settings[key] = settings[key];
        }
        //set inputs
        model.settings_inputs = { ...model.settings };
        //return with key
        return reply(true, 'got settings', { settings: model.settings });
    }
    //end fetchSettings()



    /*
     * @Params:  none
     * @Does:    saves settings
     */
    public saveSettings() {
        let model = this;
        //unset message
        model.message.clearMessage();
        //fetch changes
        let keys = model.fetchChanges();
        //set info to inputs
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //set changes
            model.settings[key] = model.settings_inputs[key];
        }
        //set save item
        let item = {
            identifier: model.identifier,
            ...model.settings
        };
        //save settings
        model.AccountsDb.upsertItem(item, model.ACCOUNTS_VOICES_COL);
        //set saved message
        model.message.setMessage('saved settings', '', true, 5000);
        //return saved
        return reply(true, 'saved settings');
    }
    //end saveSettings()



    /*
     * @Params:  key, value
     * @Does:    validates input
     *           sets input key
     *           sets has change
     */
    public handleChange(key, value) {
        let model = this;
        //set input key
        model.settings_inputs[key] = value;
    }
    //end handleChange()



    /*
     * @Params:  none
     * @Does:    checks if has changes
     * @Return:  changes
     */
    public fetchChanges() {
        let model = this;
        //set changes array
        let changes = [];
        //change keys
        let keys = [
            'highlight',
            'voice',
            'pitch',
            'speed'
        ];
        //iterate through keys
        for (let i = 0; i < keys.length; i++) {
            //set key
            let key = keys[i];
            //check fi is different
            if (model.settings[key] != model.settings_inputs[key]) changes.push(key);
        }
        //return changes
        return changes;
    }
    //end fetchChanges()



    /*
     * @Params:  none
     * @Does:    resets inputs to original
     */
    public resetChanges() {
        let model = this;
        //set settings to previous
        model.settings_inputs = { ...model.settings };
    }
    //end resetChanges()

    //==============================
    /// END SETTINGS ///




}


function reply(s, m, d = {}) {
    return { success: s, message: m, data: d }
};


//wait function
function wait(duration) {
    return new Promise(resolve => {
        setTimeout(() => {
            return resolve(true);
        }, duration);
    });
}