import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'common-elements-radio-toggle-base',
    templateUrl: './radio-toggle-base.component.html',
    styleUrls: ['./radio-toggle-base.component.css']
})
export class RadioToggleBaseComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}









    /// INTERACTION ///
    //====================================

    //click event
    public handleClick(event) {
        let model = this;
        //splash
        model.splash(event);
    }
    //end handleClick()


    //====================================
    /// END INTERACTION ///













    /// SPLASH ///
    //=================================

    //splash target element
    @ViewChild('TargetElement') TargetElement: ElementRef;

    //splash event array
    public splash_events = [];


    /*
     * @Params:  event
     * @Does:    gets position of mouse in target
     *           adds splash event
     */
    public splash(event) {
        let model = this;
        //get mouse coordinates
        let x = event.x - this.TargetElement.nativeElement.offsetLeft;
        let y = event.y - this.TargetElement.nativeElement.offsetTop;
        //add splash event
        model.splash_events.push({ x: x, y: y });
        //check if has many events, splice extra
        if (model.splash_events.length > 3) model.splash_events.splice(0, 1);
    }
    //end splash()


    //=================================
    /// END SPLASH ///











}